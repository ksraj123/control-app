DISTRIBUTION=$(grep ^ID= /etc/os-release | awk -F '=' '{print $2}')
VERSION=$(grep ^VERSION_ID= /etc/os-release | awk -F '=' '{print $2}')
PRIVATE_DEPENDENCIES="pyengage ingestor-api-model cloud-services ingestor-storage ingestor-security ingestor-users"
if [ $DISTRIBUTION == 'fedora' ] && [ $VERSION -lt '30' ]
then
   pip uninstall -y $PRIVATE_DEPENDENCIES;pip install -r requirements.txt
else
   pip3 uninstall -y $PRIVATE_DEPENDENCIES;pip3 install -r requirements.txt
fi
