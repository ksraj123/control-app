# -*- coding: utf-8 -*-
'''
Users management for cu app
'''

# Copyright Gonzalo Odiard, Ingestor 2018

# This file is part of Ingestor's Knowbot platform and is copyright
# and a valuable trade secret of Ingestor, Inc.

from engage.engage import Engage
from engage.botconfig import get_bot_config_by_id

PUBLIC_USER_ID = 'help@ingestor.com'


class User(object):

    def __init__(self, email):
        self._email = email
        self._password = None
        self._roles_by_bot = None
        self._bots_path = None
        self._bots_ids = None
        self._bots_names = {}
        self._token = None
        self._api_key = None
        # needed by LoginManager
        self.is_authenticated = False
        self.is_active = True
        self.is_anonymous = False
        self._authzero_profile = {}

    def _get_authzero_profile(self):
        return self._authzero_profile

    def _set_authzero_profile(self, authzero_profile):
        self._authzero_profile = authzero_profile

    def _get_engage(self):
        return Engage()

    def get_bots(self):
        # self._loads_bot_data()
        roles = self.get_roles_by_bot()
        if roles:
            return roles.keys()
        else:
            return []

    def set_password(self, password):
        # TODO
        self._password = password

    def validate_password(self, password):
        # TODO
        return password == self._password

    def password_available(self):
        return self._password is not None

    def set_token(self, token):
        self._token = token
        self.is_authenticated = True

    def get_token(self):
        return self._token

    def set_api_key(self, api_key):
        self._api_key = api_key
        self.is_authenticated = True

    def get_api_key(self):
        return self._api_key

    # method required by flask-login

    def get_id(self):
        return self._email

    def get_bots_names(self):
        if self._bots_names == {}:
            profile = self._get_authzero_profile()
            for bot_id in profile.get('bots'):
                bot_config = get_bot_config_by_id(bot_id)
                self._bots_names[bot_id] = bot_config
        return self._bots_names

    def get_bot_path_by_name(self, bot_name):
        bot = self.get_bots_names().get(bot_name)
        return bot.get_bot_path()

    def get_bot_name_by_path(self, bot_path):
        return self._bots_ids.get(bot_path)

    def get_roles_by_bot(self):
        profile = self._authzero_profile
        return profile.get('roles', {})

    def get_role_at_bot(self, bot_name):
        return self.get_roles_by_bot().get(bot_name)

    def reset_bots_data(self):
        self._roles_by_bot = None
        self._bots_names = None
        self._bots_path = None
        self._bots_ids = None

    def __repr__(self):
        return 'User(%s, %s)\n' % (self._email, self.get_bots())
