FROM python:3.9 as main

WORKDIR /usr/pip-dep
COPY requirements.txt .

# create .netrc
ARG GITLAB_TOKEN_NAME
ARG GITLAB_TOKEN_PASS
RUN echo "machine gitlab.com" > /root/.netrc
RUN echo "login "$GITLAB_TOKEN_NAME >> /root/.netrc
RUN echo "password "$GITLAB_TOKEN_PASS >> /root/.netrc

RUN pip3 install -r requirements.txt

WORKDIR /usr/src

#####################################

from main as deploy

copy . .

CMD python app.py
