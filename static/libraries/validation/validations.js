function validateLogin(event) {
  event.preventDefault();
  var form = event.target;
  setNext(form);
  if (form.email.value == '' || form.password.value == '') {
    showError("Email and password are required");
    return;
  }
  hideError();
  form.submit();
}

function setNext(form) {
  var location = window.location;
  var searchParams = new URLSearchParams(location.search);
  var next = searchParams.get('next');
  form.next.value = next || '';
}

function registerStepOne(event) {
  event.preventDefault();
  var form = event.target;
  if (form.email.value == '') {
    showError("Work Email is required");
    return false;
  }
  hideError();
  form.submit();
}

function registerStepTwo(event) {
  event.preventDefault();
  var form = event.target;
  if (form.email.value == '') {
    showError("Work Email is required");
    return false;
  } else {
    if (form.logincode.value == '' || form.password.value == '' || form.password2.value == '') {
      showError("Login code and Password are required");
      return false;
    }
    if (form.password.value != form.password2.value) {
      showError("Passwords are different");
      return false;
    } else {
      var validation = passwordPatternValidation(form.password.value);
      if (!validation['valid']) {
        showError(validation['errorMsg']);
        return false;
      }
    }
  }
  hideError();
  form.submit();
}

function validateChangePassword(event) {
  event.preventDefault();
  var form = event.target;
  hideError();
  hideSuccess();
  if (form.old_password.value == '') {
    showError(msgBox("", "The old password is required"));
    return false;
  } else if (form.password.value == '') {
    showError(msgBox("", "The new password is required"));
    return false;
  } else {
    if (form.password.value != form.password2.value) {
      showError(msgBox("", "The new passwords are different"));
      return false;
    } else {
      var validation = passwordPatternValidation(form.password.value);
      if (!validation['valid']) {
        showError(msgBox("Invalid Password!", validation['errorMsg']));
        return false;
      }
    }
  }
  form.submit();
}

function msgBox(status, message) {
  return '<strong>' + status + '</strong> ' + message +
         '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>';
}

function removeReadOnly(input) {
  input.form.logincode.readOnly = true;
  input.form.password.readOnly  = true;
  input.form.password2.readOnly = true;
  input.removeAttribute('readonly');
}

function showError(errorMsg) {
  $('#error-msg').html(errorMsg);
  $('#error-msg').removeClass('hidden').addClass('show').show();
}

function hideError() {
  $('#error-msg').hide();
}

function showSuccess(message) {
  $('#success-msg').html(message);
  $('#success-msg').show();
}

function hideSuccess() {
  $('#success-msg').hide();
}

function hideMessages() {
  hideSuccess();
  hideError();
}

function passwordPatternValidation(password) {
  var rules = [{
    exp: /^.{8,20}$/,
    msg: '<li>Contain at least 8 characters.</li>'
    }, {
    exp: /[%&$+!@#*]/,
    msg: '<li>Include at least one symbol: %, &, $, +, !, @, # or *.</li>'
    }, {
    exp: /[A-Z]/,
    msg: '<li>Include at least one uppercase.</li>'
    }, {
    exp: /[a-z]/,
    msg: '<li>Include at least one lowercase.</li>'
    }, {
    exp: /[0-9]/,
    msg: '<li>Include at least one number (0,1,2...9).</li>'
    }, {
    exp: /^\S*$/,
    msg: '<li>Not contain spaces.</li>'
  }];
  var error = 'The password must: <ul class="text-left">';
  var valid = true;
  for (var i = 0; i < rules.length; i++) {
    var rule = rules[i];
    if (!rule.exp.test(password)) {
      valid = false;
      error += rule.msg + '\n';
    }
  }
  return { valid: valid, errorMsg: error + '</ul>' }
}

function isImage(file) {
  var allowedMimeTypes = ['image/jpeg', 'image/pjpeg', 'image/png', 'image/gif'];
  return allowedMimeTypes.indexOf(file.type) > -1;
}

function isValidSize(file) {
  const size = (file.size / 1024).toFixed(2);
  const limit = 200 // KB
  return size < limit;
}

function validateAvatar(form, inputId, errorImg, errorSize) {
  hideError();
  var img_avatar = form[inputId].files[0];
  var valid = true;
  
  if (!isImage(img_avatar)){
    valid = false;
    showError(msgBox("Error", errorImg));
  }
  if (valid && !isValidSize(img_avatar)) {
    valid = false;
    showError(msgBox("Error", errorSize));
  }

  return valid;
}

function validateEmail(email) {
  var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  return re.test(email);
}

function validateEmailAddresses(emailsAddresses) {
  emailsAddresses = emailsAddresses.replace(/ /g,'');
  var emails = emailsAddresses.split(',');
  var valid = true;
  emails.forEach(function(email) {
    valid = valid && validateEmail(email);
  });
  return valid;
}

function validateTags(stringOfTags) {
  // This method was added in case Ingestor requires any validation of tags in the future
  return true;
}