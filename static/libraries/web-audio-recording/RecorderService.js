class RecorderService {
  constructor () {
    window.AudioContext = window.AudioContext || window.webkitAudioContext

    this.em = document.createDocumentFragment()

    this.state = 'inactive'

    this.events = {}
    this.chunks = []
    this.chunkType = ''

    this.encoderMimeType = 'audio/wav'

    this.config = {
      manualEncoderId: 'wav',
      micGain: 1.0,
      processorBufferSize: 2048,
      stopTracksAndCloseCtxWhenFinished: true,
      usingMediaRecorder: typeof window.MediaRecorder !== 'undefined',
      //userMediaConstraints: { audio: true }
      userMediaConstraints: { audio: { echoCancellation: false } }
    }
  }

  on(event, callback) {
    this.events[event] = callback
  }

  emit() {
    if (this.events[arguments[0]]) {
      this.events[arguments[0]](arguments[1], arguments[2], arguments[3], arguments[4])
    }
  }

  /* Returns promise */
  startRecording () {
    if (this.state !== 'inactive') {
      return
    }

    // This is the case on ios/chrome, when clicking links from within ios/slack (sometimes), etc.
    if (!navigator || !navigator.mediaDevices || !navigator.mediaDevices.getUserMedia) {
      alert('Missing support for navigator.mediaDevices.getUserMedia') // temp: helps when testing for strange issues on ios/safari
      return
    }

    this.audioCtx = new AudioContext()
    this.micGainNode = this.audioCtx.createGain()
    this.outputGainNode = this.audioCtx.createGain()

    // If not using MediaRecorder(i.e. safari and edge), then a script processor is required. It's optional
    // on browsers using MediaRecorder and is only useful if wanting to do custom analysis or manipulation of
    // recorded audio data.
    if (!this.config.usingMediaRecorder) {
      this.processorNode = this.audioCtx.createScriptProcessor(this.config.processorBufferSize, 1, 1) // TODO: Get the number of channels from mic
    }

    // Create stream destination on chrome/firefox because, AFAICT, we have no other way of feeding audio graph output
    // in to MediaRecorder. Safari/Edge don't have this method as of 2018-04.
    if (this.audioCtx.createMediaStreamDestination) {
      this.destinationNode = this.audioCtx.createMediaStreamDestination()
    }
    else {
      this.destinationNode = this.audioCtx.destination
    }

    // Create web worker for doing the encoding
    if (!this.config.usingMediaRecorder) {
      this.encoderWorker = new Worker('/static/libraries/web-audio-recording/encoder-wav-worker.js')
      this.encoderMimeType = 'audio/wav'

      this.encoderWorker.addEventListener('message', (e) => {
        let event = new Event('dataavailable')
        event.data = new Blob(e.data, { type: this.encoderMimeType })
        this._onDataAvailable(event)
      })
    }

    var recorder = this;

    // This will prompt user for permission if needed
    return navigator.mediaDevices.getUserMedia(this.config.userMediaConstraints)
      .then((stream) => {
        recorder._startRecordingWithStream(stream)
        var options = {
          recorder: recorder,
          interval: 100,
          timeout: 5
        };
        var speechEvents = hark(stream, options);

        speechEvents.on('speaking', function () {
          console.log("start")
        });

        speechEvents.on('stopped_speaking', function () {
          recorder.stopRecording()
          console.log("stop")
        });

      })
      .catch((error) => {
        alert('Error with getUserMedia: ' + error.message) // temp: helps when testing for strange issues on ios/safari
        console.log(error)
      })
  }

  _startRecordingWithStream (stream) {
    this.micAudioStream = stream

    this.inputStreamNode = this.audioCtx.createMediaStreamSource(this.micAudioStream)
    this.audioCtx = this.inputStreamNode.context

    // Allow optionally hooking in to audioGraph inputStreamNode, useful for meters
    if (this.onGraphSetupWithInputStream) {
      this.onGraphSetupWithInputStream(this.inputStreamNode)
    }

    this.inputStreamNode.connect(this.micGainNode)
    this.micGainNode.gain.setValueAtTime(this.config.micGain, this.audioCtx.currentTime)

    let nextNode = this.micGainNode

    this.state = 'recording'

    if (this.processorNode) {
      nextNode.connect(this.processorNode)
      this.processorNode.connect(this.outputGainNode)
      this.processorNode.onaudioprocess = (e) => this._onAudioProcess(e)
    }
    else {
      nextNode.connect(this.outputGainNode)
    }

    this.outputGainNode.connect(this.destinationNode)

    if (this.config.usingMediaRecorder) {
      this.mediaRecorder = new MediaRecorder(this.destinationNode.stream)
      this.mediaRecorder.addEventListener('dataavailable', (evt) => this._onDataAvailable(evt))
      this.mediaRecorder.addEventListener('error', (evt) => this._onError(evt))

      this.mediaRecorder.start()
    }
    else {
      // Output gain to zero to prevent feedback. Seems to matter only on Edge, though seems like should matter
      // on iOS too.  Matters on chrome when connecting graph to directly to audioCtx.destination, but we are
      // not able to do that when using MediaRecorder.
      this.outputGainNode.gain.setValueAtTime(0, this.audioCtx.currentTime)
    }
  }

  _onAudioProcess (e) {
    if (this.config.broadcastAudioProcessEvents) {
      this.em.dispatchEvent(new CustomEvent('onaudioprocess', {
        detail: {
          inputBuffer: e.inputBuffer,
          outputBuffer: e.outputBuffer
        }
      }))
    }

    // Safari and Edge require manual encoding via web worker. Single channel only for now.
    // Example stereo encoderWav: https://github.com/MicrosoftEdge/Demos/blob/master/microphone/scripts/recorderworker.js
    if (!this.config.usingMediaRecorder) {
      if (this.state === 'recording') {
        if (this.config.broadcastAudioProcessEvents) {
          this.encoderWorker.postMessage(['encode', e.outputBuffer.getChannelData(0)])
        }
        else {
          this.encoderWorker.postMessage(['encode', e.inputBuffer.getChannelData(0)])
        }
      }
    }
  }

  stopRecording () {
    if (this.state === 'inactive') {
      return
    }

    if (this.config.usingMediaRecorder) {
      this.state = 'inactive'
      this.mediaRecorder.stop()
    }
    else {
      this.state = 'inactive'
      this.encoderWorker.postMessage(['dump', this.audioCtx.sampleRate])
    }
  }

  _onDataAvailable (evt) {
    this.chunks.push(evt.data)
    this.chunkType = evt.data.type

    if (this.state !== 'inactive') {
      return
    }

    let blob = new Blob(this.chunks, { 'type': this.chunkType })
    let blobUrl = URL.createObjectURL(blob)
    const recording = {
      ts: new Date().getTime(),
      data: blob,
      blobUrl: blobUrl,
      mimeType: blob.type,
      size: blob.size
    }

    this.chunks = []
    this.chunkType = null

    if (this.destinationNode) {
      this.destinationNode.disconnect()
      this.destinationNode = null
    }
    if (this.outputGainNode) {
      this.outputGainNode.disconnect()
      this.outputGainNode = null
    }

    if (this.processorNode) {
      this.processorNode.disconnect()
      this.processorNode = null
    }

    if (this.encoderWorker) {
      this.encoderWorker.postMessage(['close'])
      this.encoderWorker = null
    }

    if (this.micGainNode) {
      this.micGainNode.disconnect()
      this.micGainNode = null
    }
    if (this.inputStreamNode) {
      this.inputStreamNode.disconnect()
      this.inputStreamNode = null
    }

    if (this.config.stopTracksAndCloseCtxWhenFinished) {
      // This removes the red bar in iOS/Safari
      this.micAudioStream.getTracks().forEach((track) => track.stop())
      this.micAudioStream = null

      this.audioCtx.close()
      this.audioCtx = null
    }

    this.em.dispatchEvent(new CustomEvent('recording', { detail: { recording: recording } }))
  }

  _onError (evt) {
    console.log('error', evt)
    this.em.dispatchEvent(new Event('error'))
    alert('error:' + evt) // for debugging purposes
  }
}


function hark(stream, options) {
  var audioContextType = window.webkitAudioContext || window.AudioContext;
  var harker = this;
  
  harker.events = {};
  harker.on = function (event, callback) {
    harker.events[event] = callback;
  };

  harker.emit = function() {
    if (harker.events[arguments[0]]) {
      harker.events[arguments[0]](arguments[1], arguments[2], arguments[3], arguments[4]);
    }
  };

  // make it not break in non-supported browsers
  if (!audioContextType) return harker;

  options = options || {};
  // Config
  var smoothing = (options.smoothing || 0.1),
  interval = (options.interval || 50),
  threshold = (options.threshold || 1),
  play = options.play,
  history = options.history || 10,
  recorder = options.recorder,
  timeout = options.timeout * 1000 || 5000;

  // Setup Audio Context
  if (!window.audioContext00) {
    window.audioContext00 = new AudioContext();
  }

  var gainNode = audioContext00.createGain();
  gainNode.connect(audioContext00.destination);
  // don't play for self
  gainNode.gain.value = 0;

  var sourceNode, fftBins, analyser;

  analyser = audioContext00.createAnalyser();
  analyser.fftSize = 512;
  analyser.smoothingTimeConstant = smoothing;
  fftBins = new Float32Array(analyser.fftSize);

  //WebRTC Stream
  sourceNode = audioContext00.createMediaStreamSource(stream);
  sourceNode.connect(analyser);

  if (play) analyser.connect(audioContext00.destination);

  harker.speaking = false;

  harker.speakingHistory = [];
  for (var i = 0; i < history; i++) {
    harker.speakingHistory.push(0);
  }

  var recordingTime = 0;
  var extend = false;

  // Poll the analyser node to determine if speaking
  // and emit events if changed
  var looper = function() {

    setTimeout(function() {
      recordingTime += interval;

      //check if stop has been called
      if (recordingTime > timeout) { 
        recorder.stopRecording();
        return;
      }

      var currentVolume = getMaxVolume(analyser, fftBins);
      recorder.emit('volume_change', currentVolume);

      var history = 0;
      if (currentVolume > threshold && !harker.speaking) {
        // trigger quickly, short history
        for (var i = harker.speakingHistory.length - 3; i < harker.speakingHistory.length; i++) {
          history += harker.speakingHistory[i];
        }

        if (history >= 2) {
          harker.speaking = true;
          harker.emit('speaking');
          recorder.emit('start_record');
          if (!extend) {
            recordingTime -= 2000 // 2 seconds more
            extend = true;
          }
        }
      } else if (currentVolume <= threshold && harker.speaking) {
        for (var j = 0; j < harker.speakingHistory.length; j++) {
          history += harker.speakingHistory[j];
        }
        if (history === 0) {
          harker.speaking = false;
          harker.emit('stopped_speaking');
          recorder.emit('stop_record');
        }
      }
      harker.speakingHistory.shift();
      harker.speakingHistory.push(0 + (currentVolume > threshold));

      looper();
    }, interval);
  };

  looper();

  function getMaxVolume(analyser, fftBins) {
    var maxVolume = -Infinity;
    analyser.getFloatFrequencyData(fftBins);

    for (var i = 4, ii = fftBins.length; i < ii; i++) {
      if (fftBins[i] > maxVolume && fftBins[i] < 0) {
        maxVolume = fftBins[i];
      }
    }
    return volumeValue(maxVolume);
  }

  function volumeValue(vol) {
    var currentVol = -1 * vol;
    if (currentVol < 25) {
      return 7;
    } else if (currentVol < 30) {
      return 6;
    } else if (currentVol < 35) {
      return 5;
    } else if (currentVol < 40) {
      return 4;
    } else if (currentVol < 45) {
      return 3;
    } else if (currentVol < 50) {
      return 2;
    } else if (currentVol < 55) {
      return 1;
    } else {
      return 0;
    }
  }

  return harker;
}
