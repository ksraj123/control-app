function set_page(page) {
    // NOTE: not supported ond old browsers https://caniuse.com/#feat=urlsearchparams
    location = window.location;
    var searchParams = new URLSearchParams(location.search);
    searchParams.set('page', page);
    window.location.search = searchParams.toString();
};

function set_search(text) {
    // NOTE: not supported ond old browsers https://caniuse.com/#feat=urlsearchparams
    location = window.location;
    var searchParams = new URLSearchParams(location.search);
    searchParams.set('text_filter', text);
    searchParams.set('page', 0);
    setTimeout(function() {window.location.search = searchParams.toString()}, 1);
};

function set_direction(direction) {
    // NOTE: not supported ond old browsers https://caniuse.com/#feat=urlsearchparams
    location = window.location;
    var searchParams = new URLSearchParams(location.search);
    searchParams.set('direction', direction);
    searchParams.set('page', 0);
    setTimeout(function() {window.location.search = searchParams.toString()}, 1);
};

function set_since(since) {
    // NOTE: not supported ond old browsers https://caniuse.com/#feat=urlsearchparams
    location = window.location;
    var searchParams = new URLSearchParams(location.search);
    searchParams.set('since', since);
    searchParams.set('page', 0);
    setTimeout(function() {window.location.search = searchParams.toString()}, 1);
};

function set_tag(tag) {
    // NOTE: not supported ond old browsers https://caniuse.com/#feat=urlsearchparams
    location = window.location;
    var searchParams = new URLSearchParams(location.search);
    searchParams.set('tag', tag);
    searchParams.set('page', 0);
    setTimeout(function() {window.location.search = searchParams.toString()}, 1);
};

function set_status(status) {
    // NOTE: not supported ond old browsers https://caniuse.com/#feat=urlsearchparams
    location = window.location;
    var searchParams = new URLSearchParams(location.search);
    searchParams.set('status', status);
    searchParams.set('page', 0);
    setTimeout(function() {window.location.search = searchParams.toString()}, 1);
};

function set_queue_type(queue_type) {
    // NOTE: not supported ond old browsers https://caniuse.com/#feat=urlsearchparams
    location = window.location;
    var searchParams = new URLSearchParams(location.search);
    searchParams.set('queue_type', queue_type);
    searchParams.set('page', 0);
    setTimeout(function() {window.location.search = searchParams.toString()}, 1);
};

function set_solved(solved) {
    // NOTE: not supported ond old browsers https://caniuse.com/#feat=urlsearchparams
    location = window.location;
    var searchParams = new URLSearchParams(location.search);
    searchParams.set('solved', solved);
    searchParams.set('page', 0);
    setTimeout(function() {window.location.search = searchParams.toString()}, 1);
};

function set_file_type_ext(text) {
    // NOTE: not supported ond old browsers https://caniuse.com/#feat=urlsearchparams
    location = window.location;
    var searchParams = new URLSearchParams(location.search);
    searchParams.set('file_type_ext', text);
    searchParams.set('page', 0);
    setTimeout(function() {window.location.search = searchParams.toString()}, 1);
};

function set_doc_status(status) {
    location = window.location;
    var searchParams = new URLSearchParams(location.search);
    searchParams.set('doc_status', status);
    searchParams.set('page', 0);
    setTimeout(function() {window.location.search = searchParams.toString()}, 1);
}


function display_input(bot_id, input_id, quantity, position) {
    var actual_location = window.location;
    // remove the '_' char at the begining
    input_id = input_id.replace('/', '_');
    var searchParams = new URLSearchParams(actual_location.search);
    var page = searchParams.get('page');

    if (position == 10) {
        searchParams.set('position', 0);
        searchParams.set('page', (page? parseInt(page)+1 : 1))
    } else if (position == -1) {
        searchParams.set('position', 9);
        searchParams.set('page', (page? parseInt(page)-1 : 0))
    } else {
        searchParams.set('position', position);
    }
    searchParams.set('quantity', quantity);
    url = '/question/' + bot_id + '/' + input_id
    window.location = url + '?' + searchParams.toString();
};

function displayDocument(bot_id, doc_id, quantity, position) {
    var actual_location = window.location;
    doc_id = doc_id.replace('/', '_');
    var searchParams = new URLSearchParams(actual_location.search);
    var page = searchParams.get('page');

    if (position == 10) {
        searchParams.set('position', 0);
        searchParams.set('page', (page? parseInt(page)+1 : 1))
    } else if (position == -1) {
        searchParams.set('position', 9);
        searchParams.set('page', (page? parseInt(page)-1 : 0))
    } else {
        searchParams.set('position', position);
    }
    searchParams.set('quantity', quantity);

    url = '/document/' + bot_id + '/' + doc_id
    window.location = url + '?' + searchParams.toString();
};

function validatePropertie(params, propertie) {
    return params.has(propertie)? Number.isInteger(params.get(propertie)) : true;
}

function display_issue(bot_id, issue_id, quantity, position) {
    var actual_location = window.location;
    var searchParams = new URLSearchParams(actual_location.search);
    var page = searchParams.get('page');

    if (position == 10) {
        searchParams.set('position', 0);
        searchParams.set('page', (page? parseInt(page)+1 : 1))
    } else if (position == -1) {
        searchParams.set('position', 9);
        searchParams.set('page', (page? parseInt(page)-1 : 0))
    } else {
        searchParams.set('position', position);
    }
    searchParams.set('quantity', quantity);

    // remove the '#' char at the begining
    issue_id = issue_id.replace('#', '');
    // keep queue filters and page
    window.location = '/issue/' + bot_id + '/' + issue_id + '?' + searchParams.toString();
};

function show_issues(bot_id) {
    actual_location = window.location;
    // keep queue filters and page
    window.location = '/issues/' + bot_id + actual_location.search;
};

function show_questions(bot_id) {
    actual_location = window.location;
    // keep queue filters and page
    window.location = '/questions/' + bot_id + actual_location.search;
};

function show_library(bot_id) {
    actual_location = window.location;
    // keep queue filters and page
    window.location = '/library/' + bot_id + actual_location.search;
};

function clean_id(id, id_type) {
    if (id_type != undefined) {
        id = id.replace('/' + id_type + '/', '');
    }
    id = id.replace('/', '_');
    return id;
};

function verify(bot_id, input_id, response_id, queue_id, queue_type) {
    actual_location = window.location;
    url = '/verify/' + bot_id + '/' + input_id + '/' + response_id;
    // if find the checkbox, verify if is checked
    var removeOtherResponses = false;
    if ($('#removeOtherResponses') != undefined) {
        removeOtherResponses = $('#removeOtherResponses').prop('checked');
    }

    if (queue_id != undefined && queue_id != '') {
        queue_id = queue_id.replace('#', '');
        url = url + '/' + queue_id;
        url = url + '/' + queue_type;
    }
    // keep queue filters and page
    url = url + actual_location.search;
    if (removeOtherResponses) {
        url += url.indexOf('?') === -1 ? '?' : '&';
        url += 'remove_other_responses=S';
    }
    window.location = url;
};

function remember(bot_id, input_text, response_text, queue_id, callback) {

    var dialog = bootbox.dialog({
       message: '<p class="text-center">Processing...</p>',
       closeButton: false
    });

    const data = new FormData();
    data.append('input_text', input_text);
    data.append('response_text', response_text);
    if (queue_id != undefined) {
        data.append('queue_id', queue_id);
    }

    fetch('/remember/' + bot_id, {
        method: 'POST',
        body: data
    })
    .then(function () {
        callback();
    })
    .catch(reason => console.error(reason));

};

function modify_response_text(bot_id, response_id, response_text, callback) {
    // DEPRECATED
    if (response_text == '') {
        // do not save empty text
        return;
    }
    var processingDialog = bootbox.dialog({
       message: '<p class="text-center">Processing...</p>',
       closeButton: false
    });

    const data = new FormData();
    data.append('response_id', response_id);
    data.append('response_text', response_text);

    fetch('/modify_response/' + bot_id, {
        method: 'POST',
        body: data
    })
    .then(function () {
        callback();
    })
    .catch(reason => console.error(reason));

};

function remove_response(bot_id, input_id, response_id) {
    input_id = clean_id(input_id, 'input');
    response_id = clean_id(response_id, 'response');
    bootbox.confirm("Are you sure?", function(result) {
        if (result) {
            window.location = '/remove_response/' + bot_id + '/'+ input_id + '/' + response_id + window.location.search;
        };
    });
};

function remove_input(bot_id, input_id) {
    input_id = clean_id(input_id, 'input');
    bootbox.confirm("Are you sure?", function(result) {
        if (result) {
            actual_location = window.location;
            window.location = '/remove_input/' + bot_id + '/' + input_id + actual_location.search;
        };
    });
};

function remove_document(bot_id, doc_id) {
    doc_id = clean_id(doc_id, 'doc');
    bootbox.confirm("Are you sure?", function(result) {
        if (result) {
            actual_location = window.location;
            search = actual_location.search.replace('msg_code=1', '');
            search = search.length == 1 ? '' : search;
            window.location = '/remove_document/' + bot_id + '/' + doc_id + search;
        };
    });
};

function html_to_text(html) {
    var text = html.replace(/<br\/>/g,'\n');
    text = text.replace(/<a href=".+" target="_blank">/g,'');
    return text.replace(/<\/a>/g,'');
}

function showConfirmModal(title, text, callback) {
    var modal = document.createElement('div');
    modal.id = 'editModal';

    var content = `
    <div class="modal fade" id="showConfirmation" tabindex="-1" role="dialog" aria-labelledby="showConfirmationLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <div class="modal-title" id="showConfirmationLabel">${title}</div>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
            <div class="modal-body">
              <div class="pb-3">${text}</div>
              <div class="d-flex justify-content-between">
                <button id="cancelBtn" type="button" class="btn btn-outline-question cancel">CANCEL</button>
                <button id="confirmButton" type="button" class="btn btn-outline-question d-none d-sm-block" style="color: #363F49; border-color: #363F49;">PROCEED</button>
                <button id="confirmButtonmobile" type="button" class="btn btn-outline-question d-block d-sm-none" style="color: #363F49; border-color: #363F49;">PROCEED</button>
              </div>
            </div>
        </div>
      </div>
    </div>
    `
    modal.innerHTML = content;

    document.body.appendChild(modal);

    function proceed() {
      $('#showConfirmation').off('hidden.bs.modal');
      $('#showConfirmation').on('hidden.bs.modal', function (e) {
          callback(true);
      })
      $('#showConfirmation').modal('hide');
    }

    function cancel() {
      $('#showConfirmation').off('hidden.bs.modal');
      $('#showConfirmation').on('hidden.bs.modal', function (e) {
        callback(false);
      })
      $('#showConfirmation').modal('hide');
    }

    $('#cancelBtn').off('click');
    $('#cancelBtn').on('click', cancel);
    $('#confirmButton').off('click');
    $('#confirmButton').on('click', proceed);
    $('#confirmButtonMobile').off('click');
    $('#confirmButtonMobile').on('click', proceed);

    $('#showConfirmation').modal('show');
  }

function getTagHtml(index, tag, extraId) {
    var extraParam = extraId ? `, '${extraId}'` : '';
    var extra = extraId ? `_${extraId}_` : '';
    return `
    <p id="filter_tag${extra}${index}" style="font-size:16px;" class='badge badge-document badges-filter-tag'>
        ${tag}<button type="button" class="close mx-1" style="font-size: 18px" onclick="removeTag('${index}','${tag}'${extraParam})"><span> &times;</span></button>
    </p>`
}

function addNewTag(inputId, tagList, lastIndex, tagSectionId, validationFunc, extraId) {
    var inputValue = $(`#${inputId}`).val();

    if (inputValue.length != 0) {
      if (validationFunc && !validationFunc(inputValue)) {
        $('#val-error-msg').html("{{_('Invalid tags.')}}");
        $('#val-error-msg').show();
        return false;
      }
    } else {
      return false;
    }

    inputValue = inputValue.trim();
    var tags = inputValue.split(' ').filter(tag => tag != '');
    addTagsToListAndView(tags, tagList, tagSectionId, lastIndex, extraId);

    $(`#${inputId}`).val(null);
}

function addTagsToListAndView(sourceList, tagList, tagSectionId, lastIndex, extraId) {
    sourceList.forEach(function(tag) {
        if (!tagList.includes(tag)) {
            tagList.push(tag);
            var newTag = getTagHtml(lastIndex, tag, extraId);
            $(`#${tagSectionId}`).append(newTag);
            lastIndex ++;
        }
    });
}

function removeTagFromListAndView(tagList, index, tag, extraId) {
    var extra = extraId ? `_${extraId}_` : '';
    var i = tagList.indexOf(tag);
    tagList.splice(i, 1);
    $( `#filter_tag${extra}${index}`).remove();
}

function filterLine(source, specificLine) {
    var result = '';
    source.split('\n').forEach(function(line) {
        if (!line.startsWith(specificLine)) {
            result += `${line}\n`;
        }
    });
    return result;
}

function initTagsInput(inputId, bot) {
    var bot_tags = new Bloodhound({
        datumTokenizer: Bloodhound.tokenizers.obj.whitespace('name'),
        queryTokenizer: Bloodhound.tokenizers.whitespace,
        prefetch: {
            url: '/bot_tags/' + bot,
            ttl: 300000,
            filter: function(list) {
                return $.map(list, function(tag) {
                    return { name: tag }; });
            }
        }
    });
    bot_tags.initialize(true);

    $("#" + inputId).tagsinput({
        freeInput: false,
        typeaheadjs: {
            name: 'bot_tags',
            displayKey: 'name',
            valueKey: 'name',
            source: bot_tags.ttAdapter()
        }
    });
}