pybabel extract -F babel.cfg -k lazy_gettext --no-location  --sort-output --omit-header -o messages.pot .
pybabel update -i messages.pot --ignore-obsolete --omit-header -d translations
