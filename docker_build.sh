export GITLAB_TOKEN_NAME=$(cat ~/.netrc | grep gitlab.com -A 2 | grep login | awk '{print $2}')
export GITLAB_TOKEN_PASS=$(cat ~/.netrc | grep gitlab.com -A 2 | grep password | awk '{print $2}')
docker build --build-arg GITLAB_TOKEN_NAME --build-arg GITLAB_TOKEN_PASS -t cu-app .
