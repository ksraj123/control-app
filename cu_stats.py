# -*- coding: utf-8 -*-
'''
Stats used to display in the dashboard
'''

# Copyright Gonzalo Odiard, Ingestor 2019

# This file is part of Ingestor's Knowbot platform and is copyright
# and a valuable trade secret of Ingestor, Inc.

import logging
import os
from datetime import datetime, timedelta

from flask import jsonify, request, render_template, make_response
from flask import Blueprint
from cu_security import current_user
from cu_security import login_required
from flask_babel import _, ngettext

import cairosvg

from engage import usage
from ingestor_storage import uuid_to_str
from engage import engage_tools
from engage.botconfig import get_bot_config_by_id, BotConfig

import cu_tools
import cu_database
import cu_security

COLORS = [
    'rgb(255, 99, 132, %s)',
    'rgb(255, 159, 64, %s)',
    'rgb(255, 205, 86, %s)',
    'rgb(75, 192, 192, %s)',
    'rgb(54, 162, 235, %s)',
    'rgb(153, 102, 255, %s)',
    'rgb(201, 203, 207, %s)',
    'rgb(235, 54, 66, %s)',
    'rgb(54, 235, 99, %s)',
    'rgb(64, 121, 255, %s)',
    'rgb(236, 64, 255, %s)',
    'rgb(255, 64, 147, %s)',
    'rgb(77, 64, 255, %s)',
    'rgb(46, 255, 213, %s)',
    'rgb(54, 235, 154, %s)',
    'rgb(54, 235, 117, %s)',
    'rgb(54, 235, 66, %s)',
    'rgb(138, 235, 54, %s)',
    'rgb(235, 202, 54, %s)',
    'rgb(235, 114, 54, %s)',
    'rgb(192, 75, 134, %s)',
    'rgb(75, 126, 192, %s)',
    'rgb(182, 192, 75, %s)',
    'rgb(192, 132, 75, %s)',
    'rgb(192, 89, 75, %s)',
    'rgb(184, 75, 192, %s)',
    'rgb(192, 75, 108, %s)',
    'rgb(75, 79, 192, %s)',
    'rgb(177, 204, 0, %s)',
    'rgb(204, 82, 0, %s)']

MUNSELL_COLORS = [
    '#ff007e', '#ff0066', '#ff0048', '#ff0000', '#ff3000', '#fd6600',
    '#ffa300', '#ffb000', '#ffb900', '#ffc000', '#ffe400', '#ffe800',
    '#f6ee00', '#e1f400', '#c4fb00', '#4eeb00', '#00f200', '#00fd3c',
    '#00c260', '#00c273', '#00c286', '#00c29d', '#00c1b6', '#00c0ca',
    '#00c0e7', '#00b9f1', '#00b5ff', '#00b0ff', '#00abff', '#00a1ff',
    '#005dff', '#4231ff', '#8000fe', '#a000ef', '#f009ff', '#ff00ff',
    '#ff00f9', '#ff00d9', '#ff00b2', '#ff0098']

# configure logger
logging.basicConfig(level=os.getenv('LOGGER_LEVEL', logging.WARNING))
logger = logging.getLogger('stats')


stats_blueprint = Blueprint('stats', __name__, template_folder='templates')


def get_since():
    since_days = request.args.get('since', None)
    if since_days is None:
        return since_days
    today = datetime.today()
    try:
        days = int(since_days)
    except Exception:
        days = 0
    return today - timedelta(days=days)


def replace_none_keys(dictionary):
    # when query mongo None can be returned as a key
    if None in dictionary:
        dictionary['None'] = dictionary[None]
        del(dictionary[None])


@stats_blueprint.route('/dashboard_month_json/<path:bot_name>')
@login_required
def dashboard_month_json(bot_name):
    bot_path = current_user.get_bot_path_by_name(bot_name)
    dashboard_data = usage.get_minimal_dashboard_data(
        cu_database.stats, bot_name,
        usage.WINDOW_MONTH, bot_path, bot_name,
        token=current_user.get_token())
    return jsonify(dashboard_data.__dict__)


@stats_blueprint.route('/dashboard_prior_month_json/<path:bot_name>')
@login_required
def dashboard_prior_month_json(bot_name):
    bot_path = current_user.get_bot_path_by_name(bot_name)
    logger.debug('bot_path %s' % bot_path)

    prior_now = datetime.today()
    last_month = prior_now - timedelta(
        days=usage.days_by_window(usage.WINDOW_MONTH))

    prior_month = prior_now - timedelta(
        days=usage.days_by_window(usage.WINDOW_MONTH) * 2)
    dashboard_data = usage.get_minimal_dashboard_data(
        cu_database.stats, bot_name,
        prior_month.strftime('%Y-%m-%d'), bot_path, bot_name,
        token=current_user.get_token(),
        window2=last_month.strftime('%Y-%m-%d'))

    return jsonify(dashboard_data.__dict__)


@stats_blueprint.route('/dashboard_month_points/<path:bot_name>')
@login_required
def dashboard_month_points(bot_name):
    bot_path = current_user.get_bot_path_by_name(bot_name)
    if bot_path is None:
        return jsonify([])
    since = datetime.today() - timedelta(
        days=usage.days_by_window(usage.WINDOW_MONTH))
    month_points = cu_database.stats.get_points(bot_path, since)
    return jsonify({"month_points": month_points})


@stats_blueprint.route('/dashboard_all_points/<path:bot_name>')
@login_required
def dashboard_all_points(bot_name):
    bot_path = current_user.get_bot_path_by_name(bot_name)
    if bot_path is None:
        return jsonify([])
    all_points = cu_database.stats.get_points(bot_path)
    return jsonify({"all_points": all_points})


@stats_blueprint.route('/dashboard/<path:bot_name>')
@login_required
@cu_security.verify_bot
def dashboard(bot_name):
    days = [
        {'name': _('Today'), 'value': usage.WINDOW_DAY},
        {'name': _('Past Week'), 'value': usage.WINDOW_WEEK},
        {'name': _('Past Two Weeks'), 'value': usage.WINDOW_FORTNIGHT},
        {'name': _('Past Month'), 'value': usage.WINDOW_MONTH},
        {'name': _('Past Year'), 'value': usage.WINDOW_YEAR}
    ]
    bot_path = current_user.get_bot_path_by_name(bot_name)
    if bot_path is None:
        return jsonify([])

    filter = cu_tools.get_filter(bot_name, request)
    filter['bot_mongo_id'] = bot_path.replace('/', '_')
    bot_path = current_user.get_bot_path_by_name(bot_name)
    bot_tags = sorted(cu_database.library_storage.get_tags(bot_path))
    return render_template(
        'dashboard-new.html', filter=filter, days=days,
        bot_tags=bot_tags, page='dashboard',
        language=cu_tools.get_locale_str())


@stats_blueprint.route('/ontologies/<path:bot_name>')
@login_required
@cu_security.verify_bot
def ontologies(bot_name):
    days = [
        {'name': _('Today'), 'value': usage.WINDOW_DAY},
        {'name': _('Past Week'), 'value': usage.WINDOW_WEEK},
        {'name': _('Past Two Weeks'), 'value': usage.WINDOW_FORTNIGHT},
        {'name': _('Past Month'), 'value': usage.WINDOW_MONTH},
        {'name': _('Past Year'), 'value': usage.WINDOW_YEAR}
    ]
    bot_path = current_user.get_bot_path_by_name(bot_name)
    if bot_path is None:
        return jsonify([])

    since = datetime.today() - timedelta(
        days=usage.days_by_window(usage.WINDOW_MONTH))
    all_points = cu_database.stats.get_points(bot_path)
    month_points = cu_database.stats.get_points(bot_path, since)

    filter = cu_tools.get_filter(bot_name, request)
    filter['bot_mongo_id'] = bot_path.replace('/', '_')

    bot_path = current_user.get_bot_path_by_name(bot_name)
    return render_template(
        'ontologies-abm.html', filter=filter, days=days,
        all_points=all_points, month_points=month_points,
        page='ontologies', language=cu_tools.get_locale_str())


@stats_blueprint.route('/enterprise-dashboard')
@login_required
def enterprise_dashboard():
    filter = cu_tools.get_filter(None, request)
    since = filter['since']

    today = datetime.today()
    if since is None:
        since = today - timedelta(days=365)
        filter['days'] = 365

    chart_colors_by_bots = {}
    instances_data = {}
    my_bots = {}
    total_users = []

    bot_configs = current_user.get_bots_names().values()
    for i, bot_config in enumerate(bot_configs):
        my_bots[bot_config.get_bot_path()] = bot_config.get_display_name()
        chart_colors_by_bots[bot_config.get_bot_path()] = COLORS[
            i % len(COLORS)]
        instances_data[bot_config.get_bot_path()] = {
            'bot_id': bot_config.get_bot_path(),
            'bot_name': bot_config.get_bot_name(),
            'name': bot_config.get_display_name(),
            'total_users': len(bot_config.get_users()),
            'total_searches': 0,
            'total_qa_pairs': cu_database.qa.count_qa_pairs_per_bots(
                {bot_config.get_bot_path(): ''})
        }
        total_users = list(set(total_users + bot_config.get_users()))

    day_count = (today - since).days + 1

    searches_per_bot = []
    total_searches = 0
    actions = cu_database.user_actions.get_searches_per_bot(my_bots, since)
    for bot in actions:
        if 'display_name' in bot:
            current = {
                'data': [],
                'fill': False,
                'label': bot['display_name'],
                'backgroundColor': chart_colors_by_bots[bot['bot_id']] % 0.2,
                'borderColor': chart_colors_by_bots[bot['bot_id']] % 1,
                'borderWidth': 1,
                'lineTension': 0
            }
            bot_searches = 0
            bot_actions = bot['actions']
            actions_by_date = {data['date']: data['count']
                               for data in bot_actions}
            for single_date in (since + timedelta(n)
                                for n in range(day_count)):
                date = single_date.strftime('%Y-%m-%d')
                count = (actions_by_date[date]
                         if date in actions_by_date
                         else 0)
                current['data'].append({'x': date, 'y': count})
                bot_searches += count

            instances_data[bot['bot_id']]['total_searches'] = bot_searches
            searches_per_bot.append(current)

    active_users = []
    for bot in cu_database.user_actions.get_active_users_per_bot(my_bots,
                                                                 since):
        if 'display_name' in bot:
            current = {
                'data': [],
                'fill': False,
                'label': bot['display_name'],
                'backgroundColor': chart_colors_by_bots[bot['bot_id']] % 0.2,
                'borderColor': chart_colors_by_bots[bot['bot_id']] % 1,
                'borderWidth': 1,
                'lineTension': 0
            }

            bot_users = bot['users']
            users_by_date = {data['date']: data['active_users']
                             for data in bot_users}
            for single_date in (since + timedelta(n)
                                for n in range(day_count)):
                date = single_date.strftime('%Y-%m-%d')
                count = (users_by_date[date] if date in users_by_date else 0)
                current['data'].append({'x': date, 'y': count})
                total_searches += count

            active_users.append(current)

    top_data_general = {
        'total_instances': len(bot_configs),
        'total_users': len(total_users),
        'total_searches': total_searches,
        'total_qa_pairs': cu_database.qa.count_qa_pairs_per_bots(my_bots)
    }

    instances_data = list(instances_data.values())

    return render_template(
        'enterprise-dashboard.html', filter=filter,
        instances_data=instances_data,
        searches_per_bot=searches_per_bot,
        active_users=active_users,
        top_data_general=top_data_general,
        page='enterprise-dashboard',
        language=cu_tools.get_locale_str())


def _fill_blank_dates(original_data, from_date, to_date):
    full_data = []
    day_count = (to_date - from_date).days + 1
    data_by_date = {data['date']: data['count'] for data in original_data}
    for single_date in (from_date + timedelta(n) for n in range(day_count)):
        date = single_date.strftime('%Y-%m-%d')
        count = (data_by_date[date] if date in data_by_date else 0)
        full_data.append({'date': date, 'count': count})
    return full_data


@stats_blueprint.route('/enterprise-dashboard/<path:bot_name>')
@login_required
def enterprise_dashboard_instance(bot_name):
    filter = cu_tools.get_filter(None, request)
    since = filter['since']

    today = datetime.today()
    if since is None:
        since = today - timedelta(days=365)
        filter['days'] = 365

    bot_config = get_bot_config_by_id(bot_name)
    bot_path = bot_config.get_bot_path()

    instance_data = {
        'bot_id': bot_config.get_bot_name(),
        'display_name': bot_config.get_display_name(),
        'total_users': len(bot_config.get_users()),
        'active_users': 0,  # TODO
        'total_qa_pairs': cu_database.qa.count_qa_pairs_per_bots(
            {bot_config.get_bot_path(): ''})  # TODO have to change this
    }

    # instance data massage
    qa_stats = cu_database.user_actions.get_questions_stats_by_bot(
        bot_id=bot_path, since=since)
    created_qa_stats = [q for q in qa_stats
                        if q['action'] == cu_database.
                        user_actions.ACTION_TYPE_NEW_QUESTION]
    verified_qa_stats = [q for q in qa_stats
                         if q['action'] == cu_database.
                         user_actions.ACTION_TYPE_RESPONSE_VERIFICATION]
    created_issues = cu_database.queue_storage.get_issues_stats_by_bot(
        bot_path=bot_path, since=since)
    resolved_issues = cu_database.queue_storage.get_issues_stats_by_bot(
        bot_path=bot_path, since=since, resolved=True)

    instance_data['activity'] = []  # Chart data

    created_qa_line = build_graph_line_data(
        color_number=0,
        data=_fill_blank_dates(created_qa_stats, since, today),
        label='Created Q & A Pairs',
        x_field='date',
        y_field='count')

    verified_qa_line = build_graph_line_data(
        color_number=1,
        data=_fill_blank_dates(verified_qa_stats, since, today),
        label='Verified Q & A Pairs',
        x_field='date',
        y_field='count')

    created_issues_line = build_graph_line_data(
        color_number=2,
        data=_fill_blank_dates(created_issues, since, today),
        label='Created Issues',
        x_field='date',
        y_field='count')

    resolved_issues_line = build_graph_line_data(
        color_number=3,
        data=_fill_blank_dates(resolved_issues, since, today),
        label='Resolved Issues',
        x_field='date',
        y_field='count')

    instance_data['activity'].append(created_qa_line)
    instance_data['activity'].append(verified_qa_line)
    instance_data['activity'].append(created_issues_line)
    instance_data['activity'].append(resolved_issues_line)

    user_bots = current_user.get_bots_names()
    users = cu_database.users.get_users(bot_config.get_users())
    users_dict = {}
    for user in users:
        if user.get('email', user.get('userid')):
            users_dict[user.get('email', user.get('userid'))] = user
    bot_filter = [{'bot_id': bot_id, 'name': user_bots[bot_id]}
                  for bot_id in user_bots.keys()]

    searches = []
    questions = cu_database.user_actions.get_searches_stats_by_bot(
        bot_id=bot_path, since=since)
    for question in questions:
        question_user_id = question['userid']
        search_vm = {}
        search_vm['question'] = question['question']
        search_vm['user'] = (users_dict[question_user_id].get('full-name')
                             if question_user_id in users_dict
                             else question_user_id)
        search_vm['role'] = _get_role_from_user(bot_config=bot_config,
                                                user_id=question_user_id)
        search_vm['tags'] = (question['tags'] if 'tags' in question else '')
        search_vm['doc'] = question['timestamp']
        searches.append(search_vm)

    instance_data['total_searches'] = len(searches)
    qapairs = []
    questions = list(cu_database.qa.get_questions_since(bot_path, since))
    for question in questions:
        qapair = {}
        question_user_id = ''
        if 'created-by' in question:
            question_user_id = question['created-by']
        elif 'question-user-id' in question:
            question_user_id = question['question-user-id']
        qapair['question'] = question['text']
        qapair['user'] = (users_dict[question_user_id].get('full-name')
                          if question_user_id in users_dict
                          else question_user_id)
        qapair['role'] = _get_role_from_user(bot_config=bot_config,
                                             user_id=question_user_id)
        qapair['tags'] = (question['tags'] if 'tags' in question else '')
        qapair['doc'] = question['created']
        qapairs.append(qapair)

    userslogin = get_login_stats(bot_config, since, users)
    instance_data['active_users'] = len(userslogin)
    return render_template('enterprise-dashboard-instance.html',
                           instance_data=instance_data, filter=filter,
                           bot_filter=bot_filter,
                           searches=searches, qapairs=qapairs,
                           userslogin=userslogin,
                           page='enterprise-dashboard',
                           language=cu_tools.get_locale_str())


def get_login_stats(bot_config, since, users):

    userslogin = []
    userids = [i['email'] for i in users if 'email' in i]
    login_data = cu_database.user_actions.get_top_logins(
        bot_config.get_bot_path(), userids, since=since)
    for user in users:
        user_id = user.get('email')
        if user_id in login_data:
            user_login = {}
            user_login['user'] = user.get('full-name')
            user_login['email'] = user.get('email')
            user_login['lastlogin'] = (login_data[user_id]['last_login'] or '')
            user_login['role'] = _get_role_from_user(bot_config, user_id)
            userslogin.append(user_login)
    return userslogin


def _get_role_from_user(bot_config, user_id):
    role_vm = 'user'
    for role_name, role in bot_config.get_roles().items():
        if user_id in role.get('users'):
            role_vm = role_name
            break
    return role_vm


def build_graph_line_data(color_number, data, label, x_field, y_field):
    current = {
        'data': [],
        'fill': False,
        'label': label,
        'backgroundColor': (COLORS[color_number] % 0.2),
        'borderColor': (COLORS[color_number] % 1),
        'borderWidth': 1,
        'lineTension': 0
    }
    for item in data:
        current['data'].append({
            'x': item[x_field],
            'y': item[y_field]
        })
    return current


@stats_blueprint.route('/instances-manager')
@login_required
def instances_manager():
    filter = cu_tools.get_filter(None, request)

    return render_template(
        'instances-manager.html',
        page='instances-manager',
        filter=filter,
        language=cu_tools.get_locale_str())


@stats_blueprint.route('/render_percent_chart/<path:bot_name>/chart.svg')
def render_percent_chart(bot_name):
    percent = int(request.args.get('percent', 0))
    color = request.args.get('color', '')
    circle_bg_color = request.args.get('circle_bg_color', '')
    response = make_response(create_svg_chart(percent, color, circle_bg_color))
    response.content_type = 'image/svg+xml'
    return response


@stats_blueprint.route(
    '/render_percent_chart/<path:bot_name>/<percent>/<color>/<colc>/chart.png')
def render_percent_chart_png(bot_name, percent, color, colc):
    percent = int(percent)
    circle_bg_color = colc
    png_output = create_png_chart(percent, color, circle_bg_color)
    response = make_response(png_output)
    response.content_type = 'image/png'
    return response


def create_svg_chart(percent, color, circle_bg_color):
    calc_dash_offset = 440 - ((440 * percent) / 100)
    return render_template(
        'percent-chart.svg', dash_offset=int(calc_dash_offset),
        percent=int(percent), color=color, circle_bg_color=circle_bg_color)


def create_png_chart(percent, color, circle_bg_color):
    svg_chart = create_svg_chart(percent, color, circle_bg_color)
    return cairosvg.svg2png(bytestring=svg_chart)


@stats_blueprint.route('/stats/queue_by_date/<path:bot_id>')
@login_required
def queue_by_date(bot_id):
    since = get_since()
    bot_id = bot_id.replace('_', '/')
    activity = cu_database.stats.get_queue_by_date(bot_id, since)
    activity_log = cu_database.stats.get_count_by_date(bot_id, since)
    for key, value in activity_log.items():
        if key in activity:
            activity[key] += value
        else:
            activity[key] = value
    return jsonify(activity)


@stats_blueprint.route('/stats/queue_by_type/<path:bot_id>')
@login_required
def queue_by_type(bot_id):
    since = get_since()
    bot_id = bot_id.replace('_', '/')

    queue_stats = cu_database.stats.get_queue_by_type(bot_id, since)
    replace_none_keys(queue_stats)
    logger.debug(queue_stats)
    chart_data = []
    for key in queue_stats.keys():
        label = cu_tools.queue_type_descriptions.get(
            key, key.replace('_', ' ').capitalize())
        color = cu_tools.queue_type_colors.get(key, 'grey')
        chart_data.append({'value': queue_stats[key],
                           'label': label,
                           'color': color,
                           'key': key})
    return jsonify({'chart': chart_data, 'filter': '?solved=&queue_type=',
                    'display_labels': True})


@stats_blueprint.route('/stats/queue_by_status/<path:bot_id>')
@login_required
def queue_by_status(bot_id):
    since = get_since()
    bot_id = bot_id.replace('_', '/')
    colors_by_state = {'open': 'orange', 'resolved': 'green'}
    queue_stats = cu_database.stats.get_queue_by_status(bot_id, since)
    chart_data = []

    for key in queue_stats.keys():
        if key == 'open':
            filter_value = 'no'
        else:
            filter_value = 'yes'
        label = key.replace('_', ' ').capitalize()
        color = colors_by_state.get(key, 'grey')
        chart_data.append({'value': queue_stats[key],
                           'label': label,
                           'color': color,
                           'key': filter_value})
    return jsonify({'chart': chart_data, 'filter': '?solved=',
                    'display_labels': True})


@stats_blueprint.route('/stats/queue_resolution_time/<path:bot_id>')
@login_required
def queue_resolution_time(bot_id):
    since = get_since()
    bot_id = bot_id.replace('_', '/')
    chart_data = []
    for item in cu_database.stats.get_queue_resolution_time(bot_id, since):
        label = item['text']
        chart_data.append({'value': item['resolution-time'],
                           'label': label})
    return jsonify({'chart': chart_data, 'filter': None,
                    'display_labels': False})


@stats_blueprint.route('/report/<path:bot_name>/<days>')
@login_required
@cu_security.verify_permission(BotConfig.PERM_VIEW_DASHBOARD)
def report(bot_name, days):
    now = datetime.today()
    since = now
    period = now.strftime('%Y/%m/%d')
    if days != usage.WINDOW_DAY:
        since = now - timedelta(days=usage.WINDOW_DAYS[days])
        period = since.strftime('%Y/%m/%d') + ' - ' + period
    report_file_name = 'ActivityReport_%s_%s-%s' % (
        bot_name, since.strftime('%m%d%Y'), now.strftime('%m%d%Y'))

    report_data = get_report_data(bot_name, days)
    for data in report_data.get('overview'):
        data['name'] = cu_tools.usage_translations[data['name']]
    for data in report_data.get('open_issues_list'):
        data['name'] = cu_tools.usage_translations[data['name']]
    for data in report_data.get('resolved_issues'):
        data['name'] = cu_tools.usage_translations[data['name']]
    return render_template(
        'report-modal.html',
        url_root=request.url_root,
        report_data=report_data,
        report_file_name=report_file_name)


def get_report_data(bot_name, days):
    engage = cu_tools.init_engage(bot_name, current_user=current_user)
    data = usage.get_report_data(engage, days)

    data['open_issues'] = label_by_quantity(
        data['open_issues'],
        _('There are <b>no</b> open issues.'),
        ngettext('There is <b>%(num)s</b> open issue.',
                 'There are <b>%(num)s</b> open issues.',
                 num=data['open_issues']))

    data['answered_successfully'] = label_by_quantity(
        data['answered_successfully'],
        _('There were <b>no</b> questions answered successfully'),
        ngettext('There were <b>%(num)s</b> question answered successfully',
                 'There were <b>%(num)s</b> questions answered successfully',
                 num=data['answered_successfully']))

    data['closed_issues'] = label_by_quantity(
        data['closed_issues'],
        _('<b>None</b> of those questions required Expert intervention'),
        _('<b>%s</b> of those questions required Expert intervention')
        % data['closed_issues'])

    current_users = data['current_users']

    data['current_users']['users'] = label_by_quantity(
        current_users['users'],
        _('<b>No</b> Users'),
        ngettext('<b>%(num)s</b> User', '<b>%(num)s</b> Users',
                 num=current_users['users']))

    data['current_users']['admins'] = label_by_quantity(
        current_users['admins'],
        _('<b>No</b> Admins'),
        ngettext('<b>%(num)s</b> Admin', '<b>%(num)s</b> Admins',
                 num=current_users['admins']))

    data['current_users']['experts'] = label_by_quantity(
        current_users['experts'],
        _('<b>No</b> Experts'),
        ngettext('<b>%(num)s</b> Expert', '<b>%(num)s</b> Experts',
                 num=current_users['experts']))

    time_data = {
        usage.WINDOW_DAY: _('today'),
        usage.WINDOW_WEEK: _('the past week'),
        usage.WINDOW_FORTNIGHT: _('the past two weeks'),
        usage.WINDOW_MONTH: _('the past month'),
        usage.WINDOW_YEAR: _('the past year')
    }
    time = time_data[days]
    now = datetime.today()
    period = now.strftime('%Y/%m/%d')
    if days != usage.WINDOW_DAY:
        since = now - timedelta(days=usage.WINDOW_DAYS[days])
        period = since.strftime('%Y/%m/%d') + ' - ' + period
    else:
        since = now

    prefixes = {
        usage.WINDOW_DAY: _('Today'),
        usage.WINDOW_WEEK: _('In the past week'),
        usage.WINDOW_FORTNIGHT: _('In the past two weeks'),
        usage.WINDOW_MONTH: _('In the past month'),
        usage.WINDOW_YEAR: _('In the past year')
    }
    prefix = prefixes[days]

    avg_time_label = label_by_quantity(
        data['average_resolution_time'],
        _('%s, the average time to resolve an issue was '
          '<b>less than 1 day</b>') % prefix,
        ngettext('%(prefix)s, the average time to resolve an issue was '
                 '<b>%(num)s day</b>',
                 '%(prefix)s, the average time to resolve an issue was '
                 '<b>%(num)s days</b>',
                 prefix=prefix, num=data['average_resolution_time']))

    max_time_label = label_by_quantity(
        data['maximum_resolution_time'],
        _(' and the maximum time to resolve an issue was '
          '<b>less than 1 day</b>'),
        ngettext(' and the maximum time to resolve an issue was '
                 '<b>%(num)s day</b>.',
                 ' and the maximum time to resolve an issue was '
                 '<b>%(num)s days</b>.',
                 num=data['maximum_resolution_time']))

    data['average_resolution_label'] = avg_time_label + max_time_label

    prior_prefixes = {
        usage.WINDOW_DAY: _('Yesterday'),
        usage.WINDOW_WEEK: _('During the previous week'),
        usage.WINDOW_FORTNIGHT: _('During the previous weeks'),
        usage.WINDOW_MONTH: _('During the previous month'),
        usage.WINDOW_YEAR: _('During the previous year')
    }
    prior_prefix = prior_prefixes[days]

    prior_avg_time_label = label_by_quantity(
        data['prior_average'],
        _('%s, the average time to resolve an issue was '
          '<b>less than 1 day</b>') % prior_prefix,
        ngettext('%(prefix)s, the average time to resolve an issue was '
                 '<b>%(num)s day</b>',
                 '%(prefix)s, the average time to resolve an issue was '
                 '<b>%(num)s days</b>',
                 prefix=prefix, num=data['prior_average']))

    prior_max_time_label = label_by_quantity(
        data['prior_maximum'],
        _(' and the maximum time to resolve an issue was '
          '<b>less than 1 day</b>'),
        ngettext(' and the maximum time to resolve an issue was '
                 '<b>%(num)s day</b>.',
                 ' and the maximum time to resolve an issue was '
                 '<b>%(num)s days</b>.', num=data['prior_maximum']))

    data['prior_average_label'] = prior_avg_time_label + prior_max_time_label

    bot_config = get_bot_config_by_id(bot_name)
    data['time'] = time
    data['period'] = period
    data['bot_name'] = bot_name
    data['bot_display_name'] = bot_config.get_display_name()

    return data


def label_by_quantity(value, label_zero, label_many):
    if value == 0:
        return label_zero
    else:
        return label_many


@stats_blueprint.route('/user-history/<path:bot_name>')
@login_required
def user_history_from(bot_name):
    userid = request.args.get('userid', None)
    return user_activity(userid, bot_name)


@stats_blueprint.route('/user-history')
@login_required
def user_history():
    return user_activity()


def user_activity(userid=None, bot_name=None):
    filter = cu_tools.get_filter(bot_name, request)

    title_activity = _('My Activity')
    if userid:
        name = cu_tools.get_user_name(userid)
        if name:
            title_activity = _('%s\'s Activity') % name
        else:
            title_activity = _('User Activity for %s') % userid
    else:
        userid = current_user.get_id()

    if bot_name:
        bot_id = current_user.get_bot_path_by_name(bot_name)

    # remove auth0 from email
    if '|' in userid:
        userid = userid.split('|')[1]
    user_questions = list(cu_database.user_actions.get_by_userid(
        userid, bot=bot_id,
        since=filter['since'], descending=(filter['direction'] == '-1')))

    stats = {
        'count_questions': len(user_questions),
        'pinned_responses': 0,
        'notes_saved': 0,
        'up_votes': 0,
        'down_votes': 0}

    for question in user_questions:
        question['id'] = uuid_to_str(question['_id']).lstrip('#U')
        if 'timestamp' in question:
            question['time_ago'] = cu_tools.get_elapsed_desc(
                question['timestamp'], cu_tools.get_locale_str())
            question['timestamp_ago'] = cu_tools.format_timestamp(
                question['timestamp'])
        if 'actions' in question:
            for action in question['actions']:
                if 'note' in action:
                    question['have_notes'] = True
                    stats['notes_saved'] += 1
                    action['note']['time_ago'] = cu_tools.get_elapsed_desc(
                        action['note']['timestamp'], cu_tools.get_locale_str())
                if 'comment' in action:
                    question['have_comment'] = True
                    action['comment']['time_ago'] = \
                        cu_tools.get_elapsed_desc(
                            action['comment']['timestamp'],
                            cu_tools.get_locale_str())
                if 'thumbs_up' in action:
                    question['have_thumbs_up'] = True
                    stats['up_votes'] += 1
                if 'thumbs_down' in action:
                    question['have_thumbs_down'] = True
                    stats['down_votes'] += 1
                if 'pinned' in action:
                    question['have_pinned'] = True
                    stats['pinned_responses'] += 1
                if 'response_verifier' in action:
                    action['verifier'] = action['response_verifier']
                if 'response_verifier_time' in action:
                    action['verify_time'] = cu_tools.get_elapsed_desc(
                        action['response_verifier_time'],
                        cu_tools.get_locale_str())
                if 'action' in question and 'action_description' in action:
                    question['action_description'] = \
                        action['action_description']

    return render_template('user-history.html', page='user-history',
                           user_questions=user_questions, stats=stats,
                           filter=filter,
                           user=userid, title_activity=title_activity)


def parse_responses(responses):
    data = engage_tools.get_inner_data(responses)
    res = ''
    for response in data:
        if response['text'] != '':
            res += response['text']
        else:
            res += _('Empty Response')
        res += '\n'

    if len(res) != 0:
        return {'up': res,
                'down': engage_tools.text_to_html(res),
                'shows': True}

    return {'up': None, 'down': None, 'shows': False}
