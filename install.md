Files in the server
-------------------

Follow https://github.com/ingestor/slackbot/wiki/Installation-with-Nginx-uwsgi-flask but with the files adapated for this app

/etc/systemd/system/cu-ingestor.service 

```
[Unit]

Description=uWSGI instance to run slackbot
After=network.target

[Service]
User=gonzalo
Group=www-data
WorkingDirectory=/src/slackbot
# EnvironmentFile=/src/slackbot/service-env
ExecStart=/src/slackbot/env/bin/uwsgi --ini slackbot.ini

[Install]
WantedBy=multi-user.target
```

/etc/nginx/sites-available/cu-ingestor

```
server {
    listen 4242 ssl;
    server_name control-unit.ingestor.io;
    ssl_certificate /srv/certs/ingestor.ai/chain.pem;
    ssl_certificate_key /srv/certs/ingestor.ai/key.pem;
    ssl_protocols       TLSv1 TLSv1.1 TLSv1.2;

    location / {
        include uwsgi_params;
        uwsgi_pass unix:/src/cu-app/cu-app.sock;
    }
}
```


