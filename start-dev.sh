# export GOOGLE_APPLICATION_CREDENTIALS=cert/Control-Unit-5b442b54e43d.json
export GOOGLE_APPLICATION_CREDENTIALS=cert/dataflow-test_gcp-creds.json
export SEARCH_API_HOST=https://platform-sli-search-zwr2bob4pa-ue.a.run.app
export LOGGER_LEVEL=DEBUG
export MONGO_HOST=db.dev.ingestor.io
export REDIS_URL=redis://127.0.0.1:6379
export AI_JOB_HOST=https://ingestor-legacy-qa.ue.r.appspot.com
export RANK_SEARCH_HOST=https://ai-smart-search.gcp-legacy-qa.ingestor.io
export ELASTICSEARCH_URL=http://10.142.0.48:9200
source ./cert/auth0.sh
source ./cert/email.sh
python3 app.py
