# Knowledge application for the Ingestor suite

[![coverage report](https://gitlab.com/ingestor/app-services/cu-app/badges/master/coverage.svg)](https://gitlab.com/ingestor/apps-services/cu-app/commits/master)

This application was previously known as Control Unit

# Setup

## System dependencies:

We need `python3-devel` for building some python modules, and we use the `ffmpeg` package for text to speech functionality.

On Fedora

```
$ sudo dnf -y install python3-devel
$ sudo dnf -y install https://download1.rpmfusion.org/free/fedora/rpmfusion-free-release-$(rpm -E %fedora).noarch.rpm
$ sudo dnf -y install https://download1.rpmfusion.org/nonfree/fedora/rpmfusion-nonfree-release-$(rpm -E %fedora).noarch.rpm
$ sudo dnf install ffmpeg
```

On Ubuntu

```
$ sudo apt install python3-dev
$ sudo apt install ffmpeg
```

Then, use virtualenv.

## On Fedora < 31:

```
$ sudo dnf install python3-virtualenv
$ virtualenv-3 env
$ source ./env/bin/activate
$ pip install -r requirements.txt
```

On Fedora >= 30 virtualenv-3 is renamed to virtualenv

## On Ubuntu 16 or Fedora >= 31:

```
$ sudo dnf install virtualenv python3-pip
$ virtualenv -p python3 env
$ source ./env/bin/activate
$ pip3 install -r requirements.txt
```

Note: use apt on Ubuntu systems.

Virtualenv installation can change on other linux distributions.

## On iOS

Install homebrew, a package manager for macOS. Follow this guide for more explanation: https://docs.python-guide.org/starting/install3/osx/

After a successful installation of homebrew, let’s use homebrew to install python3:
```
$ brew install python3
```
Confirm if python 3 is installed:
```
$ which python3
```
Check if you have pip installed:
```
$ pip -v
```
To Install pip3:
```
$ curl https://bootstrap.pypa.io/get-pip.py -o get-pip.py
```
Confirm if pip 3 is installed:
```
$ which pip3
```

# Environment variables

* MONGO_HOST: the database server where will connect to get the data.
By default points to our production database `db.ingestor.io` and for development
should be set to `db.dev.ingestor.io`

NOTE: The env variable MONGO_DATABASEis not needed anymore, because all the environments are using `kbprod` as database name (in the past, we used `kbdev` for development)

* LOGGER_LEVEL: (Optional) by default, logging level is WARNING, only exceptions and errors
will be logged. If you need see all the log messages, set these env variable to `DEBUG`

logging leveles defined here: https://docs.python.org/3/library/logging.html#logging-levels

* SEARCH_API_HOST: the url to the search API service, by example `http://localhost:5050`
The search api provides all the logic that depends on ingestor-nl functionalities
and is needed to do smart search. You can use the instance running on heroku
that is using the dev database `https://ingestor-search-api.herokuapp.com/`

* JWT_SECRET: to secure the requests to the APIs, the apps and services should share
the same JWT_SECRET environment value. If the env variable is not setup, wee use a default
value that is only useful for testing purposes.

* REDIS_URL: (Optional) If available, Redis is used to cache users, botconfig and config data.
A Redis url have the format `redis://127.0.0.1`

* DEFAULT_INSTANCE: The name of a default instance when is applicable. The instance should be configuired as public. This is useful in cases like the 'covid' instance where the home page is the ask page and is not requested a login to use the app.

## SES email service environment variables

Sending emails using SES needs the following env variables: `SENDER`, `SENDERNAME`, `SERVER_SMTP`, `USERNAME_SMTP` and `PASSWORD_SMTP`

## TWILIO

These env variables are needed to use twilio `TWILIO_ACCOUNT_SID`, `TWILIO_AUTH_CODE`, `TWILIO_PHONE_NUMBER`

## Speech to text

GOOGLE_APPLICATION_CREDENTIALS=/src/cu-app/cert/xxxxxx-xxxx-xxxxxxxxx.json
Path to the file with the certifiate used to connect to the google API

## Google services

You need the files `cert/Control-Unit-5b442b54e43d.json`, `cert/client_id.json` and
`cert/google_client_data.json`

# Certificates

To connect to the mongo database, you will need the files `cert/client.pem` and `cert/CA.crt`

To access S3 the app needs the certificate `cert/aws-access.json`

If you don't have them, request them to the project admin

# Systemd services

The cu-app run as a systemctl unit `cu-ingestor`

You can check status doing:
```
$ sudo systemctl status cu-ingestor
```
The staus show on the second line the path of the file `cu-ingestor.service`

That file is not on git, because have the nev variables needed to run the app defined

And read the log doing:
```
$ sudo journalctl --system -u cu-ingestor -f
```

# Docker alternative for starting up the application

You can startup the application for development using a docker container following the steps below.

## Steps needed

- Install [Docker](https://docs.docker.com/install/linux/docker-ce/ubuntu/)
- Make this project your working directory
- Create a file named `cu-app.env` and add inside all the ENV variables mentioned before
- Have an .ssh key pair that matches the ones in your repository account
- Build the docker image with this command:
```bash
    docker build --build-arg ssh_prv_key="$(cat ~/.ssh/id_rsa)" \
             --build-arg ssh_pub_key="$(cat ~/.ssh/id_rsa.pub)" \
             -t cu-app-local .
```
- Create and start a container based on the previus image with this command:
```bash
    docker run -d -p 8000:80 --env-file ./cu-app.env \
             --name cu-app-local --restart unless-stopped \
             -v $(pwd):/app -v $(pwd)/cu-app.ini:/app/uwsgi.ini cu-app-local
```
- And we are done! You can access your application at `localhost:8000`. You can change the port and other things modifying the `docker run` command as you like

# Maintenance

## Update the app

We addeed a file `bin/update-cu.sh` to update the app on the server environment.

A branch can be added as a parameter to update the app to that branch.

The script update deoendencies, set permissions and restart the systemd service
