# -*- coding: utf-8 -*-
'''
Tools for the control unit for ingestor
'''

# Copyright Gonzalo Odiard, Ingestor 2018

# This file is part of Ingestor's Knowbot platform and is copyright
# and a valuable trade secret of Ingestor, Inc.

from base64 import b64encode
import logging
import math
import os
from datetime import datetime, timedelta
import cu_database
import json

from cu_authentication import current_user
from flask_babel import lazy_gettext, current_app, _
from flask import request, session, g

from engage.botconfig import BotConfig, get_bot_config_by_name, \
    get_all_bots_config
from engage.engage import Engage
from ingestor_storage import STATUS_REQUIRE_ACTION
from ingestor_storage import STATUS_NEEDS_REVIEW
from ingestor_storage import STATUS_VERIFIED_INPUT
from ingestor_storage import object_id_to_path
from ingestor_storage.actions import UserActions, Library
from ingestor_storage import Users
from engage import engage_tools
from engage.email_tools import send_simple_email, send_sms
from babel.dates import format_timedelta

logging.basicConfig(level=os.getenv('LOGGER_LEVEL', logging.WARNING))
logger = logging.getLogger('cu_tools')

PERM_MODIFY_USER_SETTINGS = 'PERM_MODIFY_USER_SETTINGS'
ENTRIES_BY_PAGE = 10
MAX_PAGINATION_LINKS = 10
icons = {STATUS_REQUIRE_ACTION: 'minus-circle',
         STATUS_NEEDS_REVIEW: 'exclamation-circle',
         STATUS_VERIFIED_INPUT: 'check-circle'}
desc_status = {STATUS_REQUIRE_ACTION: lazy_gettext('Needs answer'),
               STATUS_NEEDS_REVIEW: lazy_gettext('Needs review'),
               STATUS_VERIFIED_INPUT: lazy_gettext('Verified')}
colors = {STATUS_REQUIRE_ACTION: '#f0003d',
          STATUS_NEEDS_REVIEW: '#ffa700',
          STATUS_VERIFIED_INPUT: '#65b544'}

queue_colors = {engage_tools.QUEUE_TYPE_TOO_VAGUE: '#ffa700',
                engage_tools.QUEUE_TYPE_NEEDS_REVIEW: '#ffa700',
                engage_tools.QUEUE_TYPE_NO_RESPONSE: '#f0003d',
                engage_tools.QUEUE_TYPE_LIBRARY_ONLY: '#f0003d',
                engage_tools.QUEUE_TYPE_REACTION: '#3c7fff',
                engage_tools.QUEUE_TYPE_MISUNDERSTOOD: '#f5dc0d'}

# unicode chars for the emajis
queue_reactions = {'disappointed': 'fa-sad-tear',
                   'frown': 'fa-frown',
                   'meh': 'fa-meh',
                   'smile': 'fa-smile'}

config_texts_icons = {':black_square_button:': '&#x25A0',
                      ':downarrow:': '&#x2B07',
                      ':neutral_face:': '&#x1F610',
                      ':more:': '&#x002B',
                      ':shrug:': '¯\\_(ツ)_/¯'}

usage_translations = {
    'User interactions': lazy_gettext('User interactions'),
    'Unique users': lazy_gettext('Unique users'),
    'Unique questions asked': lazy_gettext('Unique questions asked'),
    'Total issues created': lazy_gettext('Total issues created'),
    'Ambiguous/vague questions': lazy_gettext('Ambiguous/vague questions'),
    'Questions with no answer': lazy_gettext('Questions with no answer'),
    'Questions misunderstood': lazy_gettext('Questions misunderstood'),
    'Feedbacks': lazy_gettext('Feedbacks'),
    'Questions with no answers': lazy_gettext('Questions with no answers'),
    'Thumbs up': lazy_gettext('Thumbs up'),
    'Thumbs down': lazy_gettext('Thumbs down'),
    'Q & A pair verified': lazy_gettext('Q & A pair verified'),
}

homepage_translations = {
    'Ask': lazy_gettext('Ask'),
    'Issues': lazy_gettext('Issues'),
    'Q&A': lazy_gettext('Q&A'),
    'Library': lazy_gettext('Library'),
    'Dashboard': lazy_gettext('Dashboard'),
    'Settings': lazy_gettext('Settings'),
    'Training': lazy_gettext('Training'),
    'Default': lazy_gettext('Default')
}

queue_type_descriptions = {
    engage_tools.QUEUE_TYPE_TOO_VAGUE: lazy_gettext('Too vague question'),
    engage_tools.QUEUE_TYPE_NEEDS_REVIEW: lazy_gettext('Needs review'),
    engage_tools.QUEUE_TYPE_NO_RESPONSE: lazy_gettext('No response'),
    engage_tools.QUEUE_TYPE_REACTION: lazy_gettext('Reaction'),
    engage_tools.QUEUE_TYPE_LIBRARY_ONLY:
    lazy_gettext('Library only response'),
    engage_tools.QUEUE_TYPE_MISUNDERSTOOD: lazy_gettext("Can't find answer")}

queue_type_colors = {
    engage_tools.QUEUE_TYPE_TOO_VAGUE: '#ffa700',
    engage_tools.QUEUE_TYPE_NEEDS_REVIEW: '#65b544',
    engage_tools.QUEUE_TYPE_NO_RESPONSE: '#f0003d',
    engage_tools.QUEUE_TYPE_REACTION: '#3c7fff',
    engage_tools.QUEUE_TYPE_MISUNDERSTOOD: '#f5dc0d'}

file_type_extensions = {
    'text/html': 'HTML',
    'application/pdf': 'PDF',
    'application/vnd.openxmlformats-officedocument.wordprocessingml.document':
        'DOCX'}


doc_statuses = {
    Library.DOC_STATUS_INGESTED: lazy_gettext('Ready'),
    Library.DOC_STATUS_UPLOADED: lazy_gettext('Queued')
}


doc_badges = {
    Library.DOC_STATUS_INGESTED: {
        'label': lazy_gettext('Ready'),
        'style': 'success'
    },
    Library.DOC_STATUS_UPLOADED: {
        'label': lazy_gettext('Queued'),
        'style': 'primary'
    }
}

roles_description = {
    'admin': lazy_gettext('Admin'),
    'expert': lazy_gettext('Expert'),
    'user': lazy_gettext('User')
}


action_types = {
    UserActions.ACTION_TYPE_LOGIN:
        lazy_gettext("Login"),
    UserActions.ACTION_TYPE_LOGOUT:
        lazy_gettext("Logout"),
    UserActions.ACTION_TYPE_NEW_QUESTION:
        lazy_gettext("New Q & A "),
    UserActions.ACTION_TYPE_QUESTION_MODIFICATION:
        lazy_gettext("Question Modification"),
    UserActions.ACTION_TYPE_QUESTION_DELETION:
        lazy_gettext("Question Deleted"),
    UserActions.ACTION_TYPE_NEW_RESPONSE:
        lazy_gettext("New Response"),
    UserActions.ACTION_TYPE_RESPONSE_MODIFICATION:
        lazy_gettext("Response Modification"),
    UserActions.ACTION_TYPE_RESPONSE_DELETION:
        lazy_gettext("Response Deleted"),
    UserActions.ACTION_TYPE_RESPONSE_VERIFICATION:
        lazy_gettext("Response Verification"),
    UserActions.ACTION_TYPE_DOCUMENT_UPLOAD:
        lazy_gettext("Document Uploaded"),
    UserActions.ACTION_TYPE_DOCUMENT_DOWNLOAD:
        lazy_gettext("Document Downloaded"),
    UserActions.ACTION_TYPE_DOCUMENT_REMOVAL:
        lazy_gettext("Document Removed"),
    UserActions.ACTION_TYPE_NEW_USER:
        lazy_gettext("New User"),
    UserActions.ACTION_TYPE_USER_MODIFICATION:
        lazy_gettext("User Modification"),
    UserActions.ACTION_TYPE_USER_DELETION:
        lazy_gettext("User Deleted"),
    UserActions.ACTION_TYPE_NEW_ROLE:
        lazy_gettext("New Role"),
    UserActions.ACTION_TYPE_ROLE_MODIFICATION:
        lazy_gettext("Roles Modification"),
    UserActions.ACTION_TYPE_ROLE_DELETION:
        lazy_gettext("Role Deleted"),
    UserActions.ACTION_TYPE_AVATAR_UPLOADED:
        lazy_gettext("Avatar Upload"),
    UserActions.ACTION_TYPE_BOT_AVATAR_UPLOADED:
        lazy_gettext("Bot Avatar Upload"),
    UserActions.ACTION_TYPE_PASSWORD_MODIFICATION:
        lazy_gettext("Password Modification"),
    UserActions.ACTION_TYPE_SETTINGS_MODIFICATION:
        lazy_gettext("Settings Modification"),
    UserActions.ACTION_TYPE_INSTANCE_SETTINGS_MODIFICATION:
        lazy_gettext("Instance Settings Modification"),
    UserActions.ACTION_TYPE_NOTIFICATIONS_SETTINGS_MODIFICATION:
        lazy_gettext("Notifications Settings Modification"),
    UserActions.ACTION_TYPE_SECURITY_SETTINGS_MODIFICATION:
        lazy_gettext("Security Settings Modification"),
    UserActions.ACTION_TYPE_SOCIAL_SETTINGS_MODIFICATION:
        lazy_gettext("Social Settings Modification"),
    UserActions.ACTION_TYPE_INGESTOR_SETTINGS_MODIFICATION:
        lazy_gettext("Ingestor Settings Modification"),
    UserActions.ACTION_TYPE_USER_IDENTITY_SETTINGS_MODIFICATION:
        lazy_gettext("User Identity Settings Modification")
}

permissions = {
    BotConfig.PERM_VIEW_DASHBOARD: {
        'name': lazy_gettext('View Dashboard'),
        'description': lazy_gettext('Permission to view dashboards')},
    BotConfig.PERM_VIEW_ISSUES: {
        'name': lazy_gettext('View Issues'),
        'description': lazy_gettext('Permission to view issues')},
    BotConfig.PERM_MODIFY_ISSUES: {
        'name': lazy_gettext('Modify Issues'),
        'description':
            lazy_gettext('Permission to add, delete or modify issues')},
    BotConfig.PERM_VIEW_LIBRARY: {
        'name': lazy_gettext('View Library'),
        'description':
            lazy_gettext('Permission to view library items')},
    BotConfig.PERM_MODIFY_LIBRARY: {
        'name': lazy_gettext('Modify Library'),
        'description':
            lazy_gettext('Permission to add, delete or modify library items')},
    BotConfig.PERM_VIEW_QA: {
        'name': lazy_gettext('View Q & A'),
        'description': lazy_gettext('Permission to view Q & A items')},
    BotConfig.PERM_MODIFY_QA: {
        'name': lazy_gettext('Modify Q & A'),
        'description': lazy_gettext(
            'Permission to add, delete or modify Q & A items')},
    BotConfig.PERM_MODIFY_SETTINGS: {
        'name': lazy_gettext('Modify Settings'),
        'description': lazy_gettext('Permission to modify Settings')},
    BotConfig.PERM_VERIFY: {
        'name': lazy_gettext('Verify'),
        'description': lazy_gettext('Permission to verify changes')},
    BotConfig.PERM_OVER_CONTENT_PROTECT: {
        'name': lazy_gettext('Overwrite Content Protection'),
        'description': lazy_gettext('Overwrite content protection')},
    PERM_MODIFY_USER_SETTINGS: {
        'name': lazy_gettext('Edit user settings'),
        'description': lazy_gettext('Permission to edit user settings')}
}

BOTCONFIG_ANSWER_SEARCH = 'answer_search'
BOTCONFIG_LIBRARY_SEARCH = 'library_search'


def load_data_from_json(jsonp_ath):
    res = {}
    with open(jsonp_ath) as data:
        res = json.load(data)
    return res


def clean_id(id, id_type=None):
    if id_type is not None:
        id = id.replace('/%s/' % id_type, '')
    id = id.replace(":@", "")
    id = id.replace("/", "_")
    return id


def format_comment_text(text):
    text = text.replace('"', '\"')
    text = text.replace("'", "\'")
    return text


def format_config_text(text):
    text = text.replace('\\n', '<br/>')
    text = text.replace('\n', '<br/>')
    text = text.replace('"', '')
    for icon in config_texts_icons.keys():
        text = text.replace(icon, config_texts_icons[icon])
    return text


def format_timestamp(timestamp):
    if isinstance(timestamp, datetime):
        return timestamp.strftime('%b %d, %Y - %H:%M')
    else:
        return ''


def format_bot_config(bot_config):
    # process all the texts for the ui to have valid html
    formated = BotConfig()
    formated._display_name = format_config_text(bot_config.get_display_name())
    formated._help_list = format_config_text(bot_config.get_help_list())
    formated._hmm_msg = format_config_text(bot_config.get_hmm_msg())
    formated._no_answer = format_config_text(bot_config.get_no_answer())

    formated._misunderstood_msg = \
        format_config_text(bot_config.get_misunderstood_msg())
    formated._inexact_match_msg = \
        format_config_text(bot_config.get_inexact_match_msg())
    formated._too_vague_msg = \
        format_config_text(bot_config.get_too_vague_msg())
    formated._bookmark_msg = format_config_text(bot_config.get_bookmark_msg())

    formated._smile_msg = format_config_text(bot_config.get_smile_msg())
    formated._meh_msg = format_config_text(bot_config.get_meh_msg())
    formated._frown_msg = format_config_text(bot_config.get_frown_msg())
    formated._react_button = format_config_text(bot_config.get_react_button())

    formated._ask_button = format_config_text(bot_config.get_ask_button())
    formated._more_button = format_config_text(bot_config.get_more_button())
    formated._show_more = format_config_text(bot_config.get_show_more())
    formated._help_button = format_config_text(bot_config.get_help_button())
    formated._help_msg = format_config_text(bot_config.get_help_msg())

    formated._sms_help_msg = format_config_text(bot_config.get_sms_help_msg())
    formated._sms_switch_msg = \
        format_config_text(bot_config.get_sms_switch_msg())

    formated._welcome_msg = format_config_text(bot_config.get_welcome_msg())
    formated._flag_text = format_config_text(bot_config.get_flag_text())
    formated._comment_msg = format_config_text(bot_config.get_comment_msg())
    formated._comment_confirmation = \
        format_config_text(bot_config.get_comment_confirmation())

    formated._share_text = format_config_text(bot_config.get_share_text())
    formated._share_msg = format_config_text(bot_config.get_share_msg())

    formated._note_text = format_config_text(bot_config.get_note_text())
    formated._notes_msg = format_config_text(bot_config.get_notes_msg())
    formated._note_confirmation = \
        format_config_text(bot_config.get_note_confirmation())

    formated._help_action = bot_config.get_help_action()
    formated._ask_preamble = \
        format_config_text(bot_config.get_ask_preamble())

    formated._disclaimer = \
        format_config_text(bot_config.get_disclaimer())

    formated._answer_search = bot_config.get_answer_search()
    formated._library_search = bot_config.get_library_search()
    formated._question_search = bot_config.get_question_search()

    return formated


def prepare_pages_array(quantity, actual_page):
    # based in the quantity of entries, prepare a array of page numbers
    # to use for links in the pagination ui.
    # add a link "next" and "prev" too
    cant_pages = math.ceil(quantity / ENTRIES_BY_PAGE)
    if cant_pages == 1:
        return []
    if quantity <= (ENTRIES_BY_PAGE * MAX_PAGINATION_LINKS):
        return list(range(cant_pages))
    min_page = max(0, actual_page - int(MAX_PAGINATION_LINKS / 2))
    max_page = min_page + MAX_PAGINATION_LINKS
    if max_page > cant_pages:
        max_page = cant_pages
        min_page = max_page - MAX_PAGINATION_LINKS
    pages = []
    if min_page > 0:
        pages.append('prev')
    pages.extend(range(min_page, max_page))
    if max_page < cant_pages:
        pages.append('next')
    return pages


def get_userid_and_bot_path(current_user, bot_name):
    if current_user.is_authenticated:
        bot_path = current_user.get_bot_path_by_name(bot_name)
        userid = current_user.get_id()
    else:
        bot_path = cu_database.public_bot.get_path_by_id(bot_name)
        userid = 'non authenticated'
    return userid, bot_path


def init_engage(bot_name, current_user=None, token=None):
    '''
    Init Engage object

    Parameters:

    bot_name : str
    as 'alley'

    token: in the initial stage, we only set the token
    from the get_credentials calls

    current_user : cu.users.User
    if available, take token and bot_id
    '''
    """
    This wrapper update is necessary so we dont overwrite all engage.
    The only place that uses token in engage is in _request_json
    so we wrap an api key as a token and make sure in that method to
    unwrap it before using the api key.
    """
    wrapper = current_user.get_token()
    if current_user.get_api_key():
        wrapper = ('api_key', current_user.get_api_key())
    return Engage(token=wrapper, bot_name=bot_name)


def _get_user_list_by_role(bot_config, role):
    if role == BotConfig.USER_ROLE_ADMIN:
        return bot_config.get_administrators()
    elif role == BotConfig.USER_ROLE_EXPERT:
        return bot_config.get_experts()
    elif role == BotConfig.USER_ROLE_USER:
        return bot_config.get_users()
    else:
        logger.error('_get_user_list_by_role: role %s invalid' % role)
        return []


def is_ingestor_user(user_id):
    if '|' in user_id:
        user_id = user_id[user_id.rfind('|') + 1:]
    return user_id in cu_database.config.get_ingestor_users()


def check_bot_name_exists(bot_name):
    bot_names = []
    for bot in get_all_bots_config():
        bot_names.append(bot.get_bot_name())

    return bot_name in bot_names


def replace_http_by_https(url):
    if 'http://' in url:
        url = url.replace('http://', 'https://')
    return url


def get_persistent_secret_key():
    if os.getenv('JWT_SECRET'):
        return os.getenv('JWT_SECRET')
    # get random key from a file if exist
    SESSION_KEY_FILE = '/tmp/cu_session_secret_key'
    if os.path.isfile(SESSION_KEY_FILE):
        with open(SESSION_KEY_FILE) as f:
            secret_key = f.read()
    else:
        secret_key = b64encode(os.urandom(64)).decode('utf-8')
        with open(SESSION_KEY_FILE, 'w') as f:
            f.write(secret_key)
    return secret_key


def get_file_type_by_extension(extension):
    for file_type in file_type_extensions.keys():
        if file_type_extensions[file_type] == extension:
            return file_type
    return None


def get_filter(bot_name, request):
    try:
        active_page = int(request.args.get('page', '0'))
        if active_page < 0:
            active_page = 0
    except Exception:
        active_page = 0

    text_filter = request.args.get('text_filter', '')
    text = text_filter
    if text_filter == '':
        text_filter = None
    else:
        text_filter = text_filter.strip()

    status = request.args.get('status', '')
    if status == '':
        status = None
    else:
        status = int(status)

    tag = request.args.get('tag')
    queue_type = request.args.get('queue_type', '')
    solved = request.args.get('solved', 'no')

    # direction: descending = -1, ascending = 1
    direction = request.args.get('direction', '-1')

    since_param = request.args.get('since', '9999')
    since = None
    try:
        days = int(since_param)
        if since_param != '9999':
            since = datetime.today() - timedelta(days=days)
    except Exception:
        logger.error('invalid param since=%s' % since_param)

    if bot_name and current_user.is_authenticated:
        user_role = current_user.get_role_at_bot(bot_name)
        # TODO: avoid error bot display name
        # bot_display_name = current_user.get_bots_names().get(
        #     bot_name, bot_name)
        bot_display_name = ''
    else:
        user_role = 'user'
        bot_display_name = None

    # day is converted to a datetime
    date_param = request.args.get('date')
    date = None
    if date_param:
        date = datetime.strptime(date_param, '%Y-%m-%d')

    quantity = request.args.get('quantity')
    if quantity is not None:
        try:
            quantity = int(quantity)
        except Exception:
            logger.error('invalid param quantity=%s' % quantity)
            quantity = None

    position = request.args.get('position')
    if position is not None:
        try:
            position = int(position)
        except Exception:
            logger.error('invalid param position=%s' % position)
            position = None

    bot_config = get_bot_config(bot_name)

    filter = {}
    filter['text_filter'] = text_filter
    filter['text'] = text
    filter['status'] = status
    filter['queue_type'] = queue_type
    filter['solved'] = solved
    filter['since'] = since
    filter['tag'] = tag
    filter['days'] = days
    filter['date'] = date
    filter['active_page'] = active_page
    filter['direction'] = direction
    filter['bot'] = bot_name
    filter['bot_name'] = bot_display_name
    filter['user_role'] = user_role
    filter['quantity'] = quantity
    filter['position'] = position
    filter['question_search'] = bot_config.get_question_search() == 'true'
    filter['answer_search'] = bot_config.get_answer_search() == 'true'
    filter['library_search'] = bot_config.get_library_search() == 'true'
    filter['bot_config'] = bot_config

    # filter for the library
    filter['file_type_ext'] = request.args.get('file_type_ext')

    return filter


def is_iphone(str_user_agent):
    is_iphone = 'iPhone' in str_user_agent
    is_iphone = is_iphone or 'iPad' in str_user_agent
    is_iphone = is_iphone or 'iPod' in str_user_agent
    return is_iphone


def send_message(recipient, subject, title, text, url_root,
                 button_label=None, url=None,
                 in_reply_to=None, sender=None, validate_recipient=True):
    '''
    Send a message. If recipient is an email address, use send_mail.
    Otherwise, try sending a text message.
    '''

    if '@' in recipient:
        try:
            send_simple_email(recipient, subject, title, text, url_root,
                              button_label, url, in_reply_to, sender,
                              validate_recipient=validate_recipient)
        except Exception as e:
            logger.exception('Exception sending email', e)
    else:
        body = '%s: %s' % (title, text)
        if button_label:
            body += '[ %s ] -> %s' % (button_label, url)
        try:
            send_sms(recipient, subject, body, sender)
        except Exception as e:
            logger.exception('Exception sending SMS', e)


def send_upload_doc_notification(bot_name, doc_id, doc_name):
    bot_config = get_bot_config(bot_name)
    ingestor_contact = bot_config.get_ingestor_help().strip()

    subject = 'A new document has been uploaded to \
    %s' % bot_config.get_display_name()
    title = 'Your document has been uploaded'
    text = 'The document %s is now available on the %s instance of \
        Ingestor.' % (doc_name, bot_config.get_display_name())
    doc_id = object_id_to_path(doc_id).replace('/', '_')
    url = '%sdocument/%s/%s' % (request.url_root, bot_name, doc_id)

    send_message(ingestor_contact, subject, title, text, request.url_root,
                 url=url, button_label="View Status")


def send_ingested_doc_notification(bot_name, doc_id):
    bot_config = get_bot_config(bot_name)

    bot_path = current_user.get_bot_path_by_name(bot_name)

    document = cu_database.library_storage.get_document(bot_path, doc_id)
    sme_contact = document.uploaded_by.strip()
    doc_name = document.title

    subject = 'A new document is available on \
    %s' % bot_config.get_display_name()
    title = 'Your document is now available.'
    text = 'The document %s has been ingested into the %s instance of \
    Ingestor.' % (doc_name, bot_config.get_display_name())
    doc_id = doc_id.replace('/', '_')
    url = '%sdocument/%s/%s' % (request.url_root, bot_name, doc_id)

    send_message(sme_contact, subject, title, text, request.url_root,
                 url=url, button_label="View Document")


def get_user_name(userid):
    name = None
    if userid:
        user_data = cu_database.users.get_user(userid)
        if user_data:
            # the name could eb saved as FIRST_NAME or FULL_NAME
            name = user_data.get(
                cu_database.users.FIRST_NAME,
                user_data.get(cu_database.users.FULL_NAME))
    return name


def get_locale_str():
    lang = session.get('lang')
    if lang is not None:
        return lang
    # otherwise try to guess the language from the user accept
    # header the browser transmits.
    lang = request.accept_languages.best_match(current_app.config['LANGUAGES'])
    if lang is not None:
        return lang
    return 'en'


def get_elapsed_desc(timestamp, locale):
    if not timestamp:
        return ''
    timestamp = timestamp.replace(tzinfo=None)
    now = datetime.now()
    delta = now - timestamp
    delta_text = (format_timedelta(delta, locale=locale))

    if delta < timedelta(seconds=10):
        return _('just now')
    elif (timestamp.year, timestamp.month, timestamp.day) ==\
            (now.year, now.month, now.day):
        return _('today')
    elif delta < timedelta(days=2):
        return _('yesterday')
    else:
        return (_('%s  ago') % delta_text)


def organize_tags_from(text, tags):
    result_text = ''
    TAG_SEPARATOR = 'tag:'
    for line in text.split('\n'):
        if not line.startswith(TAG_SEPARATOR):
            result_text += line + '\n'

    if tags:
        result_text += '%s %s' % (TAG_SEPARATOR, tags)

    return result_text


def has_forbidden_tag(tags, forbidden_tags):
    return any(tag in tags for tag in forbidden_tags)


def get_forbidden_tags_by(user_id, bot_name):
    roles = get_bot_config(bot_name).get_roles()
    forbidden = []
    for role in roles.keys():
        current_role = roles[role]
        if user_id in current_role.get('users', []):
            forbidden = forbidden + current_role.get('forbidden_tags', [])
    return forbidden


def get_bot_config(bot_name):
    if not g.get('bot_config') or g.bot_config.get_bot_name() is None:
        bot_config = get_bot_config_by_name(bot_name, autosave=False)
        g.bot_config = bot_config
    return g.bot_config


def refresh_session_profile(userid=None):
    PROFILE = 'profile'
    USER_ID = 'user_id'
    APP_METADATA = "app_metadata"
    AUTH0_DATA = "auth0_data"
    users = Users()
    if not userid:
        userid = session.get(PROFILE).get(USER_ID)

    userinfo = users.get_user(userid)
    api_key = users.check_api_key(userid, userinfo)

    # Store the user information in flask session.
    session['profile'] = {
        'user_id': userinfo[USER_ID],
        'name': userinfo[AUTH0_DATA]['name'],
        'picture': userinfo[AUTH0_DATA]['picture'],
        'user_data': userinfo,
        'api_key': api_key,
        'roles': userinfo.get(APP_METADATA, {}).get('bots'),
        'bots': list(userinfo.get(APP_METADATA, {}).get('bots', {}).keys())
    }
