from werkzeug.local import LocalProxy
from flask import session, redirect, g
from cu_users import User
import cu_tools
import redis
import os
from ingestor_jwt.auth0 import AuthZeroValidator


current_user = LocalProxy(lambda: _get_user())
redis_url = os.getenv('REDIS_URL')
redis_store = None
if redis_url:
    redis_store = redis.from_url(redis_url)
PLATFORM_ACCESS_TOKEN = "PLATFORM_ACCESS_TOKEN"
authzero_validator = AuthZeroValidator()

AUTH_DOMAIN = os.getenv('AUTHZERO_DOMAIN')
CLIENT_ID = os.getenv('AUTHZERO_PLATFORM_API_CLIENT_ID')
CLIENT_SECRET = os.getenv('AUTHZERO_PLATFORM_API_CLIENT_SECRET')
AUDIENCE = os.getenv('AUTHZERO_API_AUDIENCE')


class PlatformAccessToken:

    def __init__(self):
        self.access_token = self.get_access_token_for_platform()

    def get_access_token_for_platform(self):
        token = authzero_validator.ger_access_token(AUTH_DOMAIN, CLIENT_ID,
                                                    CLIENT_SECRET, AUDIENCE)
        if redis_store:
            redis_store.set(PLATFORM_ACCESS_TOKEN, token, ex=86400)
            return redis_store.get(PLATFORM_ACCESS_TOKEN)
        else:
            return None

    def get_access_token(self):
        if redis_store:
            token = redis_store.get(PLATFORM_ACCESS_TOKEN)
            if not token:
                token = self.get_access_token_for_platform()
            return token.decode()
        else:
            return None


pat = PlatformAccessToken()


def _get_user():
    profile = session.get('profile')
    if profile:
        if g.current_user:
            return g.current_user
        else:
            userid = profile.get('user_id')
            user = User(userid)
            user.is_authenticated = True
            fake_token = ('api_key', profile.get('api_key'))
            user.set_token(fake_token)
            user.set_api_key(profile.get('api_key'))
            user.is_ingestor_user = cu_tools.is_ingestor_user(userid)
            user._set_authzero_profile(profile)
            g.current_user = user
            return user
    else:
        print('get_usr doesnt have profile')
        redirect('/logout')
