class AppConfig(object):

    LANGUAGES = []

    LANG_CONFIG = [
        {'name': 'English', 'value': 'en'},
        {'name': 'Español', 'value': 'es'},
        {'name': 'Italiano', 'value': 'it'},
        # {'name': 'Deutsch', 'value': 'de'},
        # {'name': 'Francais', 'value': 'fr'}
    ]

    def __init__(self):
        self._get_language_list()

    def _get_language_list(self):
        for lang in self.LANG_CONFIG:
            self.LANGUAGES.append(lang.get('value'))
