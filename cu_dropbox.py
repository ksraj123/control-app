# -*- coding: utf-8 -*-
'''
Dropbox integration for the control unit app
'''

# Copyright Gonzalo Odiard, Ingestor 2018

# This file is part of Ingestor's Knowbot platform and is copyright
# and a valuable trade secret of Ingestor, Inc.

import json
from flask import session

# TODO: move to configuration

DROPBOX_APP_KEY = '1ke9w8vva87xcgy'
# DROPBOX_APP_SECRET = 'lhuu97qvp2olv9s'


def get_dropbpox_credentials_data():
    if 'dropbpox_credentials' in session:
        return json.loads(session['dropbpox_credentials'])
    else:
        return {'app_key': DROPBOX_APP_KEY}


"""
TODO: if we need do Oauth2

import cu_tools

import logging

from flask import Blueprint
from flask import session, redirect, request, url_for, flash, abort, jsonify

from dropbox import DropboxOAuth2Flow
from dropbox.oauth import BadRequestException, BadStateException, CsrfException
from dropbox.oauth import NotApprovedException, ProviderException

# configure loggers
logging.basicConfig(level=os.getenv('LOGGER_LEVEL', logging.WARNING))
logger = logging.getLogger('dropbox')

dropbox = Blueprint('dropbox', __name__, template_folder='templates')

def set_dropbpox_credentials_data(credentials):
    session['dropbpox_credentials'] = json.dumps({
        'app_key': DROPBOX_APP_KEY,
        'access_token': credentials['access_token'],
        'account_id': credentials['account_id'],
        'user_id': credentials['user_id']})
    logger.debug('Stored information %s' % session['dropbpox_credentials'])


def get_dropbox_auth_flow(web_app_session):
    redirect_url = cu_tools.replace_http_by_https(
        url_for('dropbox.dropbox_auth_finish', _external=True))
    logger.debug('dropbox redirect_url %s' % redirect_url)
    return DropboxOAuth2Flow(
        DROPBOX_APP_KEY, DROPBOX_APP_SECRET, redirect_url, web_app_session,
        "dropbox-auth-csrf-token")


@dropbox.route('/get_dropbox_credentials/')
def get_dropbox_credentials():
    return jsonify(get_dropbpox_credentials_data())


@dropbox.route('/dropbox_auth_start')
# URL handler for /dropbox-auth-start
def dropbox_auth_start():
    bot_id = request.args['bot_id']
    authorize_url = get_dropbox_auth_flow(session).start(url_state=bot_id)
    return redirect(authorize_url)


@dropbox.route('/dropbox_auth_finish')
def dropbox_auth_finish():
    try:
        oauth_result = get_dropbox_auth_flow(session).finish(
                    request.args)
        logger.debug(oauth_result)
        credentials = {'access_token': oauth_result.access_token,
                       'account_id': oauth_result.account_id,
                       'user_id': oauth_result.user_id,
                       'url_state': oauth_result.url_state}
        bot_id = oauth_result.url_state
        logger.debug(credentials)
        set_dropbpox_credentials_data(credentials)
    except BadRequestException:
        abort(400)
    except BadStateException:
        # Start the auth flow again.
        redirect("/dropbox-auth-start")
    except CsrfException:
        abort(403)
    except NotApprovedException:
        flash('Not approved?  Why not?')
        return redirect("/")
    except ProviderException as e:
        logger.log("Auth error: %s" % (e,))
        abort(403)
    return redirect('/library/%s?openDropboxPicker' % bot_id)
"""
