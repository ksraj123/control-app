APP_PATH=cu-app
sudo chown -R $LOGNAME $APP_PATH
cd $APP_PATH
git fetch

branch='master'
if [ $# -eq 1 ];
then
	branch=$1
        git branch -D $branch
	git checkout -b $branch origin/$branch
else
	git checkout master
fi

echo 'Updating branch ' $branch

PRIVATE_DEPENDENCIES="pyengage ingestor-api-model cloud-services ingestor-storage ingestor-security"

git pull
source env/bin/activate
pip3 uninstall -y $PRIVATE_DEPENDENCIES

pip3 install -r requirements.txt
./compile-translations.sh
cd ..
sudo chown -R www-data $APP_PATH
echo "Restarting service..."
sudo systemctl restart cu-ingestor

