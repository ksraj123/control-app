export GOOGLE_APPLICATION_CREDENTIALS=cert/Control-Unit-5b442b54e43d.json
export SEARCH_API_HOST=https://ingestor-search-api.herokuapp.com
export LOGGER_LEVEL=DEBUG
export MONGO_HOST=db.prod.ingestor.io
export REDIS_URL=redis://127.0.0.1:6379
source ./cert/auth0-prod.sh
python3 app.py

