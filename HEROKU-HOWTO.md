# Heroku experiments

First of all, go to https://heroku.com and create a account


## Installing Heroku on Fedora 32

```
$ npm install -g heroku
```
More info about how to install

https://devcenter.heroku.com/articles/heroku-cli

## Login to Heroku

```
$ heroku login
```

This will open a browser page, if you already created a heroku account,

```
$ heroku login
heroku: Press any key to open up the browser to login or q to exit:
Opening browser to https://cli-auth.heroku.com/auth/cli/browser/4cba8684-2c09-4a57-aef6-a529c76a4a38
Logging in... done
Logged in as gonzalo@ingestor.com
```

I am on the cu-app directory, will create the heroku app

```
$ heroku create
Creating app... done, ⬢ stark-springs-39385
https://stark-springs-39385.herokuapp.com/ | https://git.heroku.com/stark-springs-39385.git
```

Try to push to heroku fails because can not get our private dependencies

```
$ git push heroku master
Enumerating objects: 8578, done.
Counting objects: 100% (8578/8578), done.
Delta compression using up to 4 threads
Compressing objects: 100% (4747/4747), done.
Writing objects: 100% (8578/8578), 3.43 MiB | 550.00 KiB/s, done.
Total 8578 (delta 5998), reused 4935 (delta 3491), pack-reused 0
remote: Compressing source files... done.
remote: Building source:
remote:
remote: -----> Python app detected
remote: -----> Installing python-3.6.10
remote: -----> Installing pip
remote: -----> Installing SQLite3
remote: -----> Installing requirements with pip
remote:        Collecting git+ssh://****@github.com/ingestor/speech_recognition (from -r /tmp/build_86ff9f508ae2eceacd18f2872f0d4f02/requirements.txt (line 25))
remote:          Cloning ssh://****@github.com/ingestor/speech_recognition to /tmp/pip-req-build-x1gt3wwg
remote:          Running command git clone -q 'ssh://****@github.com/ingestor/speech_recognition' /tmp/pip-req-build-x1gt3wwg
remote:          Host key verification failed.
remote:          fatal: Could not read from remote repository.
remote:          Please make sure you have the correct access rights
remote:          and the repository exists.
remote:        ERROR: Command errored out with exit status 128: git clone -q 'ssh://****@github.com/ingestor/speech_recognition' /tmp/pip-req-build-x1gt3wwg Check the logs for full command output.
remote:  !     Push rejected, failed to compile Python app.
remote:
remote:  !     Push failed
remote: Verifying deploy...
remote:
remote: !	Push rejected to stark-springs-39385.
remote:
To https://git.heroku.com/stark-springs-39385.git
 ! [remote rejected] master -> master (pre-receive hook declined)
error: failed to push some refs to 'https://git.heroku.com/stark-springs-39385.git'
```

Try to generate a key to use by heroku to acces github

```
$ ssh-keygen -t rsa -b 4096 -C "heroku@ingestor.com"
Generating public/private rsa key pair.
Enter file in which to save the key (/home/gonzalo/.ssh/id_rsa): ../heroku/id_rsa
```
I am generating them on the directory `../heroku`

This will generate two files `id_rsa` and `id_rsa.pub` on the directory `../heroku`
I will rename them to `deploy-key` and `deploy-key.pub`

Then associate the public key to my github account

Setup the config variables:

```
$ heroku config:set DEPLOYKEYPUB="`cat ../heroku/deploy-key.pub | base64`"
$ heroku config:set DEPLOYKEY="`cat ../heroku/deploy-key | base64`"
```

Then create the file `bin/pre_compile` to store them as ssh key before gettting the dependencies:

```
# This script configures ssh on Heroku to use the deploy key.
# This is needed to install private dependencies.
#
# Ensure we have an ssh folder
if [ ! -d ~/.ssh ]; then
  mkdir -p ~/.ssh
  chmod 700 ~/.ssh
fi

# Create the key files
echo $DEPLOYKEY | sed 's/ /\n/g' | base64 --decode > ~/.ssh/deploy-key
chmod 400 ~/.ssh/deploy-key
echo $DEPLOYKEYPUB | sed 's/ /\n/g' | base64 --decode > ~/.ssh/deploy-key.pub
chmod 400 ~/.ssh/deploy-key.pub
#ssh-add ~/.ssh/deploy-key

# Configure ssh to use the correct deploy key when connecting to github.
# Disables host verification.
echo -e "Host github.com\n"\
        "  IdentityFile ~/.ssh/deploy-key\n"\
        "  IdentitiesOnly yes\n"\
        "  UserKnownHostsFile=/dev/null\n"\
        "  StrictHostKeyChecking no"\
        >> ~/.ssh/config

# The vanilla python buildpack can now install all the dependencies in
# requirement.txt
```

After that, add this file to the repository, `git push heroku master` can
install all the python dependencies.

Reference: https://stackoverflow.com/questions/21297755/heroku-python-dependencies-in-private-repos-without-storing-my-password/50721757#50721757

Heroku detected that we are using nltk, we need to add a file `nltk.txt`
to let heroku download the data needed by the app. Create that based in our `nltk_setup.py`

```
$ cat nltk.txt
wordnet
stopwords
punkt
averaged_perceptron_tagger
```
Ref: https://devcenter.heroku.com/articles/python-nltk

The app still do not start, heroku uses gunicorn to start the app:

I need to add gunicorn as a dependency and the file `Procfile`

```
$ cat Procfile
web: gunicorn app:app
```

Now we can do:
```
$ git push heroku master
$ heroku ps:scale web=1
```

and the app starts ... and crashes, because it doesn't have all the certs and env variables

We need to modify pre_compile script to add the mongo certificates,
and add the certificates to the heroku config:

```
$ heroku config:set MONGO_HOST=db.dev.ingestor.io
$ heroku config:set MONGO_CA_CRT="`cat ./cert/CA.crt | base64`"
$ heroku config:set MONGO_CLIENT_PEM="`cat ./cert/client.pem | base64`"
```

And then the app starts.

Every database operation can be done, but AI searches crash due to memory limitations.

This is probably related to that I am doing tests with a free instance.

## tmp directory on Heroku

After move from the free instance to a the ingestor account, session on the app
is lost, and the user is redirected to login page.
Looks related to https://stackoverflow.com/a/31002236/3969110
I changed the code to allow get the session sncryption key from a config value
and added the config to heroku

Generate a random key

```
$ python
Python 3.8.2 (default, Feb 28 2020, 00:00:00)
[GCC 10.0.1 20200216 (Red Hat 10.0.1-0.8)] on linux
Type "help", "copyright", "credits" or "license" for more information.
>>> import os
>>> from base64 import b64encode
>>> b64encode(os.urandom(64)).decode('utf-8')
'xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx/xxxxxxxxxx=='
```

(key hidden)

Store it:

```
$ heroku config:set JWT_SECRET="xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx/xxxxxxxxxx=="
```

## Adding Redis

```
$ heroku addons:create heroku-redis:hobby-dev
Creating heroku-redis:hobby-dev on ⬢ stark-springs-39385... free
Your add-on should be available in a few minutes.
! WARNING: Data stored in hobby plans on Heroku Redis are not persisted.
redis-metric-19029 is being created in the background. The app will restart when complete...
Use heroku addons:info redis-metric-19029 to check creation progress
Use heroku addons:docs heroku-redis to view documentation
```

https://elements.heroku.com/addons/heroku-redis

https://devcenter.heroku.com/articles/heroku-redis#connecting-in-python

Redis is useed to cache users, botconfig and config (ingestor users and instances)

## Enable coslern cache on S3

```
$ heroku config:set USE_S3_CACHE=Y
$ heroku config:set S3_CONFIG="xxxxxxxx=="
```

## Configure variables for Google services

Run the following commands on our prod instance (because have the config files for prod)
and add the result to the variables: GOOGLE_CREDENTIALS_DATA, GOOGLE_CLIENT_ID_DATA
and GOOGLE_CLIENT_DATA
```
cat cert/Control-Unit-5b442b54e43d.json | base64
cat cert/client_id.json | base64
cat cert/google_client_data.json | base64
```

Also add the config GOOGLE_APPLICATION_CREDENTIALS=/app/cert/Control-Unit-5b442b54e43d.json
on the heroku app

## To force un update on heroku

As the app is only updated on Heroku on a push to git, if the app does not change,
but by example there are a change on a dependency, we can do

```
$ git commit --amend -C HEAD
$ git push production master -f
```

# PENDING

## Speech to text

We can not use ffmpeg to convert the audio format to send it to Google. We would need to install a buildpack to use ffmpeg or deploy the apppon a docker dontainer.

## Setp production app

I have tried to setup a production app based on the followingfg link, but for some reason
the search app didn't worked

https://devcenter.heroku.com/articles/multiple-environments#creating-a-production-environment
