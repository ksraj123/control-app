Flask==2.0.3
Flask-Login==0.4.1
Flask-Babel==1.0.0
markdown2>=2.3.8
requests>=2.20.0
uwsgi==2.0.17.1
python-dateutil==2.7.3
Babel>=2.3
gunicorn>=20.0.4

# for the google drive integration
google-api-python-client==1.7.4
google-auth-httplib2==0.0.3
google-auth-oauthlib==0.2.0
oauth2client==4.1.3
# for dropbox integration
dropbox==9.1.0
twilio==6.16.3

# to rotate images
Pillow>=7.1.0
# to generate the charts on png format
cairosvg==2.4.2
authlib

-e git+https://gitlab.com/ksraj123/app-services/pyengage.git#egg=pyengage

# to test changes in a local repository replace the git+ssh url
# by a git+file url popinting to your local repo
# git+file:///home/gguzman/Documents/ksraj123/pyengage@fix-sponsor
# git+file:///home/santiago/Desarrollo/trinomio/ksraj123/pyengage@1289-create-bot
# git+file:///home/gonzalo/trinomio/sources/ksraj123/ksraj123-storage
