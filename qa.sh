DISTRIBUTION=$(grep ^ID= /etc/os-release | awk -F '=' '{print $2}')

if [ $DISTRIBUTION == 'fedora' ]
then
   pycodestyle-3 *.py tests/*.py
   pyflakes-3 *.py tests/*.py
else
   pep8 *.py tests/*.py
   pyflakes3 *.py tests/*.py
fi

