# -*- coding: utf-8 -*-
'''
Google integration for the control unit app
'''

# Copyright Gonzalo Odiard, Ingestor 2018

# This file is part of Ingestor's Knowbot platform and is copyright
# and a valuable trade secret of Ingestor, Inc.

import cu_tools

import io
import json
import logging
import os
import mimetypes
import cu_database

from flask import jsonify, session, redirect, request, render_template
from flask import url_for
from flask import Blueprint
from cu_security import login_required
from cu_authentication import current_user
from flask_babel import _
from cloudservices.aws.files import FileManager, generate_unique_filename

import google.oauth2.credentials
import google_auth_oauthlib.flow
from googleapiclient.discovery import build
from googleapiclient.http import MediaIoBaseDownload
GOOGLE_SERVICE_ACCOUNT_FILE = './cert/client_id.json'
GOOGLE_SCOPES = ['https://www.googleapis.com/auth/drive.metadata.readonly',
                 'https://www.googleapis.com/auth/drive.readonly',
                 'openid']

# configure loggers
logging.basicConfig(level=os.getenv('LOGGER_LEVEL', logging.WARNING))
logger = logging.getLogger('google')


def read_google_client_data():
    return json.load(open('./cert/google_client_data.json'))


def get_google_drive_credentials_data():
    if 'google_credentials' in session:
        return json.loads(session['google_credentials'])
    else:
        return None


def set_google_drive_credentials_data(credentials):
    session['google_credentials'] = json.dumps({
        'token': credentials.token,
        'refresh_token': credentials.refresh_token,
        'token_uri': credentials.token_uri,
        'client_id': credentials.client_id,
        'client_secret': credentials.client_secret,
        'scopes': credentials.scopes})
    logger.debug('Stored information %s' % session['google_credentials'])


google_drive = Blueprint('google_drive', __name__,
                         template_folder='templates')


@google_drive.route('/upload_from_gdrive/<path:bot_name>/<file_ids>/')
@login_required
def upload_from_gdrive(bot_name, file_ids):
    """
    TODO: bot validation
    bot = get_bot_by_id(bot_name)
    if bot is None or bot_name not in current_user.get_bots():
        return render_template(
            'error.html', page='error', filter={},
            error_message='Instances %s not found' % bot_name)
    """

    logger.debug('upload_from_gdrive BOT = %s, FILE_ID %s' %
                 (bot_name, file_ids))
    engage = cu_tools.init_engage(bot_name, current_user=current_user)
    list_file_id = []
    if ',' in file_ids:
        list_file_id = file_ids.split(',')
    else:
        list_file_id.append(file_ids)

    error_code = 0
    for file_id in list_file_id:
        if file_id:
            error_code = _upload_from_google_drive(bot_name, file_id, engage)

    if error_code == 1:
        return render_template('error_message.html', page='error', filter={},
                               error_message='Need authorization')
    if error_code == 2:
        return render_template('error_message.html', page='error', filter={},
                               error_message="The file can't be upload")

    return redirect('/library/%s?msg_code=1' % bot_name)


def _upload_from_google_drive(bot_name, file_id, engage):
    bot_id = current_user.get_bot_path_by_name(bot_name)

    credentials_data = get_google_drive_credentials_data()
    if credentials_data is None:
        return 1

    creds = google.oauth2.credentials.Credentials(**credentials_data)
    drive = build('drive', 'v2', credentials=creds, cache_discovery=False)

    # store updated credentials
    set_google_drive_credentials_data(creds)
    session.modified = True

    file_name = drive.files().get(fileId=file_id,
                                  fields="title").execute()['title']

    logger.debug('Google drive file_name %s' % file_name)

    try:
        request_drive = drive.files().export_media(fileId=file_id,
                                                   mimeType='text/html')
        file_content = _get_file_content(request_drive)
        file_name += '.html'

    except Exception:
        # try to download
        # Export only supports Google Docs
        request_drive = drive.files().get_media(fileId=file_id)
        file_content = _get_file_content(request_drive)

    document = {
        'title': file_name,
        'uploaded_by': current_user.get_id()
    }

    doc_id = cu_database.library_storage.upload_document(
        bot_id, document)

    changes, error_code = _upload_file(
        file_name, file_content, engage, doc_id)
    if error_code == 1:
        return 2

    cu_database.library_storage.update_document(doc_id, bot_id, changes)

    cu_tools.send_upload_doc_notification(bot_name, doc_id, document['title'])

    return 0


def _upload_file(file_name, file_content, engage, doc_id):

    bucket, storage_folder = engage.get_document_storage_location(doc_id)

    file_manager = FileManager()
    tmp_filename = os.path.join('/tmp', generate_unique_filename(file_name))

    mime_type = mimetypes.guess_type(file_name)
    if mime_type:
        mime_type = mime_type[0]

    tmp_file = open(tmp_filename, 'wb')
    tmp_file.write(file_content)
    tmp_file.close()

    changes = {
        'content-type': mime_type,
        'bucket': bucket
    }
    error_code = 0

    try:
        url, error, stored_name = file_manager.upload(
            tmp_filename, bucket=bucket, public=False, keep_filename=True)
        changes['stored'] = url
        changes['stored_name'] = stored_name

    except Exception as e:
        logger.debug(e)
        error_code = 1

    os.remove(tmp_filename)
    return changes, error_code


@google_drive.route('/get_google_credentials/')
@login_required
def get_google_credentials():
    credentials_data = get_google_drive_credentials_data()
    if credentials_data is None:
        return jsonify({})

    credentials = google.oauth2.credentials.Credentials(
        **credentials_data)

    # Use the credentials
    # Cache discovery disabled to avoid log error
    # see a better solution
    # https://github.com/googleapis/google-api-python-client/issues/299
    # https://github.com/googleapis/google-api-python-client/issues/325
    build('drive', 'v2', credentials=credentials, cache_discovery=False)
    # store updated credentials
    set_google_drive_credentials_data(credentials)
    session.modified = True
    return jsonify(get_google_drive_credentials_data())


def _ingest_from_google_drive(bot_name, file_id, engage):
    credentials_data = get_google_drive_credentials_data()
    logger.debug('_ingest_from_google_drive %s' % credentials_data)
    if credentials_data is None:
        return False

    credentials = google.oauth2.credentials.Credentials(
        **credentials_data)

    # Use the credentials
    # Cache discovery disabled to avoid log error
    # see a better solution
    # https://github.com/googleapis/google-api-python-client/issues/299
    # https://github.com/googleapis/google-api-python-client/issues/325
    drive = build('drive', 'v2', credentials=credentials,
                  cache_discovery=False)

    # store updated credentials
    set_google_drive_credentials_data(credentials)
    session.modified = True

    file_name = drive.files().get(
        fileId=file_id, fields="title").execute()['title']

    logger.debug('Google drive file_name %s' % file_name)

    try:
        request_drive = drive.files().export_media(
            fileId=file_id, mimeType='text/html')
        file_content = _get_file_content(request_drive)
        # logger.debug('file_content %s' % file_content)
        response_ingest, ecode = engage.ingest_from_file(
            file_name, file_content, mime_type='text/html')
    except Exception:
        # try to download
        # Export only supports Google Docs
        request_drive = drive.files().get_media(fileId=file_id)
        file_content = _get_file_content(request_drive)
        # logger.debug('file_content %s' % file_content)
        response_ingest, ecode = engage.ingest_from_file(
            file_name, file_content)

    return True


def _get_file_content(request_drive):
    fh = io.BytesIO()
    downloader = MediaIoBaseDownload(fh, request_drive)
    done = False
    while done is False:
        status, done = downloader.next_chunk()
        logger.debug("Download %d%%." % int(status.progress() * 100))

    return fh.getvalue()


def get_google_auth_flow():
    # see https://developers.google.com/api-client-library/python/auth/web-app
    flow = google_auth_oauthlib.flow.Flow.from_client_secrets_file(
        GOOGLE_SERVICE_ACCOUNT_FILE, scopes=GOOGLE_SCOPES)

    # Indicate where the API server will redirect the user after the user
    # completes the authorization flow. The redirect URI is required.

    # when use ngrok, request.host_url is http instead of https
    host_url = cu_tools.replace_http_by_https(
        url_for('google_drive.get_drive_data', _external=True))
    host_url = cu_tools.replace_http_by_https(host_url)

    logger.debug(host_url)
    flow.redirect_uri = host_url

    return flow


@google_drive.route('/enable_google_drive/')
@login_required
def enable_google_drive():
    bot_name = request.args['bot_name']

    flow = get_google_auth_flow()
    # Generate URL for request to Google's OAuth 2.0 server.
    # Use kwargs to set optional request parameters.
    authorization_url, state = flow.authorization_url(
        # Enable offline access so that you can refresh an access token
        # without re-prompting the user for permission. Recommended for
        # web server apps.
        access_type='offline',
        state=bot_name,
        # Enable incremental authorization. Recommended as a best practice.
        include_granted_scopes='true')

    logger.debug('authorization_url = %s' % authorization_url)
    return redirect(authorization_url)


@google_drive.route('/get_drive_data/')
def get_drive_data():
    args = request.args.to_dict()
    if 'error' in args:
        error_msg = _('Authorization denied (%s)') % args['error']
        return render_template(
            'error_message.html', page='error', filter={},
            error_message=error_msg)

    bot_name = args['state']

    authorization_response = cu_tools.replace_http_by_https(request.url)

    logger.debug('get_drive_data authorization_response %s' %
                 authorization_response)
    flow = get_google_auth_flow()
    flow.fetch_token(authorization_response=authorization_response)

    # Store the credentials
    # ACTION ITEM for developers:
    #     Store user's access and refresh tokens in your data store if
    #     incorporating this code into your real app.
    credentials = flow.credentials
    set_google_drive_credentials_data(credentials)
    session.modified = True
    return redirect('/library/%s?openPicker' % bot_name)
