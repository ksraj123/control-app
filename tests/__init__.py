# -*- coding: utf-8 -*-
'''
Unit tests for pyengage
'''

# Copyright Walter Bender, Ingestor 2018

# This file is part of Ingestor's KnowBot platform and is copyright
# and a valuable trade secret of Ingestor, Inc.

import unittest

import os
import logging
from tests.unittest_tools import CuToolsTestCase

assert CuToolsTestCase

if __name__ == '__main__':
    logging.basicConfig(format='%(levelname)s:%(message)s',
                        level=os.getenv('LOGGER_LEVEL', logging.WARNING))
    unittest.main()
