# Copyright Gonzalo Odiard, Ingestor 2018

# This file is part of Ingestor's KnowBot platform and is copyright
# and a valuable trade secret of Ingestor, Inc.

import unittest
import logging
import os
import cu_tools


class CuToolsTestCase(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        super(CuToolsTestCase, cls).setUpClass()
        logging.basicConfig(format='%(levelname)s:%(message)s',
                            level=os.getenv('LOGGER_LEVEL', logging.WARNING))

    def test_pagination(self):
        quantity_entries = 53
        actual_page = 0
        pages = cu_tools.prepare_pages_array(quantity_entries, actual_page)
        logging.debug("quantity %d, actual_page %d => %s",
                      quantity_entries, actual_page, pages)
        self.assertSequenceEqual(pages, [0, 1, 2, 3, 4, 5])

    def test_pagination2(self):
        quantity_entries = 53
        actual_page = 6
        pages = cu_tools.prepare_pages_array(quantity_entries, actual_page)
        logging.debug("quantity %d, actual_page %d => %s",
                      quantity_entries, actual_page, pages)
        self.assertSequenceEqual(pages, [0, 1, 2, 3, 4, 5])

    def test_pagination3(self):
        quantity_entries = 100
        actual_page = 0
        pages = cu_tools.prepare_pages_array(quantity_entries, actual_page)
        logging.debug("quantity %d, actual_page %d => %s",
                      quantity_entries, actual_page, pages)
        self.assertSequenceEqual(pages, [0, 1, 2, 3, 4, 5, 6, 7, 8, 9])

    def test_pagination4(self):
        quantity_entries = 200
        actual_page = 0
        pages = cu_tools.prepare_pages_array(quantity_entries, actual_page)
        logging.debug("quantity %d, actual_page %d => %s",
                      quantity_entries, actual_page, pages)
        self.assertSequenceEqual(pages, [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, "next"])

    def test_pagination5(self):
        quantity_entries = 200
        actual_page = 10
        pages = cu_tools.prepare_pages_array(quantity_entries, actual_page)
        logging.debug("quantity %d, actual_page %d => %s",
                      quantity_entries, actual_page, pages)
        self.assertSequenceEqual(
            pages, ['prev', 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 'next'])

    def test_pagination6(self):
        quantity_entries = 200
        actual_page = 20
        pages = cu_tools.prepare_pages_array(quantity_entries, actual_page)
        logging.debug("quantity %d, actual_page %d => %s",
                      quantity_entries, actual_page, pages)
        self.assertSequenceEqual(
            pages, ['prev', 10, 11, 12, 13, 14, 15, 16, 17, 18, 19])

    def test_pagination7(self):
        quantity_entries = 3000
        actual_page = 0
        pages = cu_tools.prepare_pages_array(quantity_entries, actual_page)
        logging.debug("quantity %d, actual_page %d => %s",
                      quantity_entries, actual_page, pages)
        self.assertSequenceEqual(pages, [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 'next'])

    def test_pagination8(self):
        quantity_entries = 3000
        actual_page = 300
        pages = cu_tools.prepare_pages_array(quantity_entries, actual_page)
        logging.debug("quantity %d, actual_page %d => %s",
                      quantity_entries, actual_page, pages)
        self.assertSequenceEqual(
            pages, ['prev', 290, 291, 292, 293, 294, 295, 296, 297, 298, 299])


if __name__ == '__main__':
    unittest.main()
