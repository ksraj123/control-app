from flask_babel import lazy_gettext

to_tooltips = {
  "users": {
    "roles":
    lazy_gettext("Users can only use the knowledge base. SMEs can edit the "
                 "knowledge base. Admins can access and change settings.")
  },
  "identity": {
    "database_name": lazy_gettext("This is the url for your instance of "
                                  "Ingestor. It cannot be changed."),
    "display_name": lazy_gettext("This is the public name of your database. "
                                 "It will appear when users first sign in."),
    "avatar_image":
    lazy_gettext("his image will appear in the upper-left of the screen for "
                 "all users. If you have a slack bot, this image will also "
                 "appear as your bot’s avatar."),
    "media_url":
    lazy_gettext("This image will show up when answering questions via email.")
  },
  "personality": {
    "ask_preamble": lazy_gettext("A short, always-visible user prompt above "
                                 "the Ask bar."),
    "disclaimer": lazy_gettext("This message will appear at the bottom of "
                               "each question/answer pair."),
    "like_dislike_response":
    lazy_gettext("The message displayed after a user clicks on the "
                 "“thumbs up” or “thumbs down” icons."),
    "no_answer_response": lazy_gettext("The message displayed when a search "
                                       "turns up no response."),
    "user_prompt: No answer response":
    "The message displayed after a user clicks on “This does not answer "
    "my question.” (Note, make sure this will reflect proper button title",
    "feedback_prompt":
    lazy_gettext("The message that appears in the “Feedback” box to prompt a "
                 "user to leave feedback."),
    "feedback_confirmation":
    lazy_gettext("The message to users confirming that feedback has been "
                 "received."),
    "note_prompt":
    lazy_gettext("The message prompt that appears in the “Note” box to prompt "
                 "a user to leave a note."),
    "note_confirmation": lazy_gettext("The message to users confirming that "
                                      "a note has been saved."),
    "require_verification":
    lazy_gettext("Selecting this option will limit results to verified "
                 "questions and answers."),
    "misunderstanding_response":
    lazy_gettext("The message displayed when a user clicks the "
                 "”Can't find answer” button.")
  },
  "notifications": {
    "sme_email_addresses":
    lazy_gettext("These are the experts who will receive email notifications "
                 "when a new issue is created in the Issue queue."),
    "send_notifications_to":
    lazy_gettext("This determines whether users and/or experts will receive "
                 "emailed notifications.")
  },
  "social": {
    "welcome_message": lazy_gettext("The first message your users will see "
                                    "when contacting Ingestor on Slack."),
    "help_list": lazy_gettext("The response users will see when asking for "
                              "help on Slack."),
    "share_button_label":
    lazy_gettext("The word on the “share” button for Slack. This button will "
                 "allow a user to share a response with another channel."),
    "share_confirmation": lazy_gettext("The Slack response after sharing an "
                                       "answer."),
    "sms_help_response": lazy_gettext("Ingestor’s response to asking for help "
                                      "over text."),
    "sms_switch_response":
    lazy_gettext("The response to an SMS user request to switch from one "
                 "Ingestor knowledge base to another."),
    "tcp”a_prompt":
    lazy_gettext("According to the Telephone Consumer Protection Act, all "
                 "SMS users must opt-in to receiving texts."),
    "require_first_prompt":
    lazy_gettext("This will require a greeting message the first time a user "
                 "logs into Ingestor on SMS."),
    "first_prompt": lazy_gettext("This is the first message users receive "
                                 "after opting in to using Ingestor via SMS.")
  },
  "ingestor": {
    "email_alias":
    lazy_gettext("This is the email address your users can write to, which "
                 "will respond with a search result. Emails should go "
                 "to  ________@ingestor.ai."),
    "ingestor_help":
    lazy_gettext("The “Help” option will appear in emails, and will route "
                 "questions to Ingestor’s customer success team."),
    "avatar_destination_url":
    lazy_gettext("When configured, sets destination when the user clicks on "
                 "the instance logo, the url should start with https:// "
                 "or http://"),
    "public_instance": lazy_gettext("Selecting this will allow users to "
                                    "access Ingestor without registering."),
    "ask_page":
    lazy_gettext("This will determine whether the Ask page returns results "
                 "from the Q&A tab, the Library tab, or both."),
    "help_action":
    lazy_gettext("This is where users will go when they click "
                 "“Want more help?” It should open up a link "
                 "to https://control-unit.ingestor.io/ask/ama."),
    "password_expiration_days":
    lazy_gettext("The number of days before a user must reset their "
                 "password."),
    "number_of_invalid_login_attempts_allowed":
    lazy_gettext("The number of wrong passwords allowed before a user has to "
                 "reset."),
    "enable_two_factor_authentication":
    lazy_gettext("When selected, users will have to sign in using a password "
                 "sent to their email addresses."),
    "ip_filtering_mask":
    lazy_gettext("Allows users to access their accounts only from specific "
                 "IP addresses."),
    "filter_tags": lazy_gettext("Tags to use as filter on the Issues page"),
    "safety_feature":
    lazy_gettext("Enabling enhanced security features prevents users from "
                 "downloading documents or copying and pasting content.")
  },
  "error_msg": {
    "image": lazy_gettext("Not allowed file, upload a jpg, png or gif image."),
    "size": lazy_gettext("Please select image size less than 200 KB.")
  }
}


def get_tooltips():
    return to_tooltips
