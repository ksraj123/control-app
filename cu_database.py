# -*- coding: utf-8 -*-
'''
A application to configure the Ingestor services
'''

# Copyright Gonzalo Odiard, Ingestor 2018

# This file is part of Ingestor's Knowbot platform and is copyright
# and a valuable trade secret of Ingestor, Inc.

from datetime import datetime

from ingestor_security import LoginManager

from ingestor_storage import QueueStorage, QA, Storage, Stats, UserStats
from ingestor_storage import Library, Users, Notifications, Logs, Config, Notes
from ingestor_storage import PinnedResponses, UserActions
from ingestor_storage.ontology_storage import OntologyStorage


qa = QA()
queue_storage = QueueStorage()
library_storage = Library()
users = Users()
login_manager = LoginManager(users_storage=users)
user_stats = UserStats()
stats = Stats()
notifications = Notifications()
logs = Logs()
config = Config()
notes = Notes()
ontology_storage = OntologyStorage()
pinned_responses = PinnedResponses()
user_actions = UserActions()

# TODO: move public bots management out of Storage

_storage = Storage()

_PUBLIC_BOTS_REFRESH_SECONDS = 600


class PublicBot(object):

    def __init__(self, storage):
        self._storage = storage
        self._public_bots = None
        self._public_bot_names = None
        self._last_update = None

    def _init_public_bots(self):
        if not self._public_bots or self._expired():
            self._public_bots = []
            self._public_bot_names = []
            self._public_bots = self._storage.get_public_bots()
            for bot in self._public_bots:
                self._public_bot_names.append(bot['id'])
            self._last_update = datetime.now()

    def _expired(self):
        if self._last_update is None:
            return True
        delta = (datetime.now() - self._last_update).seconds
        # check if is older than 10 minutes
        return delta > _PUBLIC_BOTS_REFRESH_SECONDS

    def get_bots(self):
        self._init_public_bots()
        return self._public_bots

    def get_bot_names(self):
        self._init_public_bots()
        return self._public_bot_names

    def get_path_by_id(self, id):
        self._init_public_bots()
        for bot in self._public_bots:
            if bot['id'] == id:
                return bot['path']
        return None


public_bot = PublicBot(_storage)
