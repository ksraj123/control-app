MODULE_UPDATE=$1
echo 'Update '$1
REPOSITORY=$(grep $MODULE_UPDATE requirements.txt | grep -v "^#")

echo "REPOSITORY URL"
echo $REPOSITORY

DISTRIBUTION=$(grep ^ID= /etc/os-release | awk -F '=' '{print $2}')
VERSION=$(grep ^VERSION_ID= /etc/os-release | awk -F '=' '{print $2}')

if [ $DISTRIBUTION == 'fedora' ] && [ $VERSION -lt '30' ]
then
   pip uninstall -y $MODULE_UPDATE;pip install $REPOSITORY
else
   pip3 uninstall -y $MODULE_UPDATE;pip3 install $REPOSITORY
fi
