import io
import logging
import os
import cu_database
from cu_security import login_required
from cu_authentication import current_user

import mimetypes
import urllib.request
import urllib.parse

from flask import request, render_template, redirect, Response
from flask import Blueprint
from flask_babel import _
from werkzeug.wsgi import FileWrapper
from werkzeug.datastructures import Headers

from engage import engage_tools
from engage.engage import TMP_FOLDER
from engage.botconfig import BotConfig
from engage.botconfig import get_config_to_notify_user

from cloudservices.aws.files import FileManager, generate_unique_filename

import cu_tools
import cu_security
import cu_google
import cu_dropbox
from ingestor_storage.actions import UserActions
from ingestor_storage import tools
from cu_tools import get_bot_config

# Configure Logger
logging.basicConfig(level=os.getenv('LOGGER_LEVEL', logging.WARNING))
logger = logging.getLogger('library')
library_blueprint = Blueprint('library', __name__, template_folder='templates')

LIBRARY_MESSAGES = {
    1: _('File has been uploaded. '
         'You will be notified when ingestion is complete. '
         'This may take some time.')
}


@library_blueprint.route('/library/<path:bot_name>')
@cu_security.verify_bot
@cu_security.verify_botconfig_field(cu_tools.BOTCONFIG_LIBRARY_SEARCH)
@cu_security.verify_permission(BotConfig.PERM_VIEW_LIBRARY)
def library(bot_name):
    filter = cu_tools.get_filter(bot_name, request)
    filter['doc_status'] = request.args.get('doc_status')
    msg_code = request.args.get('msg_code')
    msg = None
    if msg_code:
        msg = LIBRARY_MESSAGES.get(int(msg_code))

    userid, bot_path = cu_tools.get_userid_and_bot_path(
        current_user, bot_name)
    if bot_path is None:
        return redirect('/invalid_instance/%s' % bot_name)

    doc_status = filter['doc_status']

    library = cu_database.library_storage.get_documents(
        bot_path, text_filter=filter['text_filter'],
        since=filter['since'],
        start=filter['active_page'] * cu_tools.ENTRIES_BY_PAGE,
        status=doc_status,
        descending=(filter['direction'] == '-1'))

    if filter['active_page'] > 0 and not library:
        # this page do not have more issues, redirect to the prev page
        return _redirect_prev_page('library', bot_name, request)

    for entry in library:
        # WORKAROUND: status could be a list
        status = engage_tools.get_first_from_data(entry.status)
        entry.status = cu_tools.doc_badges.get(status)

    if len(library) < cu_tools.ENTRIES_BY_PAGE and filter['active_page'] == 0:
        quantity = len(library)
    else:
        quantity = cu_database.library_storage.get_documents_quantity(
            bot_path, text_filter=filter['text_filter'],
            since=filter['since'],
            status=doc_status)

    filter['statuses'] = cu_tools.doc_statuses
    filter['quantity'] = quantity
    filter['pages'] = cu_tools.prepare_pages_array(
        quantity, filter['active_page'])

    # check if google_credentials are available
    credentials_data = cu_google.get_google_drive_credentials_data()
    google_client_data = cu_google.read_google_client_data()
    dropbox_credentials_data = cu_dropbox.get_dropbpox_credentials_data()

    return render_template('library.html', filter=filter, library=library,
                           msg=msg,
                           google_credentials=credentials_data or {},
                           google_client_data=google_client_data or {},
                           dropbox_credentials=dropbox_credentials_data or {},
                           page='library')


@library_blueprint.route('/document/<path:bot_name>/<escaped_doc_id>')
@cu_security.verify_bot
@cu_security.verify_botconfig_field(cu_tools.BOTCONFIG_LIBRARY_SEARCH)
@cu_security.verify_permission(BotConfig.PERM_VIEW_LIBRARY)
def document(bot_name, escaped_doc_id):
    filter = cu_tools.get_filter(bot_name, request)
    filter['doc_status'] = request.args.get('doc_status')
    userid, bot_path = cu_tools.get_userid_and_bot_path(
        current_user, bot_name)
    if bot_path is None:
        return redirect('/invalid_instance/%s' % bot_name)

    is_ingestor_user = current_user.is_authenticated and \
        cu_tools.is_ingestor_user(current_user.get_id())

    doc_id = escaped_doc_id.replace('_', '/')

    forbidden_tags = cu_tools.get_forbidden_tags_by(current_user.get_id(),
                                                    bot_name)
    document = cu_database.library_storage.get_document(
        bot_path, doc_id, forbidden_tags)
    if document is None:
        return render_template(
            'error_message.html', page='error', filter={},
            error_message='Document %s not found' % doc_id)

    for passage in document.passages:
        # replace links to internal files on the text too
        # to allow copy links to images
        passage.text = engage_tools.passage_to_text(passage.text, bot_name,
                                                    doc_id,
                                                    request.url_root)
        passage.html = engage_tools.text_to_html(passage.text)
        passage.escaped_id = passage.id.replace('/', '_')

    prev = None
    next = None
    if filter['position'] is not None:
        filter['real_position'] = filter['active_page'] * \
                                  cu_tools.ENTRIES_BY_PAGE
        filter['real_position'] += filter['position'] + 1

        prev, next = cu_database.library_storage.get_queue_prev_next_ids(
            bot_path,
            filter['position'],
            text_filter=filter['text_filter'],
            since=filter['since'],
            start=filter['active_page'] * cu_tools.ENTRIES_BY_PAGE,
            status=filter['doc_status'],
            descending=(filter['direction'] == '-1'))

    bot_config = get_bot_config(bot_name)
    secure_content = bot_config.get_secure_content() == 'true'
    if secure_content:
        secure_content = not cu_security.has_permission(
            bot_config, BotConfig.PERM_OVER_CONTENT_PROTECT)
    logger.debug('Secure content: %s' % secure_content)
    logger.debug('document: %s' % document.stored_name)
    return render_template('document.html', filter=filter, document=document,
                           next=next, prev=prev,
                           page='library', is_ingestor_user=is_ingestor_user,
                           secure_content=secure_content)


def _reset_search_cache(bot_name, doc_id):
    engage = cu_tools.init_engage(bot_name, current_user=current_user)
    engage.sync_object(doc_id, engage_tools.OBJ_TYPE_DOC)


@library_blueprint.route('/remove_document/<path:bot_name>/<escaped_doc_id>')
@login_required
@cu_security.verify_bot
@cu_security.verify_permission(BotConfig.PERM_MODIFY_LIBRARY)
def remove_document(bot_name, escaped_doc_id):
    bot_path = current_user.get_bot_path_by_name(bot_name)
    if bot_path is None:
        return redirect('/invalid_instance/%s' % bot_name)

    doc_id = escaped_doc_id.replace('_', '/')
    document = cu_database.library_storage.get_document(bot_path, doc_id)
    description = document.title
    try:
        # discount the words in the document from the dictionary
        engage = cu_tools.init_engage(bot_name, current_user=current_user)
        engage.update_spelldict_from_doc(doc_id, substract=True)

        cu_database.library_storage.remove(bot_path, doc_id)
        _reset_search_cache(bot_name, doc_id)

        cu_database.user_actions.add_admin_action(
            userid=current_user.get_id(),
            action=UserActions.ACTION_TYPE_DOCUMENT_REMOVAL,
            description=description,
            bot=get_bot_config(bot_name)._bot_path,
            channel=cu_database.user_actions.ACTION_CHANNEL_CU)

    except Exception as e:
        return render_template(
            'error_message.html', page='error', filter={},
            error_message=_('Exception %s') % e)

    query_string = request.query_string.decode("utf-8")
    return redirect('/library/%s?%s' % (bot_name, query_string))


@library_blueprint.route('/upload_document/<path:bot_name>',
                         methods=['POST', 'GET'])
@login_required
@cu_security.verify_bot
@cu_security.verify_permission(BotConfig.PERM_MODIFY_LIBRARY)
@cu_security.log_activity(cu_security.log_admin_activity,
                          action_type=UserActions.ACTION_TYPE_DOCUMENT_UPLOAD,
                          description_handler=cu_security.
                          document_upload_description_handler)
def upload_document(bot_name):
    doc_url = request.form.get('doc_url', '')
    origin = request.args.get('origin', '')
    # if is the new version of a document, we get the id of the prev doc
    update_doc_id = request.form.get('update_doc_id', '')

    if not doc_url:
        doc_url = request.args.get('doc_url', '')

    if doc_url:
        url_list = doc_url.split(',') if ',' in doc_url else [doc_url]
        for url in url_list:
            if origin != 'dropbox':
                origin = url
            new_doc, error = _document_from_url(url, bot_name, update_doc_id,
                                                origin=origin)
            if not error:
                doc_id = cu_tools.object_id_to_path(new_doc['_id'])
                cu_tools.send_upload_doc_notification(bot_name, doc_id,
                                                      new_doc['title'])
    else:
        files = request.files.getlist("files")
        if files:
            for _file in files:
                _document_from_file(_file, bot_name, update_doc_id)

        else:
            return render_template(
                'error_message.html', page='error', filter={},
                error_message=_('Please select a file to upload'))

    query_string = request.query_string.decode("utf-8")
    query_string += '&msg_code=1' if query_string else 'msg_code=1'
    return redirect('/library/%s?%s' % (bot_name, query_string))


@library_blueprint.route(
    '/download_document/<path:bot_name>/<escaped_doc_id>')
@cu_security.verify_bot
@cu_security.verify_botconfig_field(cu_tools.BOTCONFIG_LIBRARY_SEARCH)
def download_document(bot_name, escaped_doc_id):
    userid, bot_path = cu_tools.get_userid_and_bot_path(
        current_user, bot_name)
    if bot_path is None:
        return redirect('/invalid_instance/%s' % bot_name)

    doc_id = escaped_doc_id.replace('_', '/')
    document = cu_database.library_storage.get_document(bot_path, doc_id)
    filename = document.stored_name

    file_manager = FileManager('cert/aws-access.json')
    file_content, file_data = file_manager.download_object(
        filename, document.bucket)

    cu_database.user_actions.add_admin_action(
        userid=current_user.get_id(),
        action=UserActions.ACTION_TYPE_DOCUMENT_DOWNLOAD,
        description=document.title,
        bot=get_bot_config(bot_name)._bot_path,
        channel=cu_database.user_actions.ACTION_CHANNEL_CU)

    headers = Headers()
    headers.add('Content-Type', file_data['ContentType'])
    headers.add('Content-Disposition', 'attachment',
                filename=os.path.basename(filename))

    if file_data:
        file_data['Body'] = io.BytesIO(file_content)
        file = FileWrapper(file_data['Body'])
        return Response(file, headers=headers)
    else:
        return None


@library_blueprint.route(
    '/get_image/<path:bot_name>/<escaped_doc_id>/<filename>')
@cu_security.verify_bot
def get_image(bot_name, escaped_doc_id, filename):
    userid, bot_path = cu_tools.get_userid_and_bot_path(
        current_user, bot_name)
    if bot_path is None:
        return redirect('/invalid_instance/%s' % bot_name)

    engage = cu_tools.init_engage(bot_name, current_user=current_user)
    bucket, storage_folder = engage.get_document_storage_location(
        escaped_doc_id)

    file_manager = FileManager()
    file_content, file_data = file_manager.download_object(
        os.path.join(storage_folder, filename),
        bucket)

    if file_data:
        file_data['Body'] = io.BytesIO(file_content)
        file = FileWrapper(file_data['Body'])
        response = Response(file, mimetype=file_data['ContentType'])
        response.headers['Cache-Control'] = 'public, max-age=360'
        return response
    else:
        return None


@library_blueprint.route(
    '/upload-processed-file/<path:bot_name>', methods=['POST'])
@login_required
def upload_processed_file(bot_name):
    engage = cu_tools.init_engage(bot_name, current_user=current_user)
    doc_id = request.form.get("docPath", '')
    bot_id = current_user.get_bot_path_by_name(bot_name)

    # Save zip file to tmp
    files = request.files.getlist("processedFile")
    file = files[0] if files else None

    temp_zip_file_path = os.path.join(TMP_FOLDER, file.filename)
    file.save(temp_zip_file_path)
    file_manager = FileManager('cert/aws-access.json')
    engage.process_doc_zip_file(temp_zip_file_path, doc_id, file_manager)

    os.remove(temp_zip_file_path)

    cu_tools.send_ingested_doc_notification(bot_name, doc_id)

    document = cu_database.library_storage.get_document(bot_id, doc_id)
    if document.supersedes_to:
        expert, ingestor_contact = get_config_to_notify_user(
            get_bot_config(bot_name))
        engage.nofity_version_change(bot_id, document.supersedes_to, doc_id,
                                     expert, ingestor_contact,
                                     current_user.get_id())

    return redirect('/library/%s' % bot_name)


def _document_from_url(doc_url, bot_name, update_doc_id=None, origin=None):
    '''
    Relies on _document_from_temp_file for creating a document from a URL

    Parameters
    ----------
    doc_url : str
        the URL for the file to be uploaded
    bot_name : str
        a bot name as 'alley'
    update_doc_id : str
        the original document id if the uploaded document is a new version of
        an existing document

    Returns
    -------
        document, error_code
    '''

    filename = _get_filename_from_url(doc_url)
    tmp_filename = generate_unique_filename(filename)
    tmp_full_path = os.path.join(TMP_FOLDER, tmp_filename)
    opener = urllib.request.URLopener()
    tmp_full_path, headers = opener.retrieve(doc_url, tmp_full_path)
    return _document_from_temp_file(filename, tmp_filename, tmp_full_path,
                                    bot_name, update_doc_id, origin)


def _document_from_file(file, bot_name, update_doc_id=None):
    '''
    Relies on _document_from_temp_file for creating a document from an
    uploaded file

    Parameters
    ----------
    file : file
        the uploaded file object
    bot_name : str
        a bot name as 'alley'
    update_doc_id : str
        the original document id if the uploaded document is a new version of
        an existing document

    Returns
    -------
        document, error_code

    '''
    filename = file.filename
    tmp_filename = generate_unique_filename(filename)
    tmp_full_path = os.path.join(TMP_FOLDER, tmp_filename)
    file.save(tmp_full_path)
    return _document_from_temp_file(filename, tmp_filename, tmp_full_path,
                                    bot_name, update_doc_id)


def _document_from_temp_file(filename, tmp_filename, tmp_full_path,
                             bot_name, update_doc_id=None, origin=None):
    '''
    Creates a document from a temporary file in the local drive. Uploads file
    to S3. In case of the file being an image, creates a passage, including
    the image in it.

    Parameters
    ----------
    filename : str
        the uploaded file name
    tmp_filename : str
        the filename in the local drive
    tmp_full_path : str
        the filename in the local drive
    bot_name : str
        the bot name as 'alley'
    update_doc_id : str
        the original document id if the uploaded document is a new version of
        an existing document
    origin : str
        the file origin (dropbox, gdrive, etc)

    Returns
    -------
        document, error_code

    '''
    mimetype = mimetypes.guess_type(filename)
    if mimetype:
        mimetype = mimetype[0]

    file_manager = FileManager()

    document = {}
    error_code = 0

    try:

        document['title'] = filename
        document['content-type'] = mimetype
        document['uploaded_by'] = current_user.get_id()
        if origin:
            document['source'] = origin

        if update_doc_id:
            # add the information about what is the document
            # document is replaced
            document['supersedes-to'] = update_doc_id

        engage = cu_tools.init_engage(bot_name, current_user=current_user)
        bot_id = engage.get_bot_id()
        doc_id = cu_database.library_storage.upload_document(
            bot_id, document)
        doc_id = tools.object_id_to_path(doc_id)

        bucket, storage_folder = engage.get_document_storage_location(doc_id)

        file_url, success, destination = file_manager.upload(
            tmp_full_path, bucket=bucket,
            public=False, keep_filename=True,
            destination_folder=storage_folder)

        changes = {}
        changes['stored'] = file_url
        changes['stored_name'] = destination
        changes['bucket'] = bucket
        logger.debug('Update document doc_id: %s data %s' % (doc_id, changes))
        cu_database.library_storage.update_document(
            doc_id, bot_id, changes)

        if 'image' in document.get('content-type'):
            passage_text = []
            image_name = '[file:///' + tmp_filename + ']'
            passage_text.append(image_name)
            passages, title = cu_database.library_storage. \
                _get_passages_from_textlines(passage_text)

            cu_database.library_storage._add_passages(bot_id,
                                                      doc_id,
                                                      passages,
                                                      update_doc=True,
                                                      title=title)

    except Exception:
        logger.exception('Error uploading document')
        error_code = 1

    os.remove(tmp_full_path)
    return document, error_code


def _get_filename_from_url(file_url):
    name, ext = os.path.splitext(file_url)
    return name.rsplit('/', 1)[-1] + ext if ext else ''


def _redirect_prev_page(action, bot_name, request):
    # this page do not have more issues, redirect to the prev page
    query_string = ''
    for arg, value in request.args.items():
        if arg != 'page':
            query_string += '%s=%s&' % (arg, value)
        else:
            try:
                prev_page = int(request.args.get('page', '0')) - 1
            except Exception:
                prev_page = 0
            query_string += 'page=%d&' % prev_page
    return redirect('/%s/%s?%s' % (action, bot_name, query_string))
