# -*- coding: utf-8 -*-
'''
Security for the control unit Ingestor application
'''

# Copyright Gonzalo Odiard, Ingestor 2019

# This file is part of Ingestor's Knowbot platform and is copyright
# and a valuable trade secret of Ingestor, Inc.

import logging
import os
from functools import wraps
from cu_authentication import current_user
from flask import request, render_template, session, g
from flask import Blueprint, redirect
from flask_login import AnonymousUserMixin
from flask_babel import _
from engage.botconfig import BotConfig
import cu_database
import cu_tools
from cu_tools import get_bot_config
from cu_users import User, PUBLIC_USER_ID
from ingestor_security import LoginManager

# get random key from a file if exist
secret_key = cu_tools.get_persistent_secret_key()

ROLE_VALUES = {'admin': 0, 'expert': 1, 'user': 2}
VALUE_ROLES = {0: 'admin', 1: 'expert', 2: 'user'}

# configure logger
logging.basicConfig(level=os.getenv('LOGGER_LEVEL', logging.WARNING))
logger = logging.getLogger('security')
public_bot = cu_database.public_bot

security_blueprint = Blueprint(
    'security', __name__, template_folder='templates')

MSG_PASS_EXPIRED = _('Your password has expired. Please change it.')
MSG_MAX_INVALID_LOGIN = _('Maximum number of invalid login exceeded.')
MSG_INVALID_IP = _('Your ip is not allowed to access this bot')
MSG_USER_WITHOUT_BOT = _('Your user is not associated to any bot.<br>'
                         'Please contact to Ingestor')
MSG_ERROR_SETTING_PASS = _('Error setting user password. Try again')
MSG_ERROR_LOGINCODE_EXPIRED = _('Login code has expired. Try again')
auth_manager = LoginManager()


def login_required(f):
    @wraps(f)
    def decorated(*args, **kwargs):
        if 'profile' not in session:
            # Redirect to Login page here
            return redirect('/request_login')
        else:
            return f(*args, **kwargs)

    return decorated


def verify_bot(func):
    @wraps(func)
    def wrapper(*args, **kwargs):
        return func(*args, **kwargs)
    return wrapper


def verify_permission(permission):
    def decorator(func):
        @wraps(func)
        def wrapper(*args, **kwargs):
            bot_name = kwargs['bot_name']
            bot_config = get_bot_config(bot_name)
            granted = has_permission(bot_config, permission)
            if not granted:
                return render_template(
                    'error_message.html', page='error', filter={},
                    error_message=_('Your role at %s does not have '
                                    'permission to '
                                    'access this page') % bot_name), 401
            else:
                return func(*args, **kwargs)
        return wrapper
    return decorator


def log_authetication_activity(func, action_type=None, *args, **kwargs):
    logger.debug("logging activity in :" + func.__name__)
    action = func.__name__
    if action_type:
        action = action_type
    cu_database.user_actions.add_admin_action(
        current_user.get_id(), action, "",
        channel=cu_database.user_actions.ACTION_CHANNEL_CU)


def log_admin_activity(func, action_type=None,
                       description_handler=None,
                       *args, **kwargs):
    logger.debug("logging activity in :" + func.__name__ + " for bot: ")
    action = func.__name__
    description = ""
    if description_handler:
        description = description_handler(request)
    if action_type:
        if callable(action_type):
            action = action_type()
        else:
            action = action_type
    bot_path = None
    if 'bot_config' in g:
        bot_path = g.bot_config.get_bot_path()
    cu_database.user_actions.add_admin_action(
        userid=current_user.get_id(), action=action,
        description=description, bot=bot_path,
        channel=cu_database.user_actions.ACTION_CHANNEL_CU)


def document_upload_description_handler(request):
    description = 'Uploaded file'
    if request.files:
        description = request.files['files'].filename
    elif 'doc_url' in request.args:
        description = request.args['doc_url']
    return description


def log_activity(logging_function, action_type=None, description_handler=None,
                 execute_before=False):
    def decorate(func):
        @wraps(func)
        def call(*args, **kwargs):
            # logout interception executed before function in order to
            # keep user data
            if execute_before:
                logging_function(func, action_type, description_handler,
                                 *args, **kwargs)
            result = func(*args, **kwargs)
            if not execute_before:
                logging_function(func, action_type, description_handler,
                                 *args, **kwargs)
            return result
        return call
    return decorate


def get_roles_at_bot(bot_name: str = None):
    if not bot_name:
        return []

    user_roles_for_bot = current_user.get_roles_by_bot().get(
        bot_name, {}).get('roles')

    return user_roles_for_bot


def has_permission(bot_config, permission):
    granted = False
    if current_user.is_anonymous and bot_config.get_public() == 'true':
        role = bot_config.get_roles().get('user')
        granted = role and permission in role.get('permissions', [])
    elif bot_config.get_roles():
        bot_name = bot_config.get_bot_name()
        user_roles_for_bot = get_roles_at_bot(bot_name)
        granted = False
        roles = bot_config.get_roles()
        if roles:
            for roleName in user_roles_for_bot:
                role = roles.get(roleName)
                if permission in role.get('permissions'):
                    granted = True
                    break
        return granted
    return granted


def get_homepage(bot_config):
    homepage = None
    if bot_config.get_roles():
        roles = bot_config.get_roles()
        for roleName in roles:
            role = roles.get(roleName)
            if current_user.get_id() in role.get('users') \
                    and role.get('homepage'):
                homepage = role.get('homepage')
                break
    return homepage


def verify_botconfig_field(config_field):
    def decorator(func):
        @wraps(func)
        def wrapper(*args, **kwargs):
            return func(*args, **kwargs)
        return wrapper
    return decorator


def load_user(user_id):
    user = User(user_id)
    user.is_ingestor_user = cu_tools.is_ingestor_user(user_id)
    return user


@security_blueprint.route('/bots')
@login_required
def bots():
    # for compatibility with old links
    return redirect('/instances')


@security_blueprint.route('/instances')
@login_required
def instances():
    cu_tools.refresh_session_profile()
    my_bots = current_user.get_bots_names()
    print(current_user._get_authzero_profile())
    # remove ama from bot list
    if 'ama' in my_bots.keys():
        del my_bots['ama']

    if len(my_bots) == 0:
        return render_template(
            'error_message.html', page='error', filter={},
            error_message=MSG_USER_WITHOUT_BOT)

    if len(my_bots) == 1:
        bot_name = list(my_bots.keys())[0]
        logger.debug('Only one bot. Redirecting via bot_root(%s)' % bot_name)
        return redirect('/instance_root/%s' % bot_name)
    return render_template('bots.html', bots=my_bots, page='bots', filter={})


@security_blueprint.route('/invalid_instance/<path:bot_name>')
def invalid_instance(bot_name):
    return render_template(
        'error_message.html', page='error', filter={},
        error_message=_('Instance %s not found or without '
                        'permission') % bot_name)


@security_blueprint.route('/instance_root/<path:bot_name>')
@login_required
@verify_bot
def instance_root(bot_name):
    bot_config = get_bot_config(bot_name)
    homepage = get_homepage(bot_config)

    if homepage is not None and homepage['url']:
        return redirect(homepage['url'] + bot_name)

    if has_permission(bot_config, BotConfig.PERM_VIEW_DASHBOARD):
        return redirect('/dashboard/%s' % bot_name)
    else:
        return redirect('/ask/%s' % bot_name)

    # return redirect('/ask/%s' % bot_name)


class IngestorAnonymousUser(AnonymousUserMixin):

    def get_id(self):
        return PUBLIC_USER_ID

    def get_token(self):
        credentials, error = auth_manager.\
            get_credentials_by_userid(PUBLIC_USER_ID)
        if credentials and not error:
            return credentials['token'].decode()
        else:
            return None
