# -*- coding: utf-8 -*-
'''
A application to configure the Ingestor services
'''

# Copyright Gonzalo Odiard, Ingestor 2018

# This file is part of Ingestor's Knowbot platform and is copyright
# and a valuable trade secret of Ingestor, Inc.

import cu_tools
from cu_tools import get_bot_config
import cu_database
import cu_google
import cu_stats
import cu_settings
import cu_security
import cu_library
import visualize
import logging
import os
import json

from ingestor_storage import QueueEntry, Question
from ingestor_storage import STATUS_VERIFIED_INPUT
from ingestor_storage import Users
from ingestor_storage.actions import UserActions
from ingestor_storage import path_to_id_str
from ingestor_storage import tools as storage_tools

from engage import engage_tools
from engage.botconfig import get_config_to_notify_expert, \
    get_bot_config_by_name, BotConfig, create_bot, remove_bot
from engage.botconfig import get_config_to_notify_user

from ingestor_storage import EngageUpdater, object_id_to_path
from ingestor_storage import path_type_to_object_id
from cu_security import login_required
from cu_authentication import current_user
import ingestor_security

from flask import Flask, request, render_template, redirect, jsonify, g
from flask import session
from flask_babel import Babel, _
from babel.support import Translations
from config import AppConfig
from cu_stats import MUNSELL_COLORS
from multiprocessing import Value

from engage.botconfig import get_all_bots_config
from jinja2 import Environment, PackageLoader
from engage.email_tools import send_email, render_email_template

from authlib.integrations.flask_client import OAuth
from urllib.parse import urlencode, quote_plus
from flask import url_for

# configure loggers
logging.basicConfig(level=os.getenv('LOGGER_LEVEL', logging.WARNING))
logger = logging.getLogger('app')

engage_logger = logging.getLogger('engage')
engage_logger.setLevel(level=os.getenv('LOGGER_LEVEL', logging.WARNING))

werkzeug_logger = logging.getLogger('werkzeug')
werkzeug_logger.setLevel(logging.ERROR)

# reduce log from boto3 is very verbose
logging.getLogger('botocore').setLevel(logging.CRITICAL)
logging.getLogger('boto3').setLevel(logging.CRITICAL)

# get random key from a file if exist
secret_key = cu_tools.get_persistent_secret_key()

app = Flask(__name__)
app.secret_key = secret_key
app.register_blueprint(cu_security.security_blueprint)
app.register_blueprint(cu_google.google_drive)
app.register_blueprint(cu_stats.stats_blueprint)
app.register_blueprint(cu_settings.settings_blueprint)
app.register_blueprint(cu_library.library_blueprint)
app.send_file_max_age_default = 360  # one hour
app.config.from_object(AppConfig())
babel = Babel(app)
auth_manager = ingestor_security.LoginManager()
#  check if there are a default instance configured

oauth = OAuth(app)
AUTH_DOMAIN = os.getenv('AUTHZERO_DOMAIN')
AUTHZERO_CLIENT_ID = os.getenv('AUTHZERO_CLIENT_ID')
AUTHZERO_CLIENT_SECRET = os.getenv('AUTHZERO_CLIENT_SECRET')
AUTHZERO_URL = 'https://' + AUTH_DOMAIN
auth0 = oauth.register(
    'auth0',
    client_id=AUTHZERO_CLIENT_ID,
    client_secret=AUTHZERO_CLIENT_SECRET,
    api_base_url=AUTHZERO_URL,
    access_token_url=AUTHZERO_URL + '/oauth/token',
    authorize_url=AUTHZERO_URL + '/authorize',
    client_kwargs={
        'scope': 'openid profile email',
    },
)

DEFAULT_INSTANCE = os.getenv('DEFAULT_INSTANCE')

# Flag to avoid start multiple schedulers
scheduler_started = Value('b', False)
users = Users()


@app.route('/callback')
def callback_handling():
    logger.debug('executing auth0 callback')
    # Handles response from token endpoint
    response_of_auth = auth0.authorize_access_token()
    logger.debug(response_of_auth)
    resp = auth0.get('userinfo').json()
    userid = resp['sub']
    cu_tools.refresh_session_profile(userid)
    return redirect('/instances')


@app.errorhandler(404)
def page_not_found(e):
    # not found
    return render_template('error.html', filter={}, error_code=404), 404


@app.errorhandler(403)
def access_forbiden(e):
    # forbiden
    return render_template('error.html', filter={}, error_code=403), 403


@app.errorhandler(410)
def resource_gone(e):
    # gone
    return render_template('error.html', filter={}, error_code=410), 410


@app.errorhandler(500)
def internal_error(e):
    # internal server error
    return render_template('error.html', filter={}, error_code=500), 500


@app.route('/')
@login_required
def home():
    next = request.args.get('next', '')
    if DEFAULT_INSTANCE:
        if DEFAULT_INSTANCE in cu_database.public_bot.get_bot_names():
            return redirect('/ask/%s' % DEFAULT_INSTANCE)
    logger.debug('******    ON HOME next: "%s"' % next)
    return render_template('home.html', page='home', filter={},
                           operation='login',
                           next=next)


@app.route('/request_login')
def login():
    url_root = request.url_root
    if not url_root.startswith('http://localhost'):
        # auth0 request a https callback url
        url_root = url_root.replace('http://', 'https://')

    return auth0.authorize_redirect(
        redirect_uri=url_root + 'callback')


@app.route('/logout')
def logout():
    # Clear session stored data
    session.clear()
    # Redirect user to logout endpoint

    url_home = url_for('home', _external=True)
    if not url_home.startswith('http://localhost'):
        # auth0 request a https callback url
        url_home = url_home.replace('http://', 'https://')

    params = {
        'returnTo': url_home,
        'client_id': AUTHZERO_CLIENT_ID
    }
    return redirect(auth0.api_base_url + '/v2/logout?' +
                    urlencode(params, quote_via=quote_plus))


@app.route('/welcome')
def welcome():
    return render_template('welcome-page.html', filter={})


@app.route("/get_user_preferences/")
@login_required
def get_user_preferences():
    bot_name = request.args.get('bot', '')
    userid = current_user.get_id()
    user_preference = cu_database.users.get_user(userid)
    if bot_name and bot_name in cu_database.public_bot.get_bot_names():
        # we do not want show the tour on a public bot
        # the user already saw it on his own bot
        user_preference[Users.TOUR_COMPLETED] = True
    return jsonify(user_preference)


@app.route('/get_all_bots', methods=['GET'])
@login_required
def get_all_bots():
    all_bots = []
    is_ingestor_user = cu_tools.is_ingestor_user(current_user.get_id())
    if is_ingestor_user:
        for bot in get_all_bots_config():
            bot_data = {}
            bot_data['bot_name'] = bot.get_bot_name()
            bot_data['display_name'] = bot.get_display_name()
            bot_data['sponsor'] = bot.get_sponsor()
            all_bots.append(bot_data)
    return jsonify({"all_bots": all_bots})


@app.route('/add_new_bot', methods=['POST'])
@login_required
def add_new_bot():
    bot_data = request.json

    res = {}
    res['status_code'] = 200

    is_ingestor_user = cu_tools.is_ingestor_user(bot_data['sponsor'])
    if is_ingestor_user:
        check_bot_name = cu_tools.check_bot_name_exists(bot_data['bot_name'])
        if not check_bot_name:
            create_bot(bot_data['bot_name'], bot_data['display_name'],
                       bot_data['sponsor'])
            return jsonify(res)
        else:
            res['error_message'] = _('This name is already '
                                     'used %s') % bot_data['bot_name']
            response = jsonify(res)
            response.status_code = 400
            return response

    res['error_message'] = _('Sponsor must be a ingestor user')
    response = jsonify(res)
    response.status_code = 400
    return response


@app.route('/delete_bot', methods=['POST'])
@login_required
def delete_bot():
    bot_data = request.json

    res = {}
    res['status_code'] = 200

    is_ingestor_user = cu_tools.is_ingestor_user(current_user.get_id())
    bot_config = get_bot_config_by_name(bot_data['bot_name'])
    sponsor = bot_config.get_sponsor()

    if is_ingestor_user:
        if current_user.get_id() == sponsor:
            remove_bot(bot_data['bot_name'], bot_data['sponsor'])
            return jsonify(res)
        else:
            res['error_message'] = _('You are not a sponsor of the bot')
            response = jsonify(res)
            response.status_code = 404
            return response

    res['error_message'] = _('You are not a user of Ingestor')
    response = jsonify(res)
    response.status_code = 404
    return response


@app.route('/api/ingestor_users', methods=['GET'])
@login_required
def ingestors_users():
    ingestor_users_ids = []
    ingestor_users = []
    is_ingestor_user = cu_tools.is_ingestor_user(current_user.get_id())
    if is_ingestor_user:
        cu_database.config.reset()
        for user_id in cu_database.config.get_ingestor_users():
            user_data = user_id
            if '@' not in user_data:
                user_data = cu_database.users.get_userid_by_phone_number(
                            user_data)
                user_data = user_id if user_data is None else user_data
            ingestor_users_ids.append(user_data)

        for ingestor_user in cu_database.users.get_users(ingestor_users_ids):
            ingestor_data = {}
            ingestor_data['user_id'] = ingestor_user['userid']
            ingestor_data['full_name'] = ingestor_user.get('full-name')
            ingestor_users.append(ingestor_data)

    return jsonify({"ingestor_users": ingestor_users})


@app.route('/api/add_ingestor_user', methods=['POST'])
@login_required
def add_ingestor_user():
    phone_or_userid = request.json['user_id']

    res = {}
    res['error_message'] = None

    user_id = ''
    if '@' in phone_or_userid:
        user_id = cu_database.users.get_user(phone_or_userid)
    else:
        user_id = cu_database.users.get_userid_by_phone_number(
                                                    phone_or_userid)

    if user_id is None:
        res['error_message'] = _('That user does not exist')

    if cu_tools.is_ingestor_user(phone_or_userid):
        res['error_message'] = _('The user exists: %s'
                                 ) % phone_or_userid

    if res['error_message']:
        response = jsonify(res)
        response.status_code = 400
        return response

    res['status_code'] = 200
    res['message'] = _('New Ingestor user added')
    cu_database.config.add_ingestor_user(phone_or_userid)
    return jsonify(res)


@app.route('/api/remove_ingestor_user', methods=['POST'])
def remove_user():
    userid = request.json['user_id']

    res = {}
    res['error_message'] = None

    if not cu_tools.is_ingestor_user(userid):
        res['error_message'] = _('This user does not exist')

    if userid == current_user.get_id():
        res['error_message'] = _("You can't remove yourself")

    if res['error_message']:
        response = jsonify(res)
        response.status_code = 404
        return response

    res['status_code'] = 200
    res['message'] = _('The user %s has been removed from the list of Ingestor'
                       ' users') % userid
    cu_database.config.delete_ingestor_user(userid)
    return jsonify(res)


@app.route("/accept_cookies")
@login_required
def accept_cookies():
    userid = current_user.get_id()
    result = cu_database.users.update_user(
        userid, {Users.ACCEPT_COOKIES: True})
    return jsonify({'result': result})


@app.route("/tour_completed")
@login_required
def tour_completed():
    userid = current_user.get_id()
    result = cu_database.users.update_user(
        userid, {Users.TOUR_COMPLETED: True})
    return jsonify({'result': result})


@app.route('/issues/<path:bot_name>')
@login_required
@cu_security.verify_bot
@cu_security.verify_permission(permission=BotConfig.PERM_VIEW_ISSUES)
def issues(bot_name):
    bot_path = current_user.get_bot_path_by_name(bot_name)
    if bot_path is None:
        return redirect('/invalid_instance/%s' % bot_name)

    bot_data = get_bot_config(bot_name)
    forbidden_tags = cu_tools.get_forbidden_tags_by(current_user.get_id(),
                                                    bot_name)
    filter_tags = sorted(bot_data.get_filter_tags())
    filter_tags = [tag for tag in filter_tags if tag not in forbidden_tags]

    logger.debug('Queue bot %s' % bot_name)
    filter = cu_tools.get_filter(bot_name, request)

    resolved = None
    if filter['solved'] == 'yes':
        resolved = True
    if filter['solved'] == 'no':
        resolved = False

    issues = cu_database.queue_storage.get_queue(
        bot_path,
        text_filter=filter['text_filter'],
        since=filter['since'],
        date=filter['date'],
        start=filter['active_page'] * cu_tools.ENTRIES_BY_PAGE,
        queue_type=filter['queue_type'],
        descending=(filter['direction'] == '-1'),
        tag=filter['tag'], forbidden_tags=forbidden_tags,
        resolved=resolved)

    if filter['active_page'] > 0 and not issues:
        # this page do not have more issues, redirect to the prev page
        return _redirect_prev_page('issues', bot_name, request)

    quantities_by_range = \
        cu_database.queue_storage.get_queue_quantities_by_range(
            bot_path, text_filter=filter['text_filter'],
            since=filter['since'], queue_type=filter['queue_type'],
            date=filter['date'],
            resolved=resolved,
            tag=filter['tag'], forbidden_tags=forbidden_tags)

    # add time lapse description
    for pos, qe in enumerate(issues):
        if qe.queue_type in engage_tools.VALID_QUEUE_TYPES:
            # for issues from searchs without text and with tags
            qe.text = qe.text or '[%s]' % _('Tag search')
            qe.color = cu_tools.queue_colors[qe.queue_type]
            qe.type_description = cu_tools.queue_type_descriptions[
                qe.queue_type]
            qe.elapsed = cu_tools.get_elapsed_desc(qe.timestamp, get_locale())
            qe.time_ago = cu_tools.format_timestamp(qe.timestamp)
        qe.position = pos

    if len(issues) < cu_tools.ENTRIES_BY_PAGE and filter['active_page'] == 0:
        quantity = len(issues)
    else:
        quantity = cu_database.queue_storage.get_queue_quantity(
            bot_path,
            text_filter=filter['text_filter'],
            since=filter['since'], queue_type=filter['queue_type'],
            date=filter['date'],
            resolved=resolved,
            tag=filter['tag'], forbidden_tags=forbidden_tags)

    filter['quantity'] = quantity
    filter['quantities_by_range'] = quantities_by_range
    filter['pages'] = cu_tools.prepare_pages_array(
        quantity, filter['active_page'])
    entries = 'entry' if quantity == 1 else 'entries'
    return render_template('issues.html', filter=filter, issues=issues,
                           page='issues', entries=entries,
                           filter_tags=filter_tags)


def _redirect_prev_page(action, bot_name, request):
    # this page do not have more issues, redirect to the prev page
    query_string = ''
    for arg, value in request.args.items():
        if arg != 'page':
            query_string += '%s=%s&' % (arg, value)
        else:
            try:
                prev_page = int(request.args.get('page', '0')) - 1
            except Exception:
                prev_page = 0
            query_string += 'page=%d&' % prev_page
    return redirect('/%s/%s?%s' % (action, bot_name, query_string))


@app.route('/issue/<path:bot_name>/<entry_id>/')
@login_required
@cu_security.verify_bot
@cu_security.verify_permission(BotConfig.PERM_VIEW_ISSUES)
def issue(bot_name, entry_id):
    engage = cu_tools.init_engage(bot_name, current_user=current_user)
    bot_path = current_user.get_bot_path_by_name(bot_name)
    if bot_path is None:
        return redirect('/invalid_instance/%s' % bot_name)

    filter = cu_tools.get_filter(bot_name, request)

    resolved = None
    if filter['solved'] == 'yes':
        resolved = True
    if filter['solved'] == 'no':
        resolved = False

    prev = None
    next = None
    if filter['position'] is not None:
        filter['real_position'] = filter['active_page'] * \
                                  cu_tools.ENTRIES_BY_PAGE
        filter['real_position'] += filter['position'] + 1
        prev, next = cu_database.queue_storage.get_queue_prev_next_ids(
            bot_path,
            filter['position'],
            text_filter=filter['text_filter'],
            since=filter['since'],
            date=filter['date'],
            start=filter['active_page'] * cu_tools.ENTRIES_BY_PAGE,
            queue_type=filter['queue_type'],
            descending=(filter['direction'] == '-1'),
            resolved=resolved,
            tag=filter['tag'])

    entry_id = entry_id.replace('_', '/')
    # remove 'U' from the begginig (old format)
    entry_id = entry_id.lstrip('U')
    engage_updater = EngageUpdater(engage)
    issue, ecode = engage_updater.get_object(engage_tools.OBJ_TYPE_QUEUE,
                                             entry_id)
    if ecode == engage.OBJECT_NOT_FOUND:
        return render_template(
            'error_message.html', page='error', filter={}, error_code=500,
            error_message=_('Issue with id %s not found') % entry_id)

    if ecode != 0:
        return render_template(
            'error_message.html', page='error', filter={}, error_code=500,
            error_message=_('Backend error %s') % issue)

    if 'logid' in issue:
        log_id = issue['logid']
        log_data, ecode = engage.get_object(log_id, engage_tools.OBJ_TYPE_LOG)
        if ecode != 0:
            logger.error('Log %s not found' % log_id)
            log_data = {}
    else:
        log_data = {}
    question_text = issue['text']
    # for issues from searchs without text and with tags
    question_text = question_text or '[%s]' % _('Tag search')
    qe = QueueEntry(issue['_id'], bot_name, issue['timestamp'],
                    question_text, issue['queue_type'])
    qe.tags = issue.get('tags')
    qe.color = cu_tools.queue_colors[qe.queue_type]
    qe.elapsed = cu_tools.get_elapsed_desc(issue['timestamp'], get_locale())
    qe.time_ago = cu_tools.format_timestamp(issue['timestamp'])
    qe.type_description = cu_tools.queue_type_descriptions[qe.queue_type]

    if 'userid' in log_data:
        qe.userid = log_data['userid']
    else:
        qe.userid = None

    if 'status' in issue:
        qe.status = issue['status']

    if qe.queue_type == engage_tools.QUEUE_TYPE_TOO_VAGUE:
        qe.matches = _get_input_matches(engage, bot_name, issue)

    if qe.queue_type == engage_tools.QUEUE_TYPE_NEEDS_REVIEW:
        qe.response = engage_tools.text_to_html(str(issue['response']))
        qe.input_id = issue.get('question_id')
        qe.response_id = issue.get('answer_id')
        qe.userid = issue.get('submitted_by')

        inputid = issue.get('question_id')

        if inputid:
            question_object = cu_database.qa.get_by_id(bot_path, inputid)
            if question_object is not None:
                _load_responses_dicts(question_object)

                # prepare a list with other responses to show to the user
                # we will need have a way to let the user decide if remove
                # the other responses
                other_responses = {}
                # first with responses
                for resp_id, resp_data in \
                        question_object.responses_dict.items():
                    if resp_id != issue.get('answer_id'):
                        resp_data['verified'] = False
                        other_responses[resp_id] = resp_data
                # second with verified responses
                for resp_id, resp_data in \
                        question_object.verified_responses_dict.items():
                    if resp_id != issue.get('answer_id'):
                        resp_data['verified'] = True
                        other_responses[resp_id] = resp_data
                qe.other_responses = other_responses

    else:
        if 'userid' in log_data:
            qe.userid = log_data['userid']

    if qe.queue_type == engage_tools.QUEUE_TYPE_LIBRARY_ONLY:
        qe.passages = []
        for document in log_data.get('library_results'):
            doc_id = object_id_to_path(document['doc_id'])
            for passage in document.get('passages'):
                text = engage_tools.passage_to_text(
                    passage['text'], bot_name, doc_id, request.url_root)
                html = engage_tools.text_to_html(text)
                qe.passages.append({'text': text, 'html': html})

    if qe.queue_type == engage_tools.QUEUE_TYPE_REACTION:

        qe.related_input = _get_input_text(
            engage, log_data, issue.get('inputid'))
        qe.response, qe.response_formated = _get_response_text(
            engage, log_data, issue.get('responseid'))

        # if the reaction is associated to a passage, display it
        if issue.get('passageid'):
            passage_selected = issue.get('passageid')
            passage_selected_id = path_type_to_object_id(
                passage_selected, engage_tools.OBJ_TYPE_PASSAGE)
            qe.response_formated = passage_selected
            for document in log_data.get('library_results'):
                doc_id = object_id_to_path(document['doc_id'])
                for passage in document.get('passages'):
                    if passage_selected_id == passage['_id']:
                        text = engage_tools.passage_to_text(
                            passage['text'], bot_name, doc_id,
                            request.url_root)
                        html = engage_tools.text_to_html(text)
                        qe.response = text
                        qe.response_formated = html

        qe.comment = issue.get('comment')
        qe.reaction = issue.get('reaction')
        if qe.reaction:
            qe.reaction = qe.reaction.replace(':', '')
        # TODO: remove after fix in slackbot
        if qe.reaction == 'disappointment':
            qe.reaction = 'disappointed'
        qe.reaction_char = cu_tools.queue_reactions.get(qe.reaction)

    if qe.queue_type == engage_tools.QUEUE_TYPE_MISUNDERSTOOD:
        qe.related_input = _get_input_text(engage, log_data)
        qe.comment = issue.get('comment')

        qe.related_responses = {}
        if 'responses' in log_data:
            for response in log_data['responses']:
                try:
                    _input_id = engage_tools.get_list_from_data(
                        response.get('via'))[0]
                    question = log_data.get('textrefs', {}).get(_input_id)
                    qe.related_responses[response.get('answer')] = {
                        'text': response.get('text'),
                        'formated':
                            engage_tools.text_to_html(response.get('text')),
                        'question': engage_tools.text_to_html(question)}
                except Exception:
                    logger.exception('can not get info for bot: %s, queue %s'
                                     % (bot_name, entry_id))
    if qe.userid:
        qe.short_description = _('%(type)s, created %(when)s by %(user)s') % \
                               {'type': qe.type_description,
                                'when': qe.elapsed,
                                'user': qe.userid}
    else:
        qe.short_description = _('%(type)s, created %(when)s ') %\
                               {'type': qe.type_description,
                                'when': qe.elapsed}

    return render_template('issue.html', filter=filter, prev=prev,
                           next=next, entry=qe, page='issues')


def _get_input_text(engage, log_data, input_id_selected=None):
    '''
    If input_id_selected is provided, get that input
    '''
    original_input_text = log_data.get('input')
    # search at matches
    if 'matches' in log_data:
        matches = log_data['matches']
        if matches:
            if input_id_selected is None:
                if (isinstance(matches, list)):
                    input_id, error = engage_tools.normalize_object_path(
                        matches[0], engage_tools.OBJ_TYPE_INPUT)
                else:
                    input_id, error = engage_tools.normalize_object_path(
                        matches, engage_tools.OBJ_TYPE_INPUT)
            else:
                # comes from unified ask search
                input_id = input_id_selected

            if 'textrefs' in log_data and input_id in log_data['textrefs']:
                input_text = log_data['textrefs'][input_id]
                if input_text:
                    original_input_text = input_text
    return original_input_text


def _get_response_text(engage, log_data, response_id_selected=None):
    '''
    If response_id_selected is provided, get that response

    Returns
    response_text, response_text_formated
    '''
    response_text = ''
    response_text_formated = ''

    if 'responses' in log_data and log_data['responses']:
        responses = log_data['responses']
        # WORKAROUND: some times responses is [[{...}]]
        if isinstance(responses, list) and isinstance(responses[0], list):
            responses = responses[0]
        if response_id_selected is None:
            response = engage_tools.get_first_from_data(responses)
            response_text_formated = engage_tools.text_to_html(
                response['text'])
            response_text = response['text']
        else:
            for response in responses:
                if response.get('answer') == response_id_selected:
                    response_text_formated = engage_tools.text_to_html(
                        response['text'])
                    response_text = response['text']

    return response_text, response_text_formated


def _get_input_matches(engage, bot_name, entry):
    """
    Loads a Input collection, with the inputs related to the ids in 'matches'
    """
    matches = []
    # get the information about matches
    if 'matches' in entry:
        selected_input = entry.get('selected_input', None)
        if isinstance(entry['matches'], list):
            for match_input_id in entry['matches']:
                # WORKAROUND
                # some times we get a string '[]' instead of a empty array
                if match_input_id.strip() == '[]':
                    continue
                match = _get_input_from_match(
                    engage, bot_name, match_input_id, selected_input)
                if match is not None:
                    matches.append(match)
        else:
            match = _get_input_from_match(
                engage, bot_name, entry['matches'], selected_input)
            if match is not None:
                matches.append(match)
    return matches


def _get_input_from_match(engage, bot_name,
                          match_input_id, selected_input):
    input_data, ecode = engage.get_object(
        match_input_id, engage_tools.OBJ_TYPE_INPUT)
    if ecode == 0:
        match_input = Question(
            id=input_data['_id'], bot=bot_name, created=None,
            text=input_data['text'], answers=input_data.get('answers', []))
        _load_input_responses(input_data.get('textrefs', {}), match_input,
                              engage)
        match_input.selected = (match_input.id == selected_input)
        return match_input
    else:
        return None


@app.route('/visualize_issue/<path:bot_name>/<entry_id>')
@login_required
@cu_security.verify_bot
@cu_security.verify_permission(BotConfig.PERM_VIEW_ISSUES)
def visualize_issue(bot_name, entry_id):
    engage = cu_tools.init_engage(bot_name, current_user=current_user)
    entry_id = entry_id.replace('_', '/')
    # TODO: replace by queue_storage
    entry, ecode = engage.get_object(entry_id, engage_tools.OBJ_TYPE_QUEUE)
    question = entry['text']

    visualization = visualize.visualize(
        engage, question, current_user.get_token(), interactive=False,
        complete=False)
    return render_template('visualization.html', visualization=visualization)


@app.route('/issue_close/<path:bot_name>/<entry_id>/<queue_type>',
           methods=['POST', 'GET'])
@login_required
@cu_security.verify_bot
@cu_security.verify_permission(BotConfig.PERM_MODIFY_ISSUES)
def issue_close(bot_name, entry_id, queue_type):
    comment = request.form.get('notification_comment')
    logger.debug('issue_close - comment: %s' % comment)
    engage = cu_tools.init_engage(bot_name, current_user=current_user)
    queue_entry, error_code = engage.get_queue_entry(entry_id)
    engage.close_queue_entry(queue_entry, comment=comment,
                             notify=bool(comment))
    return jsonify({'ok': True})


@app.route('/bot_tags/<path:bot_name>')
@login_required
@cu_security.verify_bot
def bot_tags(bot_name):
    bot_path = current_user.get_bot_path_by_name(bot_name)
    tags = cu_database.library_storage.get_tags(bot_path)
    return jsonify(tags)


@app.route('/questions/<path:bot_name>')
@cu_security.verify_bot
@cu_security.verify_botconfig_field(cu_tools.BOTCONFIG_ANSWER_SEARCH)
@cu_security.verify_permission(BotConfig.PERM_VIEW_QA)
def questions(bot_name):
    userid, bot_path = cu_tools.get_userid_and_bot_path(
        current_user, bot_name)
    if bot_path is None:
        return redirect('/invalid_instance/%s' % bot_name)

    bot_data = get_bot_config(bot_name)

    forbidden_tags = cu_tools.get_forbidden_tags_by(current_user.get_id(),
                                                    bot_name)
    filter_tags = sorted(bot_data.get_filter_tags())
    filter_tags = [tag for tag in filter_tags if tag not in forbidden_tags]

    logger.debug('Queue bot %s' % bot_name)
    filter = cu_tools.get_filter(bot_name, request)
    queue = cu_database.qa.get_queue(
        bot_path,
        text_filter=filter['text_filter'],
        since=filter['since'],
        start=filter['active_page'] * cu_tools.ENTRIES_BY_PAGE,
        status=filter['status'],
        descending=(filter['direction'] == '-1'),
        tag=filter['tag'],
        forbidden_tags=forbidden_tags)

    if filter['active_page'] > 0 and not queue:
        # this page do not have more issues, redirect to the prev page
        return _redirect_prev_page('questions', bot_name, request)

    quantities_by_range = cu_database.qa.get_queue_quantities_by_range(
        bot_path, text_filter=filter['text_filter'])

    for pos, question in enumerate(queue):
        # TODO: see multiple questions
        question.text = question.text[0]
        question.time_ago = cu_tools.format_timestamp(question.created)
        question.modified_label = cu_tools.format_timestamp(
            question.modified)
        question.modified_elapsed = cu_tools.get_elapsed_desc(
            question.modified, get_locale())
        question.elapsed = cu_tools.get_elapsed_desc(
            question.created, get_locale())
        question.icon = cu_tools.icons[question.get_status()]
        question.desc_status = cu_tools.desc_status[question.get_status()]
        question.color = cu_tools.colors[question.get_status()]
        question.position = pos

    if len(queue) < cu_tools.ENTRIES_BY_PAGE and filter['active_page'] == 0:
        quantity = len(queue)
    else:
        quantity = cu_database.qa.get_queue_quantity(
            bot_path,
            text_filter=filter['text_filter'],
            since=filter['since'], status=filter['status'],
            tag=filter['tag'], forbidden_tags=forbidden_tags)

    filter['quantity'] = quantity
    filter['quantities_by_range'] = quantities_by_range
    filter['pages'] = cu_tools.prepare_pages_array(
        quantity, filter['active_page'])
    entries = 'entry' if quantity == 1 else 'entries'
    return render_template('questions.html', filter=filter, queue=queue,
                           page='questions', entries=entries,
                           filter_tags=filter_tags)


@app.route('/inputs_json/<path:bot_name>')
@cu_security.verify_bot
def inputs_json(bot_name):
    userid, bot_path = cu_tools.get_userid_and_bot_path(
        current_user, bot_name)
    if bot_path is None:
        return jsonify([])

    logger.debug('Queue bot %s' % bot_name)
    filter = cu_tools.get_filter(bot_name, request)
    text_filter = filter['text_filter']
    tags = request.args.get('tags', '')
    tags = tags.split(',') if tags else None

    engage = cu_tools.init_engage(bot_name, current_user=current_user)
    inputs = engage.find_questions_and_answers(text_filter, tags)

    forbidden_tags = cu_tools.get_forbidden_tags_by(current_user.get_id(),
                                                    bot_name)
    filtered_inputs = filter_by_forbidden(inputs, 'answers', forbidden_tags)

    # register the search in the log
    cu_database.logs.add_log(bot_path, bot_name, text_filter,
                             cu_database.logs.SUB_TYPE_QA_SEARCH,
                             userid)
    return jsonify(filtered_inputs)


@app.route('/inputs_typeahead/<path:bot_name>')
def inputs_typeahead(bot_name):
    userid, bot_path = cu_tools.get_userid_and_bot_path(
        current_user, bot_name)
    if bot_path is None:
        return jsonify([])
    query = request.args['q']
    # do not add inputs were text is not found
    # because the search is also done on the response
    queue = cu_database.qa.get_queue(
        bot_path,
        text_filter=query,
        quantity=10,
        status=STATUS_VERIFIED_INPUT,
        fields={'text': 1},
        only_filter_questions=True,
        exclusive_condition=True)
    inputs = [input.text[0] for input in queue]
    return jsonify(inputs)


@app.route('/passages_json/<path:bot_name>')
@cu_security.verify_bot
def passages_json(bot_name):
    text_filter = request.args.get('text_filter', '')
    tags = request.args.get('tags', '')
    spell_check_flag = (request.args.get('spell_check' == 'True'))
    userid, bot_path = cu_tools.get_userid_and_bot_path(
        current_user, bot_name)
    if bot_path is None:
        return jsonify([])

    engage = cu_tools.init_engage(bot_name, current_user=current_user)
    bot_config = get_bot_config(bot_name)
    text_filter = text_filter.lower().strip()
    tags = tags.split(',') if tags else None
    logger.debug('Text filter:"%s" - tags:"%s"' % (text_filter, tags))

    result_set = {}

    if spell_check_flag:
        logger.debug('Query before spell check:')
        logger.debug(text_filter)
        text_filter = engage.spell_corrector(text_filter)
        logger.debug('Query after spell check:')
        logger.debug(text_filter)
    result_set['input_text'] = text_filter

    expert, ingestor_contact = get_config_to_notify_expert(bot_config)
    documents, total_passages = engage.find_passages(
        text=text_filter, tags=tags, user_id=userid, log_id=None,
        url_root=request.url_root, expert=expert,
        ingestor_contact=ingestor_contact, search_notes=False)

    # register the search in the log
    cu_database.logs.add_log(bot_path, bot_name, text_filter,
                             cu_database.logs.SUB_TYPE_LIBRARY_SEARCH,
                             userid)

    forbidden_tags = cu_tools.get_forbidden_tags_by(current_user.get_id(),
                                                    bot_name)
    filtered_docs = filter_by_forbidden(documents, 'passages', forbidden_tags)
    result_set['passages_response'] = filtered_docs

    return jsonify(result_set)


def filter_by_forbidden(obj_list, sub_list_name, forbidden_tags):
    filtered_objs = []
    for obj in obj_list:
        forbidden = False
        for inner_obj in obj[sub_list_name]:
            forbidden = forbidden or cu_tools.has_forbidden_tag(
                inner_obj.get('tags', []), forbidden_tags)
        if not forbidden:
            filtered_objs.append(obj)
    return filtered_objs


@app.route('/question/<path:bot_name>/<input_id>')
@cu_security.verify_bot
@cu_security.verify_permission(BotConfig.PERM_VIEW_QA)
def get_question(bot_name, input_id):
    userid, bot_path = cu_tools.get_userid_and_bot_path(
        current_user, bot_name)
    if bot_path is None:
        return render_template(
            'error_message.html', page='error', filter={}, error_code=500,
            error_message=_('Not allowed to access Q&A %s') % input_id)

    filter = cu_tools.get_filter(bot_name, request)
    filter['real_position'] = filter['active_page'] * cu_tools.ENTRIES_BY_PAGE
    prev = None
    next = None
    if filter['position'] is not None:
        filter['real_position'] += filter['position'] + 1
        prev, next = cu_database.qa.get_queue_prev_next_ids(
            bot_path,
            filter['position'],
            text_filter=filter['text_filter'],
            since=filter['since'],
            start=filter['active_page'] * cu_tools.ENTRIES_BY_PAGE,
            status=filter['status'],
            descending=(filter['direction'] == '-1'),
            tag=filter['tag'])

    question = cu_database.qa.get_by_id(bot_path, input_id)
    if question:
        # TODO: see question with  multiple text
        question.text = question.text[0]
        question.elapsed = cu_tools.get_elapsed_desc(question.created,
                                                     get_locale())
        question.time_ago = cu_tools.format_timestamp(question.created)
        question.icon = cu_tools.icons[question.get_status()]
        question.desc_status = cu_tools.desc_status[question.get_status()]
        question.color = cu_tools.colors[question.get_status()]
        _load_responses_dicts(question)
        return render_template('question.html', input=question, filter=filter,
                               page='questions', prev=prev, next=next)
    else:
        return render_template(
            'error_message.html', page='error', filter={}, error_code=500,
            error_message=_('Question with id %s not found') % input_id)


@app.route('/modify_qa/<path:bot_name>')
def modify_qa(bot_name):
    input_id = request.args.get('input_id').replace('_', '/')
    new_text = request.args.get('new_text')
    engage = cu_tools.init_engage(bot_name, current_user=current_user)
    engage.modify_question_text(input_id, new_text, current_user.get_id())
    cu_database.user_actions.add_question_action(
        current_user.get_id(), get_bot_config(bot_name).get_bot_path(),
        UserActions.ACTION_TYPE_QUESTION_MODIFICATION,
        input_id=(input_id.replace("_", "/")))
    return redirect('/question/%s/%s' % (bot_name, input_id))


@app.route('/favorite-question/<path:bot_name>', methods=['POST'])
@login_required
@cu_security.verify_bot
def favorite_question(bot_name):
    input_id = request.form.get('id', '').replace('/input/', '')
    favorite_value = request.form.get('favorite', '') == 'true'
    cu_database.qa.set_favorite(input_id, favorite_value)
    return jsonify({})


def _load_responses_dicts(question_object):
    # create dict for responses and verified responses with the key and text
    # based in the information in the Responses objects
    # already loaded in the Input method
    # these dicts are used to display the reponses in the ui
    responses = {}
    verified_responses = {}
    for response in question_object.answers:
        if not response.verified_date:
            responses[response.id] = {
                'id': response.id,
                'text': response.text,
                'tags': storage_tools.get_tags_from_text(response.text),
                'formated': engage_tools.text_to_html(response.text)}
        else:
            verified_responses[response.id] = {
                'id': response.id,
                'text': response.text,
                'tags': storage_tools.get_tags_from_text(response.text),
                'formated': engage_tools.text_to_html(response.text),
                'verifier': response.verifier,
                'verified_elapsed': cu_tools.get_elapsed_desc(
                    response.verified_date, get_locale())}
    question_object.responses_dict = responses
    question_object.verified_responses_dict = verified_responses


def _load_input_responses(textrefs, input_object, engage):
    input_object.textrefs = textrefs
    # create dict for responses and verified responses with the key and text
    # based in the information in textrefs
    # this is
    responses_list = input_object.responses
    verified_responses_list = input_object.verified_responses
    responses = {}
    verified_responses = {}
    for key, value in input_object.textrefs.items():
        engage_updater = EngageUpdater(engage)
        response_data_mongo, error_code = engage_updater.get_object(
            engage_tools.OBJ_TYPE_RESPONSE, key)

        if not isinstance(value, str):
            # WORKAROUND: the input is broken, the textrefs do not have
            # a text as a value
            response_data, ecode = engage.get_object(
                key, engage_tools.OBJ_TYPE_RESPONSE)
            if response_data:
                value = response_data.get('text', '')
        escaped_id = cu_tools.clean_id(key, engage_tools.OBJ_TYPE_RESPONSE)
        if key in verified_responses_list:
            verified_responses[key] = {
                'escaped_id': escaped_id,
                'text': value,
                'formated': engage_tools.text_to_html(value)}
        elif key in responses_list:
            responses[key] = {
                'escaped_id': escaped_id,
                'text': value,
                'formated': engage_tools.text_to_html(value)}
    input_object.responses_dict = responses
    input_object.verified_responses_dict = verified_responses


@app.route('/remember/<path:bot_name>', methods=['POST'])
@login_required
@cu_security.verify_bot
@cu_security.verify_permission(BotConfig.PERM_MODIFY_LIBRARY)
def remember(bot_name):
    engage = cu_tools.init_engage(bot_name, current_user=current_user)
    input_text = request.form.get('input_text', '').strip()
    response_text = request.form.get('response_text', '').strip()
    tags_values = request.form.get('tags_values', '').strip()

    if tags_values:
        # TODO: some cases are not receiving the tags in a separated field
        response_text = cu_tools.organize_tags_from(response_text, tags_values)

    # this parameter is present only when is called from a input view
    input_id = request.form.get('input_id', '')

    # this parameter is present only when is called from the queue
    queue_id = request.form.get('queue_id', None)
    queue_type = request.form.get('queue_type', None)
    if queue_id is not None and queue_id.startswith('#'):
        queue_id = queue_id[1:]  # remove initial char '#'
    query_string = request.form.get('query_string')

    logger.debug('REMEMBER bot %s' % bot_name)
    logger.debug('input_id %s' % input_id)
    logger.debug('input_text %s' % input_text)
    logger.debug('response_text %s' % response_text)
    logger.debug('queue_id %s' % queue_id)
    logger.debug('queue_type %s' % queue_type)
    logger.debug('query_string %s' % query_string)
    logger.debug('tags_values %s' % tags_values)

    # remove auth0 from email
    userid = current_user.get_id()
    if '|' in userid:
        userid = userid.split('|')[1]

    if input_text and response_text:
        bot_config = get_bot_config(bot_name)
        expert, ingestor_contact = get_config_to_notify_user(bot_config)
        new_input_id, new_response_id = engage.remember(
            input_text, response_text, queue_id, queue_type,
            expert=expert, ingestor_contact=ingestor_contact,
            user_id=userid)
        action_type = UserActions.ACTION_TYPE_NEW_QUESTION
        if input_id:
            action_type = UserActions.ACTION_TYPE_RESPONSE_MODIFICATION
        cu_database.user_actions.add_question_action(
            userid, get_bot_config(bot_name).get_bot_path(),
            action_type, input_id=new_input_id['question'],
            response_id=new_input_id['answer'])

    if input_id:
        # notify to users who asked this question
        bot_path = current_user.get_bot_path_by_name(bot_name)
        users_who_asked = cu_database.user_stats.who_asked_question(
            bot_path, input_text)
        notify_users_updated_question(
            users_who_asked, bot_path, input_text, input_id)

        # if the response was created in a input view, go there
        input_id = input_id.replace('/input/', '')
        input_id = input_id.replace('/', '_')

        return redirect('/question/%s/%s?%s' %
                        (bot_name, input_id, query_string.lstrip('?')))

    query_string = request.query_string.decode("utf-8")
    if queue_id:
        return redirect('/issues/%s?%s' % (bot_name, query_string))

    return redirect('/questions/%s?%s' % (bot_name, query_string))


def notify_users_updated_question(users, bot_path, input_text, input_id):
    for userid in users:
        logger.debug('Notifying update question "%s" to user %s' %
                     (input_text, userid))
        cu_database.notifications.add_notification(
            bot_path, userid,
            cu_database.notifications.SUBJECT_UPDATED_ANSWER,
            input_text, input_id=input_id, input_text=input_text)


@app.route('/update_response/<path:bot_name>', methods=['POST'])
@login_required
@cu_security.verify_bot
@cu_security.verify_permission(BotConfig.PERM_MODIFY_QA)
def update_response(bot_name):
    engage = cu_tools.init_engage(bot_name, current_user=current_user)
    input_id = request.form.get('input_id', '')
    response_id = request.form.get('response_id', '')
    input_text = request.form.get('input_text', '').strip()
    response_text = request.form.get('response_text', '').strip()
    tags = request.form.get('tags', '')
    response_text = cu_tools.organize_tags_from(response_text, tags)

    logger.debug('UPDATE RESPONSE bot %s' % bot_name)
    logger.debug('input_id %s' % input_id)
    logger.debug('input_text %s' % input_text)
    logger.debug('response_id %s' % response_id)
    logger.debug('response_text %s' % response_text)
    logger.debug('tags %s' % tags)

    if input_text and response_text:
        bot_config = get_bot_config(bot_name)
        expert, ingestor_contact = get_config_to_notify_user(bot_config)
        engage.update_response(
            input_id, response_id, input_text, response_text,
            current_user.get_id(), True,
            expert=expert, ingestor_contact=ingestor_contact)
        # notify to users who asked this question
        bot_path = current_user.get_bot_path_by_name(bot_name)
        users_who_asked = cu_database.user_stats.who_asked_question(
            bot_path, input_text)
        notify_users_updated_question(
            users_who_asked, bot_path, input_text, input_id)
        cu_database.user_actions.add_question_action(
            current_user.get_id(), get_bot_config(bot_name).get_bot_path(),
            UserActions.ACTION_TYPE_RESPONSE_MODIFICATION,
            input_id=input_id, response_id=response_id)

    return jsonify({})


@app.route('/verify/<path:bot_name>/<input_id>/<response_id>')
@app.route(
    '/verify/<path:bot_name>/<input_id>/<response_id>/<queue_id>/<queue_type>')
@login_required
@cu_security.verify_bot
@cu_security.verify_permission(BotConfig.PERM_VERIFY)
def verify(bot_name, input_id, response_id, queue_id=None, queue_type=None):

    # check parameter to request remove other respopnses
    remove_other_responses = request.args.get(
        'remove_other_responses', '') == 'S'

    engage = cu_tools.init_engage(bot_name, current_user=current_user)

    if queue_id is not None and queue_id.startswith('#'):
        queue_id = queue_id[1:]  # remove initial char '#'

    logger.debug('input_id %s' % input_id)
    logger.debug('response_id %s' % response_id)
    logger.debug('queue_id %s' % queue_id)
    logger.debug('queue_type %s' % queue_type)

    engage.verify(input_id, response_id, queue_id, queue_type,
                  current_user.get_id())

    cu_database.user_actions.add_question_action(
        current_user.get_id(), get_bot_config(bot_name).get_bot_path(),
        UserActions.ACTION_TYPE_RESPONSE_VERIFICATION,
        input_id=input_id, response_id=response_id)

    if remove_other_responses:
        _remove_other_responses(engage, bot_name, input_id, response_id)

    if queue_id:
        query_string = request.query_string.decode("utf-8")
        return redirect('/issues/%s?%s' % (bot_name, query_string))

    # add parameters from the request, to keep the page and position
    request_query = ''
    for arg in request.args.keys():
        if arg != 'remove_other_responses':
            request_query += '%s=%s&' % (arg, request.args[arg])

    return redirect('/question/%s/%s?%s' % (bot_name, input_id, request_query))


def _remove_other_responses(engage, bot_name, input_id, selected_response):
    input_data, ecode = engage.get_object(
        input_id, engage_tools.OBJ_TYPE_INPUT)
    if ecode == 0:
        # remove the ids from the arrays in the input data
        # and remove the response objects
        input = Question(input_data['_id'], bot_name, None,
                         input_data['text'],
                         input_data.get('answers', []))

        other_responses = list(
            filter(lambda x: x != selected_response, input.answers))

        # TODO: now we can remove a response, this will not work
        for response_id in other_responses:
            user_id = current_user.get_id()
            result, ecode = engage.remove_object(
                response_id, engage_tools.OBJ_TYPE_RESPONSE, user_id)


@app.route('/remove_response/<path:bot_name>/<question_id>/<answer_id>')
@login_required
@cu_security.verify_bot
@cu_security.verify_permission(BotConfig.PERM_MODIFY_QA)
def remove_response(bot_name, question_id, answer_id):
    engage = cu_tools.init_engage(bot_name, current_user=current_user)

    cu_database.user_actions.add_question_action(
        current_user.get_id(), get_bot_config(bot_name).get_bot_path(),
        UserActions.ACTION_TYPE_RESPONSE_DELETION,
        input_id=question_id,
        response_id=answer_id)
    logger.debug('BEFORE REMOVE RESPONSE: %s' % answer_id)
    engage.remove_response(answer_id, current_user.get_id())
    query_string = request.query_string.decode("utf-8")
    return redirect('/question/%s/%s?%s' % (
        bot_name, question_id, query_string))


@app.route('/remove_input/<path:bot_name>/<input>')
@login_required
@cu_security.verify_bot
@cu_security.verify_permission(BotConfig.PERM_MODIFY_QA)
def remove_input(bot_name, input):
    input_id = input.replace('_', '/')
    engage = cu_tools.init_engage(bot_name, current_user=current_user)
    # TODO: remove now do not register who removed
    # user_id = current_user.get_id()
    cu_database.user_actions.add_question_action(
        current_user.get_id(), get_bot_config(bot_name).get_bot_path(),
        UserActions.ACTION_TYPE_QUESTION_DELETION,
        input_id=(input.replace("_", "/")))
    engage.remove_question(input_id)
    query_string = request.query_string.decode("utf-8")
    return redirect('/questions/%s?%s' % (bot_name, query_string))


def _init_engage_from_public_bot(current_user, bot_name):
    '''
    If the bot_name is in the list of public_bots
    and the bot_name is not in the list of bots from the user
    init engage using a fake user id
    '''
    is_public_bot = g.bot_config.get_public()
    user_id = current_user.get_id()
    engage = cu_tools.init_engage(bot_name, current_user=current_user)

    return is_public_bot, user_id, engage


def _get_tour_response():
    return {
        'question': 'How do I use Ingestor?',
        'questions': [{
            'question': 'How do I use Ingestor?',
            'input_id': 'tour_id',
            'escaped_input_id': 'tour_id',
            'answers': [{
                'answer': '<p><mark>Ingestor</mark> is designed to return \
              answers as fast -- or faster -- than asking a colleague. \
              When you ask <mark>Ingestor</mark>, your organization gets \
              smarter.</p>',
                'escaped_response_id': 'tour_id',
                'verified': True,
                'verified_by': 'Ingestor',
                'verify_date': '',
                'verify_time': ''
            }],
            'note': {}
        }],
        'total_results': 1,
        'from_tour': True
    }


@app.route('/ask/<path:bot_name>', methods=['GET', 'POST'])
@cu_security.verify_bot
def ask(bot_name):

    return redirect(f'/library/{bot_name}')


@app.route('/ask-page/<path:bot_name>', methods=['GET', 'POST'])
@cu_security.verify_bot
def ask_page(bot_name):

    return redirect(f'/library/{bot_name}')


def add_verify_ago_question(_question):
    for answer in _question['answers']:
        answer['verify_ago'] = cu_tools.format_timestamp(answer.get(
            'verify_date'))
        answer['verify_time'] = cu_tools.get_elapsed_desc(
            answer.get('verify_date'), get_locale())

    if 'note' in _question:
        note = _question['note']
        note['creation_time'] = cu_tools.get_elapsed_desc(
            note.get('last_modify'), get_locale())


def find_favorite_questions(bot_name):
    userid, bot_path = cu_tools.get_userid_and_bot_path(
        current_user, bot_name)
    if bot_path is None:
        return {'list': [], 'amount': 0}

    favorites = cu_database.qa.get_favorites(bot_path)
    favorites = list(map(lambda question: question['text'][0], favorites))
    return {'list': favorites, 'amount': len(favorites)}


@app.route('/store_reaction/<path:bot_name>', methods=['POST'])
@login_required
@cu_security.verify_bot
def store_reaction(bot_name):
    bot_path = current_user.get_bot_path_by_name(bot_name)
    if bot_path is None:
        return jsonify({'error': 'Invalid bot'})
    engage = cu_tools.init_engage(bot_name, current_user=current_user)
    log_id = request.form.get('log_id', '')
    reaction_type = request.form.get('reaction_type', '')
    input_text = request.form.get('input_text', '')

    input_id = None
    escaped_input_id = request.form.get('escaped_input_id', '')
    if escaped_input_id:
        input_id = escaped_input_id.replace('_', '/')

    response_id = None
    escaped_response_id = request.form.get('escaped_response_id', '')
    if escaped_response_id:
        response_id = escaped_response_id.replace('_', '/')

    passage_id = None
    escaped_passage_id = request.form.get('escaped_passage_id', '')
    if escaped_passage_id:
        passage_id = escaped_passage_id.replace('_', '/')

    bot_config = get_bot_config(bot_name)
    expert, ingestor_contact = get_config_to_notify_expert(bot_config)
    response, error = engage.queue_reaction(
        log_id, input_text, reaction_type,
        input_id=input_id, response_id=response_id, passage_id=passage_id,
        expert=expert, ingestor_contact=ingestor_contact,
        url_root=request.url_root)
    logger.debug('store_reaction result: %s' % response)
    if passage_id is not None:
        cu_database.user_actions.add_passage_action(
            current_user.get_id(), bot_path, log_id,
            passage_id,
            thumbs_up=(reaction_type == engage_tools.REACTION_TYPE_SMILE),
            thumbs_down=(reaction_type == engage_tools.REACTION_TYPE_FROWN))
    else:
        cu_database.user_actions.add_search_action(
            current_user.get_id(), bot_path, log_id,
            input_id, response_id,
            thumbs_up=(reaction_type == engage_tools.REACTION_TYPE_SMILE),
            thumbs_down=(reaction_type == engage_tools.REACTION_TYPE_FROWN))

    return jsonify({'error': error})


def update_pin(bot_name, object_type, input_id, is_pinned, log_id):
    bot_path = current_user.get_bot_path_by_name(bot_name)
    if bot_path is None:
        return jsonify({'error': _('Invalid bot')})

    user_id = current_user.get_id()
    if object_type == engage_tools.OBJ_TYPE_INPUT:
        cu_database.pinned_responses.update_qa_pin(
            bot_path, user_id, input_id, is_pinned)
        cu_database.user_actions.add_search_action(
            userid=current_user.get_id(), bot=bot_path, logid=log_id,
            input_id=input_id, response_id=input_id, pinned=is_pinned)

    if object_type == engage_tools.OBJ_TYPE_PASSAGE:
        cu_database.pinned_responses.update_passage_pin(
            bot_path, user_id, input_id, is_pinned)
        cu_database.user_actions.add_passage_action(
            userid=current_user.get_id(), bot=bot_path, logid=log_id,
            passage_id=input_id, pinned=is_pinned)

    return jsonify({'status': 'ok'})


@app.route('/update_question_pin/<path:bot_name>', methods=['POST'])
@login_required
@cu_security.verify_bot
def update_question_pin(bot_name):
    input_id, is_pinned, log_id = init_pin_params()

    return update_pin(bot_name=bot_name,
                      object_type=engage_tools.OBJ_TYPE_INPUT,
                      is_pinned=is_pinned, log_id=log_id, input_id=input_id)


@app.route('/update_passage_pin/<path:bot_name>', methods=['POST'])
@login_required
@cu_security.verify_bot
def update_passage_pin(bot_name):
    input_id, is_pinned, log_id = init_pin_params()
    # passage_id comes escaped
    input_id = input_id.replace('_', '/')
    return update_pin(bot_name=bot_name,
                      object_type=engage_tools.OBJ_TYPE_PASSAGE,
                      input_id=input_id, is_pinned=is_pinned, log_id=log_id)


def init_pin_params():
    is_pinned = request.form.get('is_pinned', '') == 'true'
    log_id = request.form.get('log_id', '')
    input_id = request.form.get('input_id', '')
    return input_id, is_pinned, log_id


@app.route('/store_comment/<path:bot_name>', methods=['POST'])
@cu_security.verify_bot
def store_comment(bot_name):
    userid, bot_path = cu_tools.get_userid_and_bot_path(
        current_user, bot_name)
    if bot_path is None:
        return jsonify({'error': 'Invalid bot'})

    is_public_bot, user_id, engage = _init_engage_from_public_bot(
        current_user, bot_name)
    if not is_public_bot:
        engage = cu_tools.init_engage(bot_name, current_user=current_user)

    log_id = request.form.get('log_id', '')
    comment = request.form.get('comment', '')
    input_text = request.form.get('input_text', '')

    input_id = None
    escaped_input_id = request.form.get('escaped_input_id', '')
    if escaped_input_id:
        input_id = escaped_input_id.replace('_', '/')

    response_id = None
    escaped_response_id = request.form.get('escaped_response_id', '')
    if escaped_response_id:
        response_id = escaped_response_id.replace('_', '/')

    passage_id = None
    escaped_passage_id = request.form.get('escaped_passage_id', '')
    if escaped_passage_id:
        passage_id = escaped_passage_id.replace('_', '/')

    bot_config = get_bot_config(bot_name)
    expert, ingestor_contact = get_config_to_notify_expert(bot_config)
    response, error = engage.queue_comment(
        log_id, input_text, cu_tools.format_comment_text(comment),
        input_id=input_id, response_id=response_id, passage_id=passage_id,
        expert=expert, ingestor_contact=ingestor_contact,
        url_root=request.url_root, user_id=current_user.get_id())
    logger.debug('store_comment result: %s' % response)
    if passage_id is not None:
        cu_database.user_actions.add_passage_action(
            current_user.get_id(), bot_path, log_id,
            passage_id, comment=comment)
    else:
        cu_database.user_actions.add_search_action(
            current_user.get_id(), bot_path, log_id,
            input_id, response_id, comment=comment)
    return jsonify({'error': error})


@app.route('/store_note/<path:bot_name>', methods=['POST'])
@login_required
@cu_security.verify_bot
def store_note(bot_name):
    engage = cu_tools.init_engage(bot_name, current_user=current_user)
    bot_path = current_user.get_bot_path_by_name(bot_name)
    if bot_path is None:
        return jsonify({'error': _('Invalid bot')})

    log_id = request.form.get('log_id', '')
    input_path = request.form.get('input_path', '')
    input_path = input_path.replace('_', '/')
    response_path = request.form.get('response_path', '')
    response_path = response_path.replace('_', '/')
    comment = request.form.get('comment', '')
    user_id = current_user.get_id()

    # notes are saved with a path as /input/ff/fff or /passage/ff/ff
    response, error = engage.record_note(input_path, user_id, comment)
    # verify if the path is from a input or a passage
    if input_path.startswith('/passage/'):
        passage_id = path_to_id_str(input_path, engage_tools.OBJ_TYPE_PASSAGE)
        cu_database.user_actions.add_passage_action(
            current_user.get_id(), bot_path, log_id,
            passage_id, note=comment)
    else:
        input_id = path_to_id_str(input_path, engage_tools.OBJ_TYPE_INPUT)
        response_id = path_to_id_str(response_path,
                                     engage_tools.OBJ_TYPE_RESPONSE)
        cu_database.user_actions.add_search_action(
            userid=current_user.get_id(), bot=bot_path, logid=log_id,
            input_id=input_id, response_id=response_id, note=comment)

    logger.debug('record note result: %s' % response)
    return jsonify({'error': error})


@app.route('/store_question_note/<path:bot_name>', methods=['POST'])
@login_required
@cu_security.verify_bot
def store_question_note(bot_name):
    bot_path = current_user.get_bot_path_by_name(bot_name)
    if bot_path is None:
        return jsonify({'error': _('Invalid bot')})

    log_id = request.form.get('log_id', '')
    question_id = request.form.get('question_id', '')
    answer_id = request.form.get('answer_id', '')
    comment = request.form.get('comment', '')
    user_id = current_user.get_id()

    result, error = cu_database.notes.record_question_note(
        bot_path, question_id, user_id, comment)

    cu_database.user_actions.add_search_action(
        userid=current_user.get_id(), bot=bot_path, logid=log_id,
        input_id=question_id, response_id=answer_id, note=comment)

    logger.debug('record note result: %s' % result)
    return jsonify({'error': error})


@app.route('/delete_note/<path:bot_name>', methods=['POST'])
@login_required
@cu_security.verify_bot
def delete_note(bot_name):
    escaped_id = request.form.get('escaped_id', '')
    note_type = request.form.get('note_type', '')
    input_path = '/%s/%s' % (note_type, escaped_id.replace('_', '/'))
    user_id = current_user.get_id()
    cu_database.notes.delete_note(input_path, user_id)
    return jsonify({'error': 0})


@app.route('/delete_question_note/<path:bot_name>', methods=['POST'])
@login_required
@cu_security.verify_bot
def delete_question_note(bot_name):
    bot_path = current_user.get_bot_path_by_name(bot_name)
    if bot_path is None:
        return jsonify({'error': _('Invalid bot')})
    question_id = request.form.get('question_id', '')
    user_id = current_user.get_id()
    cu_database.notes.delete_question_note(bot_path, question_id, user_id)
    return jsonify({'error': 0})


@app.route('/store_misunderstanding/<path:bot_name>', methods=['POST'])
@login_required
@cu_security.verify_bot
def store_misunderstanding(bot_name):
    engage = cu_tools.init_engage(bot_name, current_user=current_user)
    log_id = request.form.get('log_id', '')
    input_text = request.form.get('input_text', '')
    comment = request.form.get('comment', '')
    bot_config = get_bot_config(bot_name)
    expert, ingestor_contact = get_config_to_notify_expert(bot_config)
    logger.debug('expert %s ingestor_contact %s' % (expert, ingestor_contact))
    response, error = engage.queue_misunderstood(
        log_id, input_text, comment, expert=expert,
        ingestor_contact=ingestor_contact,
        url_root=request.url_root, user_id=current_user.get_id())

    logger.debug('record misunderstanding result: %s' % response)
    return jsonify({'error': error})


@app.route('/get_notifications')
@login_required
def get_notifications():
    user_id = current_user.get_id()
    user_notifications = cu_database.notifications.get_notifications_by_user(
        user_id, pending=False)
    """
    [{'_id': ObjectId('5d1bbb2a9dc6d6604766d725'),
    'recipient': 'godiard@gmail.com',
    'subject': 'New answer!',
    'body': 'can i bring my elephant?',
    'bot': ObjectId('000000000000004200002136'),
    'timestamp': datetime.datetime(2019, 3, 26, 12, 30, 16),
    'input_id': '/input/42/5896'}]
    """

    pending = 0
    notifications_data = []
    for notification in user_notifications:
        notifications_data.append({
            'subject': notification['subject'],
            'body': notification['body'],
            'notification_id': str(notification['_id']),
            'sent': notification.get('sent')
        })
        if not notification.get('sent'):
            pending = pending + 1

    res = {
        'notifications_data': notifications_data,
        'pending': pending
    }

    return jsonify(res)


@app.route('/mark_all_notification/<path:bot_name>')
@login_required
@cu_security.verify_bot
def mark_all_notification(bot_name):
    user_id = current_user.get_id()
    cu_database.notifications.mark_all_as_sent(user_id)
    return jsonify({})


@app.route('/display_notification/<path:notification_id>')
@login_required
def display_notification(notification_id):
    cu_database.notifications.mark_as_sent(notification_id)
    notification = cu_database.notifications.get_notification(
        notification_id)
    bot_path = object_id_to_path(str(notification['bot']))
    notification_bot = current_user.get_bot_name_by_path(bot_path)
    body = notification['body']
    question = notification.get('input_text')
    if not question and notification.get('input_id'):
        # the body could have more information that the input text
        # to be sure, we search the input and display the input text
        engage = cu_tools.init_engage(notification_bot,
                                      current_user=current_user)
        engage_updater = EngageUpdater(engage)
        input_data, error_code = engage_updater.get_object(
            engage_tools.OBJ_TYPE_INPUT, notification['input_id'])
        if not error_code:
            question = input_data['text']
    return redirect('/ask/%s?from_notification=Y&question=%s&comment=%s'
                    % (notification_bot, question, body))


@app.route('/get_ontologies')
def get_ontologies():
    return ontologies_to_json(cu_database.ontology_storage.get_all())


@app.route('/get_ontologies/<path:bot_name>')
def get_bot_ontologies(bot_name):
    bot_config = get_bot_config(bot_name)
    expansion_pipeline = bot_config.get_query_expansion_pipeline()
    ontologies_ids = []

    if expansion_pipeline and 'expansion_steps' in expansion_pipeline:
        ontologies_ids = expansion_pipeline['expansion_steps']
        ontologies_ids = filter(lambda x: x['synonym_database_lookup'],
                                ontologies_ids)
        ontologies_ids = list(
            map(lambda x: x['synonym_ontology_input'], ontologies_ids))

    return ontologies_to_json(
        cu_database.ontology_storage.get_from_ids(ontologies_ids))


def ontologies_to_json(ontologies):
    ontologies_data = []
    for i, ontology in enumerate(ontologies):
        ontology_data = {
            'name': ontology['_id'],
            'description': ontology['description'],
            'terms': [],
            'color': MUNSELL_COLORS[(i * 7) % len(MUNSELL_COLORS)]
        }
        if 'prefered_term_table' in ontology:
            for index in ontology['prefered_term_table'].keys():
                ontology_data['terms'].append({
                    "preferred_term": ontology['prefered_term_table'][index],
                    "synonyms": ontology['synonym_table'][index]
                })
            ontology_data['terms'].sort(key=lambda x: x['preferred_term'])
            ontologies_data.append(ontology_data)
    return jsonify(ontologies_data)


@app.route('/privacy-conditions')
def privacy_conditions():
    return render_template('privacy_conditions.html', filter={})


@app.route('/terms-of-use')
def terms_of_use():
    return render_template('terms-of-use.html', filter={})


@app.route('/change-language/<lang>')
def change_language(lang):
    session["lang"] = lang
    if current_user.is_authenticated:
        user_id = current_user.get_id()
        cu_database.users.update_user(
            user_id, {Users.PREFERED_LANG: lang})

    return render_template('home.html', filter={})


@app.route('/get-languages/')
def get_languages():
    return jsonify(AppConfig.LANG_CONFIG)


REPORT_JSON_CONFIG = '/tmp/report_config.json'


def store_job_config(report_config):
    with open(REPORT_JSON_CONFIG, 'w') as json_file:
        json.dump(report_config, json_file)


def load_job_config():
    report_config = {}
    with open(REPORT_JSON_CONFIG) as json_file:
        report_config = json.load(json_file)
    return report_config


def report_job():
    bots = get_all_bots_config()
    report_config = load_job_config()
    for bot in bots:
        send_report_to(bot.get_bot_name(), bot.get_display_name(),
                       bot.get_send_report_to(),
                       report_config['url_root'])


def send_report_to(bot_name, bot_display_name, email_list, url_root):
    if email_list:
        logger.debug('Sending weekly report for instance %s to %s' % (
            bot_name, email_list))
        report_data = cu_stats.get_report_data(bot_name, 'week')

        extensions = ['jinja2.ext.i18n',
                      'jinja2.ext.autoescape',
                      'jinja2.ext.with_']
        env = Environment(extensions=extensions,
                          loader=PackageLoader('app', 'templates'))

        translations = Translations.load('i18n', AppConfig.LANGUAGES)
        env.install_gettext_translations(translations)
        templ = env.get_template('report-modal.html')

        body = templ.render(
            {'report_data': report_data,
             'url_root': url_root})
        body = render_email_template(body, url_root, 800)
        emails = ','.join(email_list)
        send_email(emails, _("Weekly report for %s") % bot_display_name, body)


@babel.localeselector
def get_locale():
    return cu_tools.get_locale_str()


@babel.timezoneselector
def get_timezone():
    user = getattr(g, 'user', None)
    if user is not None:
        return user.timezone


@app.context_processor
def inject_action_types():
    # permission constants
    return {'action_types': cu_tools.action_types}


@app.context_processor
def inject_permission_constants():
    # permission constants
    return {
        BotConfig.PERM_VIEW_ISSUES: BotConfig.PERM_VIEW_ISSUES,
        BotConfig.PERM_MODIFY_ISSUES: BotConfig.PERM_MODIFY_ISSUES,
        BotConfig.PERM_VIEW_QA: BotConfig.PERM_VIEW_QA,
        BotConfig.PERM_MODIFY_QA: BotConfig.PERM_MODIFY_QA,
        BotConfig.PERM_VIEW_LIBRARY: BotConfig.PERM_VIEW_LIBRARY,
        BotConfig.PERM_MODIFY_LIBRARY: BotConfig.PERM_MODIFY_LIBRARY,
        BotConfig.PERM_VERIFY: BotConfig.PERM_VERIFY,
        BotConfig.PERM_VIEW_REPORT: BotConfig.PERM_VIEW_REPORT,
        BotConfig.PERM_VIEW_DASHBOARD: BotConfig.PERM_VIEW_DASHBOARD,
        BotConfig.PERM_MODIFY_SETTINGS: BotConfig.PERM_MODIFY_SETTINGS,
        BotConfig.PERM_CONTENT_LEVEL_1: BotConfig.PERM_CONTENT_LEVEL_1,
        BotConfig.PERM_CONTENT_LEVEL_2: BotConfig.PERM_CONTENT_LEVEL_2,
        BotConfig.PERM_CONTENT_LEVEL_3: BotConfig.PERM_CONTENT_LEVEL_3,
        BotConfig.PERM_CONTENT_LEVEL_4: BotConfig.PERM_CONTENT_LEVEL_4,
        BotConfig.PERM_OVER_CONTENT_PROTECT:
            BotConfig.PERM_OVER_CONTENT_PROTECT,
        cu_tools.PERM_MODIFY_USER_SETTINGS: cu_tools.PERM_MODIFY_USER_SETTINGS
    }


@app.context_processor
def inject_permission_checker():
    def has_permission(bot_id, permission):
        if bot_id:
            bot_config = get_bot_config(bot_id)
            granted = cu_security.has_permission(bot_config, permission)
            return granted
        return False
    return {"has_permission": has_permission}


@app.context_processor
def inject_current_user():
    return {"current_user": current_user}


@app.context_processor
def utility_bot_config():
    def _get_bot_config(bot_id):
        data = {
                'botAvatarUrl': '/static/images/logo.png',
                'botSecureContent': 'false',
                'botDestinationUrl': ''}
        if bot_id:
            bot_config = get_bot_config(bot_id)
            secure_content = bot_config.get_secure_content() == 'true'
            if secure_content:
                secure_content = not cu_security.has_permission(
                    bot_config, BotConfig.PERM_OVER_CONTENT_PROTECT)
            data = {
                    'botAvatarUrl': bot_config.get_avatar_url(),
                    'botSecureContent': secure_content,
                    'botDestinationUrl':
                    bot_config.get_avatar_destination_url()}
        return data
    return {"get_bot_config": _get_bot_config}


@app.before_request
def get_bot_config_before_request():
    g.current_user = None
    if request.view_args:
        bot_name = request.view_args.get('bot_name')
        if bot_name:
            g.bot_config = get_bot_config_by_name(bot_name, autosave=False)


if __name__ == '__main__':
    app.run(host='0.0.0.0', debug=True)
