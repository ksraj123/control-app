# -*- coding: utf-8 -*-
'''
visualize annotations

'''

# Copyright Walter Bender, Ingestor 2019

# This file is part of Ingestor's KnowBot platform and is copyright
# and a valuable trade secret of Ingestor, Inc.

import logging
import os
import sys
import cu_database
from flask_babel import _
from engage.nl_backend import get_brain_annotations

MIN = 12
# Tag names are from:
# https://www.ling.upenn.edu/courses/Fall_2003/ling001/penn_treebank_pos.html
POS_NAMES = {
    'CC': _('Coordinating conjunction'),
    'CD': _('Cardinal number'),
    'DT': _('Determiner'),
    'EX': _('Existential there'),
    'FW': _('Foreign word'),
    'IN': _('Preposition or subordinating conjunction'),
    'JJ': _('Adjective'),
    'JJR': _('Adjective, comparative'),
    'JJS': _('Adjective, superlative'),
    'LS': _('List item marker'),
    'MD': _('Modal'),
    'NN': _('Noun, singular or mass'),
    'NNS': _('Noun, plural'),
    'NNP': _('Proper noun, singular'),
    'NNPS': _('Proper noun, plural'),
    'PDT': _('Predeterminer'),
    'POS': _('Possessive ending'),
    'PRP': _('Personal pronoun'),
    'PRP$': _('Possessive pronoun'),
    'RB': _('Adverb'),
    'RBR': _('Adverb, comparative'),
    'RBS': _('Adverb, superlative'),
    'RP': _('Particle'),
    'SYM': _('Symbol'),
    'TO': _('to'),
    'UH': _('Interjection'),
    'VB': _('Verb, base form'),
    'VBD': _('Verb, past tense'),
    'VBG': _('Verb, gerund or present participle'),
    'VBN': _('Verb, past participle'),
    'VBP': _('Verb, non-3rd person singular present'),
    'VBZ': _('Verb, 3rd person singular present'),
    'WDT': _('Wh-determiner'),
    'WP': _('Wh-pronoun'),
    'WP$': _('Possessive wh-pronoun'),
    'WRB': _('Wh-adverb')
}

POS_STYLES = {
    'CC': _('unknown'),
    'CD': _('unknown'),
    'DT': _('unknown'),
    'EX': _('unknown'),
    'FW': _('unknown'),
    'IN': _('unknown'),
    'JJ':  _('adjective'),
    'JJR': _('adjective'),
    'JJS': _('adjective'),
    'LS': _('unknown'),
    'MD': _('unknown'),
    'NN':   _('noun'),
    'NNS':  _('noun'),
    'NNP':  _('noun'),
    'NNPS': _('noun'),
    'PDT': _('unknown'),
    'POS': _('unknown'),
    'PRP':  _('pronoun'),
    'PRP$': _('pronoun'),
    'RB':  _('adverb'),
    'RBR': _('adverb'),
    'RBS': _('adverb'),
    'RP':  _('unknown'),
    'SYM': _('unknown'),
    'TO':  _('unknown'),
    'UH':  _('unknown'),
    'VB':  _('verb'),
    'VBD': _('verb'),
    'VBG': _('verb'),
    'VBN': _('verb'),
    'VBP': _('verb'),
    'VBZ': _('verb'),
    'WDT': _('unknown'),
    'WP':  _('unknown'),
    'WP$': _('pronoun'),
    'WRB': _('adverb')
}

# configure logger
logging.basicConfig(level=os.getenv('LOGGER_LEVEL', logging.WARNING))
logger = logging.getLogger('visualize')


def _output_header():
    '''
    Return header including CSS definitions for parts of speech.

    Returns
    -------
    str
    HTML code
    '''

    return """
        <!DOCTYPE html>
        <html>
      <head>
        <meta http-equiv="Content-Type" \
content="text/html; charset=UTF-8">
        <link rel="stylesheet"
        href="https://use.fontawesome.com/releases/v5.7.2/css/all.css"
        integrity="%s" crossorigin="anonymous">
        <style>
    p {
      color:#282828;
      font-size: 24px;
      font-weight:bold;
      font-style:italic;
    }
    mark {
        padding-left: 5px;
        padding-right: 5px;
    }
    mark.verb {
      color:#ffffff;
      background: #ff5942;  # 7.5R 5/
    }
    mark.noun {
      color:#ffffff;
      background: #0096e5;  # 5B 5/
    }
    mark.object {
      color:#ffffff;
      background: #0096e5;  # 5B 5/
    }
    mark.pronoun {
      color:#000000;
      background: #8dceff;  # 5PB 8/
    }
    mark.subject {
      color:#ffffff;
      background: #007dff;  # 5PB 5/
    }
    mark.adjective {
      color:#000000;
      background: #f3c800;  # 5Y 8/
    }
    mark.adverb {
      color:#000000;
      background: #ff97d5;  # 5RP 8/
    }
    mark.unknown {
      color:#000000;
      background: #d3d3d3;
    }
        </style>

        <title>Markup for AI</title>
      </head>""" % \
        "sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pO"
    "pQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr"


def _close_html():
    '''
    Close body and html

    Returns
    -------
    str
    HTML code
    '''

    return ' </body>\n</html>'


def visualize_annotation(bot_name, question, token, interactive=False):
    '''
    Visualize the annotation associated with a question

    Parameters
    ----------
    bot_name : str
    name of the bot we are using

    quesiton : str
    question to parse

    interactive : boolean
    If true, include links to AI tweaker (not yet implemented)

    Returns
    -------
    str
    HTML code
    '''

    input_text = question.replace('+', ' ')
    return visualize(None, input_text, token, interactive, complete=True)


def visualize(engage, input_text, token, interactive=False, complete=False):
    '''
    Visualize the annotation associated with a question

    Parameters
    ----------
    engage : obj
    engage object (currently unused)

    input_text : str
    question to parse

    interactive : boolean
    If true, include links to AI tweaker (not yet implemented)

    complete : boolean
    If true, include header and footer, to create a complete html

    Returns
    -------
    str
    HTML code
    '''

    output = []
    if complete:
        output = [_output_header()]

    output.append('  <body style="font-family: sans-serif">')
    output.append('<p class="visual-p">%s</p>' % (input_text))

    annotations = get_brain_annotations(input_text, token)

    for brain in annotations:

        pos = brain['pos'][0]
        for p in pos[1:]:
            score = min(max(MIN, 10 * 3), 48)
            output.append('<p class="visual-p" style="font-size: %dpx">' %
                          (int(score)))

            if p in POS_STYLES:
                cssclass = POS_STYLES[p]
            else:
                cssclass = 'unknown'

            output.append('<mark class="%s">%s</mark>' %
                          (cssclass, brain['phrase']))

            output.append('<span style="font-size: %dpx"> : ' % MIN)
            if p in POS_NAMES:
                output.append(POS_NAMES[p])
            else:
                output.append(p)
            output.append(' ')

            output.append('</span>')

            if interactive:
                output.append('<i style="font-size: 24px" \
class="far fa-arrow-alt-circle-down"></i>')
                output.append('<i style="font-size: 24px" \
class="far fa-arrow-alt-circle-up"></i>')

            output.append('</p>')

            def _output_list(field, title):
                output.append(
                    '<p class="visual-p" style="font-size: %dpx">' % MIN)
                output.append('<b>%s</b><br>' % title)
                for s in brain[field]:
                    output.append('%s ' % s[0])
                    output.append(': %s<br>' % (s[1]))
                    if interactive:
                        output.append(
                            '<i style="color: #5e8a00" '
                            'class="fas fa-check-circle"></i>'
                            '<i style="color: #fd0039" '
                            'class="fas fa-times-circle"></i></p>')
                output.append('</p>')

            def _output_dict(field, title):
                output.append(
                    '<p class="visual-p" style="font-size: %dpx">' % MIN)
                output.append('<b>%s</b><br>' % title)
                for k in brain[field].keys():
                    if len(brain[field][k]) == 0:
                        continue
                    output.append('%s: ' % k)
                    if isinstance(brain[field][k][0], str):
                        output.append(', '.join(brain[field][k]))
                    else:
                        for p in brain[field][k]:
                            output.append(', '.join(p))
                        output.append(' ')
                    output.append('<br>')
                output.append('</p>')

            if 'synonyms' in brain and len(brain['synonyms']) > 0:
                _output_list('synonyms', 'Synonyms')

            if 'hyponyms' in brain and len(brain['hyponyms'].keys()) > 0:
                _output_dict('hyponyms', 'Hyponyms')

            if 'hypernyms' in brain and len(brain['hypernyms'].keys()) > 0:
                _output_dict('hypernyms', 'Hypernyms')

            if 'meronyms' in brain and len(brain['meronyms'].keys()) > 0:
                _output_dict('meronyms', 'Meronyms')

            if 'holonyms' in brain and len(brain['holonyms'].keys()) > 0:
                _output_dict('holonyms', 'Holonyms')

    if complete:
        output.append(_close_html())
    return ' '.join(output)


if __name__ == '__main__':
    token, errorCode = cu_database.login_manager.get_credentials_by_userid(
        'walter@ingestor.com')
    if len(sys.argv) < 3:
        print('Usage is: visualize bot question, e.g., \
visualize walterbot what+is+your+phone+number? [interactive]')
    elif len(sys.argv) == 3:
        print(visualize_annotation(sys.argv[1], sys.argv[2]), token)
    else:
        print(visualize_annotation(sys.argv[1], sys.argv[2], token,
                                   interactive=True))
