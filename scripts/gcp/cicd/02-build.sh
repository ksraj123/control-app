#!/bin/bash

set -euxo pipefail

export BUILD_ON_DEMAND=0

source $(dirname "$0")/vars

export COMMIT_SHA=$(git rev-parse --short HEAD)

echo "Building on branch: '$BRANCH_NAME'"


set +x 
export GITLAB_TOKEN_NAME="$(echo $INGESTOR_PULL_TOKEN | awk -F ":" '{print $1}')"
export GITLAB_TOKEN_PASS="$(echo $INGESTOR_PULL_TOKEN | awk -F ":" '{print $2}')"
set -x 

gcloud builds submit \
  --config=scripts/gcp/cicd/cloudbuild.yaml \
  --substitutions=_GITLAB_TOKEN_NAME=${GITLAB_TOKEN_NAME},_GITLAB_TOKEN_PASS=${GITLAB_TOKEN_PASS},_COMMIT_SHA=${COMMIT_SHA},_GCR_PROJECT_ID=${GCR_PROJECT_ID} \
  --timeout=3h

