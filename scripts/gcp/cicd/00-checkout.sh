#!/bin/bash

set -euxo pipefail

export BUILD_ON_DEMAND=0

source $(dirname "$0")/vars

if [ -z ${COMMIT_SHA+x} ] || [[ "$COMMIT_SHA" == "HEAD" ]]; then
    export COMMIT_SHA="$(git rev-parse --short HEAD)"
    echo "COMMIT_SHA is unset fallback, to $COMMIT_SHA"
elif [[ "$COMMIT_SHA" == "$(git rev-parse --short HEAD)" ]]; then
    # the user has provided a custom image hash
    # which we will need to deploy
    export BUILD_ON_DEMAND=1
    echo "Building $COMMIT_SHA on demand"
    git checkout "$COMMIT_SHA"
    export COMMIT_SHA="$(git rev-parse --short $COMMIT_SHA)"
fi


