#!/bin/bash
set -euxo pipefail

source $(dirname "$0")/vars

# checks if all the containers are present in the google container registry
gcloud container images list-tags \
  "$GCR_PREFIX/$GCR_PROJECT_ID/$DOCKER_CONTAINER_NAME" | grep "$(git rev-parse --short HEAD)"
