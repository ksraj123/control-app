Continuous Integration
======================

Pre-requisities 
---------------
* Must have `terraform`, `gcloud`
* Must have a google cloud project, see http://go.ingestor.ai/devops-checklist for the DevOps Checklist

Deploy
------
To deploy this service yourself, run all the scripts. 

```bash
./scripts/gcp/cicd/*.sh
```

Terraform
---------
This service uses terraform. See [terraform](./terraform) directory for more information.
