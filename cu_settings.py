# -*- coding: utf-8 -*-
'''
Settings logic for the Ingestor cu app
'''

# Copyright Gonzalo Odiard, Ingestor 2019

# This file is part of Ingestor's Knowbot platform and is copyright
# and a valuable trade secret of Ingestor, Inc.

import logging
import os
import cu_database

from PIL import ImageOps, Image
from flask import jsonify, request, render_template
from flask import Blueprint
from cu_authentication import current_user
from cu_security import login_required

from engage.botconfig import get_bot_config_by_name
from engage.botconfig import BotConfig
from engage import engage_tools
from engage import email_tools
from ingestor_storage import Users
from ingestor_storage.actions import UserActions
from flask import redirect
from flask_babel import _
from cloudservices.aws.files import FileManager, generate_unique_filename
from cu_authentication import CLIENT_ID
import cu_tools
from cu_tools import get_bot_config
import cu_security
import cu_tooltip

IMAGE_UPLOAD_FOLDER = '/tmp'

# configure logger
logging.basicConfig(level=os.getenv('LOGGER_LEVEL', logging.WARNING))
logger = logging.getLogger('settings')


settings_blueprint = Blueprint('settings', __name__,
                               template_folder='templates')


@settings_blueprint.route('/settings/<path:bot_name>')
@login_required
@cu_security.verify_permission(BotConfig.PERM_MODIFY_SETTINGS)
def settings(bot_name):
    return _settings(bot_name)


_permissions = cu_tools.permissions


def _settings(bot_name, tab='', successMsg='', errorMsg=''):
    currentTab = tab if tab else request.args.get('tab', 'users')
    bot_config = get_bot_config_by_name(bot_name)
    logger.debug('settings bot %s %s' % (bot_name, bot_config))
    filter = cu_tools.get_filter(bot_name, request)
    if not bot_config.get_roles():
        bot_config.set_roles(BotConfig.default_roles)
    is_ingestor_user = cu_tools.is_ingestor_user(current_user.get_id())
    experts = parse_experts(bot_config.get_sme_help())
    send_report_to = parse_emails(bot_config.get_send_report_to())
    bot_path = current_user.get_bot_path_by_name(bot_name)
    users = cu_database.users.get_users_by_bot(bot_path)
    users_data = parse_users(bot_config, users)
    tooltip_data = cu_tooltip.get_tooltips()
    filter_tags = parse_tags(bot_config.get_filter_tags())
    permissions = cu_tools.permissions
    homepage_translations = cu_tools.homepage_translations

    return render_template('settings.html', filter=filter, page='settings',
                           experts=experts, send_report_emails=send_report_to,
                           users=users_data, is_ingestor_user=is_ingestor_user,
                           tooltip_data=tooltip_data, filter_tags=filter_tags,
                           config=bot_config, successMsg=successMsg,
                           errorMsg=errorMsg, tab=currentTab,
                           homepage_translations=homepage_translations,
                           permissions=permissions)


def parse_users(botconfig, users_data):
    bot_name = botconfig._bot_name
    users_data.sort(key=lambda user: user.get(Users.FULL_NAME, '').lower())
    users_list = []
    for i, user in enumerate(users_data):
        user_data = parse_user(user)
        user_data['index'] = i
        roles = user.get('app_metadata').get('bots', {}).get(
            bot_name).get('roles')
        if roles:
            logger.debug('roles: %s' % roles)
            # remove empty roles
            roles = [role for role in roles if role]
            user_data['role'] = ','.join(roles)
        else:
            logger.debug('user %s without role' % user)
        users_list.append(user_data)

    return {'records': users_list, 'total': len(users_list)}


def parse_user(user):
    user_data = {
        Users.USERID: user.get(Users.USERID, ''),
        Users.FIRST_NAME: user.get(Users.FIRST_NAME, ''),
        Users.LAST_NAME: user.get(Users.LAST_NAME, ''),
        Users.FULL_NAME: user.get(Users.FULL_NAME, ''),
        Users.PHONE_NUMBER: user.get(Users.PHONE_NUMBER, ''),
        Users.EMAIL: user.get(Users.EMAIL, user.get(Users.USERID, '')),
        Users.TITLE: user.get(Users.TITLE, ''),
        Users.LOGINCODE: user.get(Users.LOGINCODE, ''),
        Users.PASSHASH: user.get(Users.PASSHASH, ''),
        'authzero_id': user.get('user_id')
    }
    # This is because javascript don't get True/False as
    # boolean for the capitalized
    user_data['from_bot_list'] = 0 if user_data[Users.USERID] else 1
    return user_data


def parse_experts(sme_help):
    return [] if not sme_help else parse_emails(sme_help.split(","))


def parse_emails(email_list):
    return parse_list_obj_with_index(email_list, 'email')


def parse_tags(tag_list):
    return parse_list_obj_with_index(tag_list, 'tag')


def parse_list_obj_with_index(_list, property_val):
    return [{'index': i, property_val: e.strip()} for i, e in enumerate(_list)]


@settings_blueprint.route('/upload-bot-avatar/<path:bot_name>',
                          methods=['POST'])
@login_required
@cu_security.log_activity(
    cu_security.log_admin_activity,
    action_type=UserActions.ACTION_TYPE_BOT_AVATAR_UPLOADED)
def upload_bot_avatar(bot_name):
    files = request.files.getlist("avatarInput")
    bot_config = get_bot_config_by_name(bot_name)
    if files and files[0].filename:
        bot_config.set_avatar_url(save_and_get_image_url(files[0]))
    else:
        bot_config.set_avatar_url('')
    return redirect('/settings/%s' % bot_name)


@settings_blueprint.route('/upload-favicon/<path:bot_name>',
                          methods=['POST'])
@login_required
@cu_security.log_activity(
    cu_security.log_admin_activity,
    action_type=UserActions.ACTION_TYPE_BOT_AVATAR_UPLOADED)
def upload_favicon(bot_name):
    files = request.files.getlist("faviconInput")
    bot_config = get_bot_config_by_name(bot_name)
    if files and files[0].filename:
        bot_config.set_favicon_url(save_and_get_image_url(files[0]))
    else:
        bot_config.set_favicon_url('')
    return redirect('/settings/%s' % bot_name)


@settings_blueprint.route('/upload-bot-media/<path:bot_name>',
                          methods=['POST'])
@login_required
@cu_security.log_activity(
    cu_security.log_admin_activity,
    action_type=UserActions.ACTION_TYPE_BOT_AVATAR_UPLOADED)
def upload_bot_media(bot_name):
    files = request.files.getlist("mediaInput")
    bot_config = get_bot_config_by_name(bot_name)
    if files and files[0].filename:
        bot_config.set_media_url(save_and_get_image_url(files[0]))
    else:
        bot_config.set_media_url('')
    return redirect('/settings/%s' % bot_name)


@settings_blueprint.route('/save-security-settings/<path:bot_name>',
                          methods=['POST'])
@login_required
@cu_security.verify_permission(BotConfig.PERM_MODIFY_SETTINGS)
@cu_security.log_activity(
    cu_security.log_admin_activity,
    action_type=UserActions.ACTION_TYPE_SECURITY_SETTINGS_MODIFICATION)
def save_security_settings(bot_name):
    successMsg = _('Your changes have been saved.')
    errorMsg = ''
    try:
        bot_config = get_bot_config_by_name(bot_name, autosave=False)
        logger.debug('SAVE SETTINGS bot %s %s' % (bot_name, bot_config))

        security_fields = {
            BotConfig.ENABLE_2FA: request.form.get('enable_2fa', False),
            BotConfig.ENABLE_2FA_EMAIL: request.form.get(
                'enable_2fa_email', ''),
        }

        bot_config.save_fields(security_fields)

    except Exception:
        errorMsg = _('An error has occurred')
        successMsg = ''
    return _settings(bot_name, 'security', successMsg, errorMsg)


@settings_blueprint.route('/save-notifications-settings/<path:bot_name>',
                          methods=['POST'])
@cu_security.log_activity(
    cu_security.log_admin_activity,
    action_type=UserActions.ACTION_TYPE_NOTIFICATIONS_SETTINGS_MODIFICATION)
def save_notifications_settings(bot_name):
    successMsg = _('Your changes have been saved.')
    errorMsg = ''
    try:
        bot_config = get_bot_config_by_name(bot_name, autosave=False)
        logger.debug('SAVE SETTINGS bot %s %s' % (bot_name, bot_config))

        send_user = request.form.get('send_user_notifications', True)
        send_expert = request.form.get('send_expert_notifications', True)

        send_report_to = request.form.get('send_report_to_val', '')
        send_report_to = send_report_to.split(",") if send_report_to else []

        notification_fields = {
            BotConfig.SME_HELP: request.form.get('sme_help_val', ''),
            BotConfig.SEND_USER_NOTIFICATIONS:
                'true' if not send_user else 'false',
            BotConfig.SEND_EXPERT_NOTIFICATIONS:
                'true' if not send_expert else 'false,',
            BotConfig.SEND_REPORT_TO: send_report_to
        }

        bot_config.save_fields(notification_fields)

    except Exception:
        errorMsg = _('An error has occurred')
        successMsg = ''
    return _settings(bot_name, 'notification', successMsg, errorMsg)


@settings_blueprint.route('/save-instance-settings/<path:bot_name>',
                          methods=['POST'])
@login_required
@cu_security.verify_permission(BotConfig.PERM_MODIFY_SETTINGS)
@cu_security.log_activity(
    cu_security.log_admin_activity,
    action_type=UserActions.ACTION_TYPE_INSTANCE_SETTINGS_MODIFICATION)
def save_instance_settings(bot_name):
    successMsg = _('Your changes have been saved.')
    errorMsg = ''
    try:
        bot_config = get_bot_config_by_name(bot_name, autosave=False)
        logger.debug('SAVE SETTINGS bot %s %s' % (bot_name, bot_config))
        bot_config.set_ask_preamble(request.form.get('ask_preamble', ''))
        bot_config.set_disclaimer(request.form.get('disclaimer', ''))
        bot_config.set_no_answer(request.form.get('no_answer', ''))
        bot_config.set_comment_msg(request.form.get('comment_prompt', ''))
        bot_config.set_comment_confirmation(
            request.form.get('comment_confirmation', ''))
        bot_config.set_notes_msg(request.form.get('notes_prompt', ''))
        bot_config.set_meh_msg(request.form.get('meh_msg', ''))
        bot_config.set_note_confirmation(
            request.form.get('notes_confirmation', ''))
        bot_config.set_require_verification(
            request.form.get('require_verification', ''))
        bot_config.set_display_name(
            request.form.get('display_name', ''))

        bot_config.save_instance()

    except Exception:
        errorMsg = _('An error has occurred')
        successMsg = ''

    return _settings(bot_name, 'instance', successMsg, errorMsg)


@settings_blueprint.route('/save-social-settings/<path:bot_name>',
                          methods=['POST'])
@login_required
@cu_security.verify_permission(BotConfig.PERM_MODIFY_SETTINGS)
@cu_security.log_activity(
    cu_security.log_admin_activity,
    action_type=UserActions.ACTION_TYPE_SOCIAL_SETTINGS_MODIFICATION)
def save_social_settings(bot_name):
    successMsg = _('Your changes have been saved.')
    errorMsg = ''
    try:
        bot_config = get_bot_config_by_name(bot_name, autosave=False)
        logger.debug('SAVE SETTINGS bot %s %s' % (bot_name, bot_config))

        # EMAIL
        bot_config.set_include_sme_contact(
            request.form.get('include_sme_contact', 'false'))
        bot_config.set_include_ingestor_contact(
            request.form.get('include_ingestor_contact', 'false'))

        # SLACK
        bot_config.set_welcome_msg(request.form.get('welcome_msg', ''))
        bot_config.set_help_list(request.form.get('help_list', ''))
        bot_config.set_share_text(request.form.get('share_text', ''))
        bot_config.set_share_msg(request.form.get('share_msg', ''))

        # # SMS
        bot_config.set_sms_help_msg(request.form.get('sms_help_msg', ''))
        bot_config.set_sms_switch_msg(request.form.get('sms_switch_msg', ''))
        bot_config.set_first_prompt(request.form.get('first_prompt', ''))

        bot_config.save_social()

    except Exception:
        errorMsg = _('An error has occurred')
        successMsg = ''

    return _settings(bot_name, 'social', successMsg, errorMsg)


@settings_blueprint.route('/save-ingestor-settings/<path:bot_name>',
                          methods=['POST'])
@login_required
@cu_security.verify_permission(BotConfig.PERM_MODIFY_SETTINGS)
@cu_security.log_activity(
    cu_security.log_admin_activity,
    action_type=UserActions.ACTION_TYPE_INGESTOR_SETTINGS_MODIFICATION)
def save_ingestor_settings(bot_name):
    successMsg = _('Your changes have been saved.')
    errorMsg = ''
    try:
        bot_config = get_bot_config_by_name(bot_name, autosave=False)
        logger.debug('SAVE INGESTOR SETTINGS bot %s %s' % (
            bot_name, bot_config))

        email_alias = request.form.get('email_alias', '')

        if email_alias != bot_config.get_email_alias():
            if email_alias in cu_database.storage.get_all_bots_email_alias():
                errorMsg = _('The email alias is already used')
                successMsg = ''
            else:
                bot_config.set_email_alias(email_alias)

        bot_config.set_ingestor_help(request.form.get('ingestor_help', ''))
        bot_config.set_public(request.form.get('public', ''))

        bot_config.set_question_search(
            request.form.get('question_search', 'false'))
        bot_config.set_answer_search(
            request.form.get('answer_search', 'false'))
        bot_config.set_library_search(
            request.form.get('library_search', 'false'))
        bot_config.set_help_action(request.form.get('help_action', ''))
        bot_config.set_avatar_destination_url(request.form.get(
                'avatar_destination_url', ''))
        bot_config.set_secure_content(request.form.get(
            'secure_content', 'false'))
        # security
        bot_config.set_password_expiration_days(
            request.form.get('password_expiration_days', 0))
        bot_config.set_number_invalid_attempts(
            request.form.get('number_invalid_attempts', 0))
        bot_config.set_ip_filter_mask(
            request.form.get('ip_filter_mask', ''))

        filter_tags = request.form.get('filter_tags_val', '')
        filter_tags = filter_tags.split(",") if filter_tags else []
        bot_config.set_filter_tags(filter_tags)

        bot_config.save_ingestor()
        # update bots info
        current_user.reset_bots_data()
    except Exception:
        errorMsg = _('An error has occurred')
        successMsg = ''

    return _settings(bot_name, 'ingestor', successMsg, errorMsg)


@settings_blueprint.route('/users/<path:bot_name>')
@login_required
@cu_security.verify_permission(BotConfig.PERM_MODIFY_SETTINGS)
def users(bot_name):
    bot_config = get_bot_config(bot_name)
    # create a single collection with the users and roles,
    # taking the data from users, experts and admin collections.
    # if experts or admins not in users, add them
    page = int(request.args.get('page', 1))
    limit = int(request.args.get('limit', 0))

    all_users = []
    # create a set to store unique ids
    all_user_ids = set()
    for user in bot_config.get_users():
        if isinstance(user, str):
            all_users.append({'id': user, 'role': BotConfig.USER_ROLE_USER})
        else:
            logger.error('user id is not a str: %s' % user)

    for expert in bot_config.get_experts():
        if isinstance(expert, str):
            found = False
            for user in all_users:
                all_user_ids.add(user['id'])
                if user['id'] == expert:
                    user['role'] = BotConfig.USER_ROLE_EXPERT
                    found = True
            if not found:
                all_users.append({'id': expert,
                                  'role': BotConfig.USER_ROLE_EXPERT})
        else:
            logger.error('user id is not a str: %s' % expert)

    for admin in bot_config.get_administrators():
        if isinstance(admin, str):
            found = False
            for user in all_users:
                all_user_ids.add(user['id'])
                if user['id'] == admin:
                    user['role'] = BotConfig.USER_ROLE_ADMIN
                    found = True
            if not found:
                all_users.append({'id': admin,
                                  'role': BotConfig.USER_ROLE_ADMIN})
        else:
            logger.error('user id is not a str: %s' % admin)

    # verify if there are ids (from experts or admins)
    # not stored as users
    non_saved_users = all_user_ids.difference(bot_config.get_users())
    if (non_saved_users):
        logger.error(
            'WARNING: FOUND USERS AS ADMIN OR EXPERT, BUT NOT USERS ADD %s' %
            non_saved_users)
        new_users = ','.join(non_saved_users)
        bot_config.add_users_by_role(
            BotConfig.USER_ROLE_USER, new_users)

    sorted_users = sorted(all_users, key=lambda k: k['id'])
    if limit != 0:
        min_data = (page - 1) * limit
        max_data = page * limit
        data = {'records': sorted_users[min_data:max_data],
                'total': len(sorted_users)}
    else:
        data = {'records': sorted_users,
                'total': len(sorted_users)}

    return jsonify(data)


@settings_blueprint.route('/users_delete/<path:bot_name>', methods=['POST'])
@login_required
@cu_security.verify_permission(BotConfig.PERM_MODIFY_SETTINGS)
@cu_security.log_activity(
    cu_security.log_admin_activity,
    action_type=UserActions.ACTION_TYPE_USER_DELETION,
    description_handler=lambda req: req.form.get('id', ''))
def users_delete(bot_name):
    bot_config = get_bot_config_by_name(bot_name)
    user_id = request.form.get('id', '')

    # drop user by email or telephone in any of the categories
    bot_config.drop_user(user_id)

    # delete the user from any custom roles
    custom_roles = bot_config.get_roles()
    for custom_role_name, custom_role in custom_roles.items():
        if user_id in custom_role.get('users'):
            custom_role.get('users').remove(user_id)
    _save_roles(bot_config, custom_roles)

    # reset user logged bits information, other logged users need logout
    # to see the change
    current_user.reset_bots_data()

    # notify users
    url_root = request.url_root
    subject = _('User has been removed from %s instance of '
                'Ingestor') % bot_config.get_display_name()
    title = _('User deleted')
    text = _('The user %s has been deleted from the %s instance of \
    Ingestor.') % (user_id, bot_config.get_display_name())
    cu_tools.send_message(user_id, subject, title, text, url_root)

    response = jsonify('ok')
    response.status_code = 200
    response.error = 0
    return response


@settings_blueprint.route('/users_save/<path:bot_name>', methods=['POST'])
@login_required
@cu_security.verify_permission(BotConfig.PERM_MODIFY_SETTINGS)
@cu_security.log_activity(
    cu_security.log_admin_activity,
    action_type=UserActions.ACTION_TYPE_USER_MODIFICATION,
    description_handler=lambda req: req.form.get('record[id]', ''))
def users_save(bot_name):
    bot_config = get_bot_config(bot_name)
    # data comes in this strange way...
    userid = request.form.get('record[id]', '')
    role = request.form.get('record[role]', '')

    # verify user by email in any of the categories
    bot_config.update_user_role(userid, role, request.url_root)

    # reset user logged bits information, other logged users need logout
    # to see the change
    current_user.reset_bots_data()

    response = jsonify('ok')
    response.status_code = 200
    return response


def save_user(bot_config, userid, user_data, role, new_user):
    '''
    Save on Users and on the BotConfig

    bot_config: BotConfig

    userid: str
    email or empty for new users

    user_data: dict
    data to create or update the user
    if is needed update, only new/changed data is needed

    role: str
    the user role

    new_user: boolean
    If is a new user for the bot, we need send the welcome email
    '''
    bot_config.save_user(userid, user_data, role, new_user, request.url_root)


def merge_users(existing_user, new_user_data):
    logger.info(f'existing user {existing_user}')
    logger.info(f'new_user_data  {new_user_data}')
    properties = [
        Users.TITLE, Users.FIRST_NAME, Users.LAST_NAME,
        Users.FULL_NAME, Users.PHONE_NUMBER, Users.EMAIL
    ]
    result_dic = existing_user
    for pro in properties:
        if pro not in existing_user:
            result_dic[pro] = new_user_data[pro]

    logger.info(f'result dict of merge_users {result_dic}')
    return result_dic


def user_edition_description(request):
    if request.form.get('userid', '') != '':
        return request.form.get('userid', '')
    else:
        return request.form.get('email', '')


def is_user_creation():
    if request.form.get('userid', '') == '':
        return UserActions.ACTION_TYPE_NEW_USER
    else:
        return UserActions.ACTION_TYPE_USER_MODIFICATION


@settings_blueprint.route('/add_user/<path:bot_name>', methods=['POST'])
@login_required
@cu_security.verify_permission(BotConfig.PERM_MODIFY_SETTINGS)
@cu_security.log_activity(cu_security.log_admin_activity,
                          action_type=is_user_creation,
                          description_handler=user_edition_description)
def add_user(bot_name):
    bot_config = get_bot_config_by_name(bot_name, autosave=True)
    form = request.form
    user_data = {
        Users.TITLE: form.get('title', ''),
        Users.FIRST_NAME: form.get('first-name', ''),
        Users.LAST_NAME: form.get('last-name', ''),
        Users.FULL_NAME:
            form.get('first-name', '') + ' ' + form.get('last-name', ''),
        Users.PHONE_NUMBER: form.get('phone-number', ''),
        Users.EMAIL: form.get('email', '').lower(),
        'from_bot_list': int(form.get('from_bot_list', 0)),
        'client_id': CLIENT_ID,
        'user_id': form.get('authzero_id')
    }

    response = {}
    response['status_code'] = 200
    response['error'] = 0

    userid = form.get('userid', '')
    role = form.get('role', '')
    is_user_edition = userid != ''

    if is_user_edition:
        print(user_data)
        bot_config.save_user(userid, user_data, role, False, request.url_root)
    else:
        userid = user_data[Users.EMAIL]

        if userid not in bot_config.get_users():
            existing_user = cu_database.users.get_user(userid)
            if not existing_user:
                bot_config.save_user('', user_data, role, True,
                                     request.url_root)
            else:
                bot_config.add_users_by_role(role, userid)
                bot_config.save_user(
                    userid, merge_users(existing_user, user_data),
                    role, True, request.url_root)
        else:
            response['status_code'] = 500
            response['error'] = 1
            response['message'] = (_('<strong>%s</strong> already '
                                     'exists on this instance.') % (userid))

    return jsonify(response)


@settings_blueprint.route('/users_add/<path:bot_name>', methods=['POST'])
@login_required
@cu_security.verify_permission(BotConfig.PERM_MODIFY_SETTINGS)
def users_add(bot_name):
    bot_config = get_bot_config(bot_name)
    new_users = request.form.get('id', '')
    role = request.form.get('role', '')
    logger.debug('ADD USERS bot %s %s %s' % (bot_name, role, new_users))
    # verify telephone number format
    bad_ids = ''
    count_error = 0
    for userid in cu_tools.get_list_of_userid(new_users):
        formated, error = engage_tools.format_telephone_number(userid)
        if error == -2:
            bad_ids = bad_ids + userid + ','
            count_error = count_error + 1
    if bad_ids.strip():
        # remove the last comma
        bad_ids = bad_ids[:len(bad_ids) - 1]
        msg = _('Invalid telephone number')
        if count_error > 1:
            msg = _('Invalid telephone numbers')

        response = jsonify({'error': '%s: %s' % (msg, bad_ids)})
        response.status_code = 500
        return response

    error, existing_users, new_users_list = bot_config.add_users_by_role(
        role, new_users)

    # notify users
    for userid in new_users_list:
        email_tools.send_welcome_email(
            userid, bot_config.get_display_name(), request.url_root)

    # reset user logged bits information, other logged users need logout
    # to see the change
    current_user.reset_bots_data()

    response = {}
    response['status_code'] = 200
    response['error'] = error
    response['existing_users'] = existing_users
    return jsonify(response)


@settings_blueprint.route('/user-settings/<path:bot_name>')
@login_required
@cu_security.verify_permission(cu_tools.PERM_MODIFY_USER_SETTINGS)
def users_settings(bot_name):
    user = cu_database.users.get_user(current_user.get_id())
    tooltip_data = cu_tooltip.get_tooltips()
    userId = current_user.get_id()
    if '@' in userId:
        user['email'] = userId
        user['id-type'] = 'email'
    else:
        user['phone_number'] = userId
        user['id-type'] = 'phone'
    return render_template('user-settings.html', filter={}, user=user,
                           tooltip_data=tooltip_data)


@settings_blueprint.route('/save-user-identity-settings', methods=['POST'])
@login_required
@cu_security.log_activity(
    cu_security.log_admin_activity,
    action_type=UserActions.ACTION_TYPE_USER_IDENTITY_SETTINGS_MODIFICATION)
def save_user_identity_settings():
    status = 'success'
    try:
        form = request.form
        data = {
            Users.DISPLAY_NAME: form.get('display_name', ''),
            Users.FULL_NAME: form.get('full_name', ''),
            Users.EMAIL: form.get('email', ''),
            Users.PHONE_NUMBER: form.get('phone_number', ''),
            Users.NOTIFICATION_FREQUENCY:
                form.get('notification_frequency', ''),
            Users.NOTIFICATION: form.get('notification', '')
        }
        cu_database.users.update_user(current_user.get_id(), data)
    except Exception:
        status = 'error'
    return redirect('/user-settings?status=%s' % status)


@settings_blueprint.route('/add_role/<path:bot_name>/<path:role_name>',
                          methods=['GET'])
@login_required
@cu_security.verify_permission(BotConfig.PERM_MODIFY_SETTINGS)
@cu_security.log_activity(cu_security.log_admin_activity,
                          action_type=UserActions.ACTION_TYPE_NEW_ROLE,
                          description_handler=lambda req:
                          req.view_args['role_name'])
def add_role(bot_name, role_name):
    bot_config = get_bot_config(bot_name)
    roles = bot_config.get_roles()
    roles[role_name.lower()] = {
                        'users': [], 'permissions': [], 'forbidden_tags': []}
    return _save_roles(bot_config, roles)


@settings_blueprint.route('/get_roles/<path:bot_name>/', methods=['GET'])
@login_required
@cu_security.verify_permission(BotConfig.PERM_MODIFY_SETTINGS)
def get_roles(bot_name):
    bot_config = get_bot_config(bot_name)
    roles = bot_config.get_roles().keys()
    return jsonify([*roles])


@settings_blueprint.route('/save_roles/<path:bot_name>', methods=['POST'])
@login_required
@cu_security.verify_permission(BotConfig.PERM_MODIFY_SETTINGS)
@cu_security.log_activity(
    cu_security.log_admin_activity,
    action_type=UserActions.ACTION_TYPE_ROLE_MODIFICATION)
def save_roles(bot_name):
    bot_config = get_bot_config(bot_name)
    roles = bot_config.get_roles()
    updated_roles = request.get_json(force='true')
    roles_without_permissions = []
    res = {}
    default_homepage = {
      'name': 'Default',
      'url': '',
      'permission': ''
    }
    res['error_message'] = None
    for role_name, role in roles.items():
        role['permissions'] = updated_roles.get(role_name)['permissions']
        role['forbidden_tags'] = updated_roles.get(role_name)['forbidden_tags']
        if 'homepage' in updated_roles.get(role_name):
            role['homepage'] = updated_roles.get(role_name)['homepage']

            if role['homepage'] is None:
                role['homepage'] = default_homepage
            elif role['homepage']['permission'] not in role['permissions'] \
                    and role['homepage']['name'] not in ['Ask', 'Default']:
                roles_without_permissions.append(role_name.upper())

    if roles_without_permissions:
        res['error_message'] = _('The following role/s do not have '
                                 'permission for the selected '
                                 'home page: %s') % (', '.join(
                                     roles_without_permissions))

        response = jsonify(res)
        response.status_code = 404
        return response

    return _save_roles(bot_config, roles)


def _save_roles(bot_config, roles):
    dict = {
        'roles': roles
    }
    res = {}
    res['status_code'] = 200
    res['error_message'] = None
    try:
        bot_config.save_fields(dict)
    except Exception:
        res['error_message'] = _('Selected roles were not saved')
    finally:
        if res['error_message']:
            response = jsonify(res)
            response.status_code = 404
            return response

        return jsonify(res)


@settings_blueprint.route('/delete_role/<path:bot_name>/<path:role_name>',
                          methods=['GET'])
@login_required
@cu_security.verify_permission(BotConfig.PERM_MODIFY_SETTINGS)
@cu_security.log_activity(cu_security.log_admin_activity,
                          action_type=UserActions.ACTION_TYPE_ROLE_DELETION,
                          description_handler=lambda req:
                          req.view_args['role_name'])
def delete_role(bot_name, role_name):
    bot_config = get_bot_config(bot_name)
    roles = bot_config.get_roles()
    del roles[role_name]
    return _save_roles(bot_config, roles)


@settings_blueprint.route('/show_roles/<path:bot_name>', methods=['GET'])
@login_required
@cu_security.verify_permission(BotConfig.PERM_MODIFY_SETTINGS)
def show_roles(bot_name):
    bot_config = get_bot_config(bot_name)
    permissions = cu_tools.permissions
    homepage_translations = cu_tools.homepage_translations
    return render_template('bot-settings-roles-permissions-table.html',
                           config=bot_config, permissions=permissions,
                           homepage_translations=homepage_translations)


@settings_blueprint.route('/upload-avatar', methods=['POST'])
@login_required
@cu_security.log_activity(cu_security.log_admin_activity,
                          action_type=UserActions.ACTION_TYPE_AVATAR_UPLOADED)
def upload_avatar():
    files = request.files.getlist("avatarInput")
    if files and files[0].filename:
        file = files[0]
        url = save_and_get_image_url(file)
        cu_database.users.update_user(current_user.get_id(),
                                      {Users.AVATAR_URL: url})
    else:
        cu_database.users.update_user(current_user.get_id(),
                                      {Users.AVATAR_URL: ''})
    return redirect('/user-settings')


def save_and_get_image_url(image_file):
    file_manager = FileManager('cert/aws-access.json')
    filename = os.path.join(
        IMAGE_UPLOAD_FOLDER, generate_unique_filename(image_file.filename))
    image_file.save(filename)
    # rotate image if needed
    image_transpose(filename)
    url, error, stored_name = file_manager.upload(filename, keep_filename=True)
    os.remove(filename)
    return url


def image_transpose(image_file):
    image = Image.open(image_file)
    image = ImageOps.exif_transpose(image)
    image.save(image_file)
    image.close()


@settings_blueprint.route('/change-password', methods=['POST'])
@login_required
@cu_security.log_activity(
    cu_security.log_authetication_activity,
    action_type=UserActions.ACTION_TYPE_PASSWORD_MODIFICATION)
def change_password():
    status = 'success'
    try:
        old_password = request.form.get('old_password', '')
        new_password = request.form.get('password', '')
        login_manager = cu_database.login_manager
        login_manager.change_password(current_user.get_id(), old_password,
                                      new_password)
    except Exception as e:
        logging.exception('Exception changing password: %s' % e)
        status = 'error'
    return redirect('/user-settings?tab=%s&status=%s' %
                    ('change-password', status))
